package kamala.kalpana.http.bookmark;

import javafx.util.Pair;
import kamala.kalpana.http.CSRFToken;
import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.json.EmptyData;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.jsoup.nodes.Element;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface BookmarkAPI
{
  String TOGGLE_TRACK_USER_URL = HTTPUtilities.ComposeURL("json", "userTrack.json");

  static CompletableFuture<Pair<Integer, CSRFToken>> GetCharacterPageToken(FListClient client, String characterName)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Pair<Integer, CSRFToken>> body = raw_response
            .thenApply(HTTPUtilities::ResponseAsDocument).thenApply(document -> {

              Integer characterId = null;
              CSRFToken token = null;

              Element character_id_input = document.getElementById("profile-character-id");
              if(character_id_input != null)
                characterId = Integer.valueOf(character_id_input.val());

              Element csrf_token = document.getElementById("flcsrf-token");
              if (csrf_token != null)
                token = new CSRFToken(csrf_token.attr("content"));

              return new Pair<>(characterId, token);
            });


    try
    {
      URI uri = new URIBuilder()
              .setScheme("https")
              .setHost("f-list.net")
              .setPath("/c/" + characterName)
              .build();

      HttpGet hpg = new HttpGet(uri);

      client.executeRequest(hpg, raw_response);

      return body;
    }
    catch (URISyntaxException e)
    {
      //e.printStackTrace();
    }

    return CompletableFuture.completedFuture(new Pair<>(-1, new CSRFToken("")));
  }

  String LIST_BOOKMARKS_URL = HTTPUtilities.ComposeURL("json", "getBookmarks.json");

  static CompletableFuture<BookmarkList> List(FListClient client)
  {
    return List(client, 0, 20);
  }

  static CompletableFuture<BookmarkList> List(FListClient client, int offset, int amount)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<BookmarkList> body = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, BookmarkList.class));

    HttpUriRequest req = RequestBuilder
            .get(LIST_BOOKMARKS_URL)
            .addParameter("offset", offset+"")
            .addParameter("amount", amount+"")
            .build();

    client.executeRequest(req, raw_response);

    return body;
  }


  enum BookmarkStatus
  {
    Added,
    None,
    Removed
  }

  static CompletableFuture<BookmarkStatus> Toggle(FListClient client, String characterName)
  {
    try
    {
      Pair<Integer, CSRFToken> token = GetCharacterPageToken(client, characterName).get();

      return Toggle(client, token.getValue(), token.getKey());
    }
    catch (InterruptedException e)
    {
      //e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      //e.printStackTrace();
    }

    return CompletableFuture.completedFuture(BookmarkStatus.None);
  }

  static CompletableFuture<BookmarkStatus> Toggle(FListClient client, CSRFToken token, int characterID)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<BookmarkStatus> sl = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(emptyData -> {
              if(HTTPUtilities.IsDataOkay(emptyData))
              {
                String state = (String) emptyData.getUnknownProperties().get("state");

                if(state.equalsIgnoreCase("no"))
                  return BookmarkStatus.Removed;
                else if(state.equalsIgnoreCase("yes"))
                  return BookmarkStatus.Added;
              }

              return BookmarkStatus.None;
            });

    HttpUriRequest req = RequestBuilder
            .post(TOGGLE_TRACK_USER_URL)
            .addParameter("dest_character_id", characterID + "")
            .addParameter(token)
            .build();

    client.executeRequest(req, raw_response);

    return sl;
  }
}
