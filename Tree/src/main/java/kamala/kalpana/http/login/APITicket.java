package kamala.kalpana.http.login;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.StateCache;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

/**
 * API Ticket class that contains data to allow authentication to the chat service as well
 * as a ticket to permit usage of the existing F-List JSON API endpoints.
 */
public class APITicket extends AbstractAPIData implements StateCache.StateObject
{
  public static final String CACHE_KEY = "chat_login_ticket";

  private String accountName;

  private List<String> characters;

  private String default_character;
  private String account_id;
  private String ticket;

  private List<FriendEntry> friends;
  private List<BookmarkEntry> bookmarks;

  /**
   * @return The name of the account that was used to generate this ticket.
   */
  @JsonIgnore
  public String getAccountName()
  {
    return accountName;
  }
  public void setAccountName(String newValue) { accountName = newValue; }

  /**
   * @return The character that is 'default' on the account that is related to this ticket.
   */
  @JsonProperty("default_character")
  public String getDefaultCharacter() { return default_character; }
  public void setDefaultCharacter(String newValue) { default_character = newValue; }

  @JsonProperty("account_id")
  public String getAccountID() { return account_id; }
  private void setAccountID(String newValue) { account_id = newValue; }

  /**
   * @return A currently valid ticket with the site API (Assuming it hasn't been invalidated).
   */
  @JsonProperty("ticket")
  public String getTicket() { return ticket; }
  private void getTicket(String newValue) { ticket = newValue; }

  @JsonProperty("characters")
  public List<String> getCharacters() { return characters; }
  public void setCharacters(List<String> newValue) { characters = newValue; }

  @JsonProperty("friends")
  public List<FriendEntry> getFriends() { return friends; }
  public void setFriends(List<FriendEntry> newValue) { friends = newValue; }

  @JsonProperty("bookmarks")
  public List<BookmarkEntry> getBookmarks() { return bookmarks; }
  public void setBookmarks(List<BookmarkEntry> newValue) { bookmarks = newValue; }


  @Override
  @JsonIgnore
  public String getType()
  {
    return "LoginTicket";
  }

  @Override
  public String getIdentifier()
  {
    return CACHE_KEY;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();

    if(this.hasError())
      sb.append("Error in Login Ticket Request: " + this.getError());
    else
    {
      sb.append("\nAccount ID: " + getAccountID());
      sb.append("\nTicket: " + getTicket());
      sb.append("\nDefault Character: " + getDefaultCharacter());
      sb.append("\nCharacters\n\t" + (getCharacters() != null ? String.join(", ", getCharacters()) : null));
    }

    return sb.toString();
  }

  static class BookmarkEntry
  {
    public String name;
  }

  static class FriendEntry
  {
    public String source_name;
    public String dest_name;
  }
}
