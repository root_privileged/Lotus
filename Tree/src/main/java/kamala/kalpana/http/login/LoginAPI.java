package kamala.kalpana.http.login;

import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.nodes.Element;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface LoginAPI
{
  interface Site
  {
    String HOMEPAGE_URL = HTTPUtilities.ComposeURL();
    String LOGIN_URL = HTTPUtilities.ComposeURL("action", "script_login.php");

    /**
     * Retrieves the CSRF Token from the main page to allow the client to submit a login without causing an error.
     *
     * @param client
     * @return
     */
    static CompletableFuture<NameValuePair> GetCSRFToken(FListClient client)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<NameValuePair> body = raw_response.thenApply(HTTPUtilities::ResponseAsDocument).thenApply(document -> {
        Element lb = document.getElementById("LoginBox");

        if (lb != null)
        {
          Element loginForm = lb.getElementsByTag("form").first();

          if (loginForm != null)
          {
            Element csrf_token = loginForm.getElementsByAttributeValue("name", "csrf_token").first();

            if (csrf_token != null)
              return new BasicNameValuePair("csrf_token", csrf_token.val());
          }
        }

        return null;
      });

      HttpGet hpg = new HttpGet(HOMEPAGE_URL);

      client.executeRequest(hpg, raw_response);

      return body;
    }

    /**
     * Attempts a login with the specified client and credentials supplied, this lacks the {@link NameValuePair csrf_token}
     * argument because it makes the request internally for you (Although it does it in a blocking manner, so be aware of this.
     *
     * @param client
     * @param username
     * @param password
     * @return Login status, also stores logged in username in the client's internal map to make contextual API's easier.
     */
    static CompletableFuture<Boolean> AttemptLogin(FListClient client, String username, String password)
    {
      NameValuePair token = null;

      try
      {
        token = GetCSRFToken(client).get();
      }
      catch (InterruptedException e)
      {
        e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        e.printStackTrace();
      }

      return AttemptLogin(client, token, username, password);
    }

    static CompletableFuture<Boolean> AttemptLogin(FListClient client, NameValuePair csrf_token, String username, String password)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<Boolean> rv = raw_response
              .thenApply(HttpResponse::getStatusLine)
              .thenApply(StatusLine::getStatusCode)
              .handle((code, throwable) -> {
                if(throwable != null)
                {
                  client.remove("username");
                  return false;
                }
                else if(code == 302)
                {
                  client.put("username", username);
                  return true;
                }

                return false;
              });

      HttpUriRequest req = RequestBuilder
              .post(LOGIN_URL)
              .addParameter("username", username)
              .addParameter("password", password)
              .addParameter("redirect", HOMEPAGE_URL)
              .addParameter(csrf_token)
              .build();

      client.executeRequest(req, raw_response);

      return rv;
    }

    static CompletableFuture<APITicket> GetSession(FListClient client)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();
      CompletableFuture<APITicket> fcl = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, APITicket.class))
              .thenApply(fChatLogin -> {
                  if (fChatLogin != null && !fChatLogin.hasError() && client.containsKey("username"))
                  {
                    fChatLogin.setAccountName((String) client.get("username"));
                    client.getState().setValue(fChatLogin);
                  }

                  return fChatLogin;
              });

      HttpPost htp = new HttpPost(FChat.CHAT_TICKET_URL);
      client.executeRequest(htp, raw_response);

      return fcl;
    }
  }

  interface FChat
  {
    String CHAT_TICKET_URL = HTTPUtilities.ComposeURL("json", "getApiTicket.php");

    static CompletableFuture<APITicket> GetSession(FListClient client, String username, String password)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();
      CompletableFuture<APITicket> fcl = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, APITicket.class))
              .thenApply(fChatLogin -> {

                if (fChatLogin != null && !fChatLogin.hasError())
                {
                  fChatLogin.setAccountName(username);
                  client.getState().setValue(fChatLogin);
                }

                return fChatLogin;
              });

      HttpUriRequest htp = RequestBuilder
              .post(CHAT_TICKET_URL)
              .addParameter("account", username)
              .addParameter("password", password)
              .build();

      client.executeRequest(htp, raw_response);

      return fcl;
    }
  }
}
