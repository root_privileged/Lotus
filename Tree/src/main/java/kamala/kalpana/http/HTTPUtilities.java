package kamala.kalpana.http;

import kamala.kalpana.http.json.AbstractAPIData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.function.Function;

public interface HTTPUtilities
{
  String BASE_URL = "https://www.f-list.net";

  static String ComposeURL() { return BASE_URL; }

  static String ComposeURL(String subPath)
  {
    return BASE_URL + "/" + subPath;
  }

  static String ComposeURL(String group, String subPath)
  {
    return BASE_URL + "/" + group + "/" + subPath;
  }

  static boolean IsDataOkay(AbstractAPIData apiData)
  {
    boolean ok = apiData != null && !apiData.hasError();

    if(!ok)
      System.out.println("Error: " + apiData.getError());

    return ok;
  }

  static <T> T ResponseAsType(HttpResponse httpResponse, Class<T> type)
  {
    T v = null;

    try
    {
      String s = HTTPUtilities.ResponseAsString(httpResponse);

      v = LotusTree.getObjectMapper().readValue(s, type);
    }
    catch (IOException e)
    {
      e.printStackTrace();
      // In this case silent suppression is okay.
    }

    return v;
  }

  static Document ResponseAsDocument(HttpResponse httpResponse)
  {
    if(httpResponse != null)
    {
      String content = ResponseAsString(httpResponse);

      if(content != null)
        return Jsoup.parse(content);
    }

    return Jsoup.parse("");
  }

  /**
   * Method to be used as a reference in a {@link kamala.kalpana.http.response.CompletableHttpResponse#thenApply(Function)}
   * chain to transform the initial {@link HttpResponse} into something more usable (Namely a string made up of the body of
   * the response if it is okay.)
   *
   * @param httpResponse
   * @return
   */
  static String ResponseAsString(HttpResponse httpResponse)
  {
    if(httpResponse != null)
    {
      HttpEntity ent = httpResponse.getEntity();

      if(ent != null)
      {
        return ContentAsString(ent);
      }
    }

    return null;
  }

  static String ContentAsString(HttpEntity httpEntity)
  {
    ContentType contentType = ContentType.getOrDefault(httpEntity);
    Charset charset = contentType.getCharset();
    try
    {
      return EntityUtils.toString(httpEntity, charset);
    }
    catch (IOException e)
    {
      return null;
    }
  }
}
