package kamala.kalpana.http.json;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public class EmptyData extends AbstractAPIData
{
  private Map<String, Object> unknownProperties = new HashMap<>();

  @JsonAnySetter
  public void setUnknownProperty(String key, Object value)
  {
    unknownProperties.put(key, value);
  }
  public Map<String, Object> getUnknownProperties()
  {
    return unknownProperties;
  }

  @Override
  public String getType()
  {
    return "Empty";
  }
}
