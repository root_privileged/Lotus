package kamala.kalpana.http.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public abstract class AbstractAPIData// extends HashMap<String, Object>
{
  private StringProperty error;

  public AbstractAPIData()
  {
    error = new SimpleStringProperty("");
  }

  public abstract String getType();

  /*
  @JsonAnySetter
  public void setUnknownProperty(String key, Object value)
  {
    this.put(key, value);
  }
  protected Object getUnknownProperty(String key)
  {
    return this.get(key);
  }*/

  @JsonProperty("error")
  public String getError() { return error.get(); }
  public void setError(String newX) { error.set(newX); }

  @JsonIgnore
  public boolean hasError()
  {
    return !error.get().isEmpty();
  }
}
