package kamala.kalpana.http.character;

import com.fasterxml.jackson.core.type.TypeReference;
import javafx.util.Pair;
import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.LotusTree;
import kamala.kalpana.http.character.dependent.BasicData;
import kamala.kalpana.http.character.dependent.FriendData;
import kamala.kalpana.http.character.dependent.GroupData;
import kamala.kalpana.http.character.dependent.ImageData;
import kamala.kalpana.http.json.EmptyData;
import kamala.kalpana.http.login.LoginAPI;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface CharacterAPI
{
  String CHARACTER_NAME_CHECK_URL = HTTPUtilities.ComposeURL("json", "checkCharacterName.json");

  String CREATING_URL = HTTPUtilities.ComposeURL("character_create.php");
  String CREATE_URL = HTTPUtilities.ComposeURL("action", "script_character_create.php");

  // GET, single parameter id=<character id>
  String EDIT_VIEW_URL = HTTPUtilities.ComposeURL("character_edit.php");
  String EDIT_SUBMIT_URL = HTTPUtilities.ComposeURL("action", "script_character_edit.php");

  Pattern ID_PATTERN = Pattern.compile("\\.\\./character_edit\\.php\\?id=(\\d+)");
  Pattern CHARACTERDATA_PATTERN = Pattern.compile("var characterdata = (\\{.*\\});");

  /**
   * Retrieves the character creation CSRF Token from the character creation page to allow calling
   * {@link CharacterAPI#Create(FListClient, NameValuePair, String)}.
   *
   * @param client Containing HTTP client and required cookies to execute a create request.
   * @return A {@link CompletableFuture<NameValuePair>} that will be completed upon request completion or failure, if the request
   */
  static CompletableFuture<NameValuePair> GetCSRFToken(FListClient client)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<NameValuePair> token = raw_response.thenApply(HTTPUtilities::ResponseAsDocument).thenApply(document -> {
      Element lb = document.getElementById("CharacterForm");

      if (lb != null)
      {
        Element csrf_token = lb.getElementsByAttributeValue("name", "csrf_token").first();

        if(csrf_token != null)
          return new BasicNameValuePair("csrf_token", csrf_token.val());
      }

      return null;
    });

    HttpGet hpg = new HttpGet(CREATING_URL);
    client.executeRequest(hpg, raw_response);

    return token;
  }

  /**
   * Sends a GET request to check if the specified character name is available to be created.
   *
   * @param client Containing HTTP client and required cookies to execute a create request.
   * @param characterName Character name to check for availability.
   * @return A {@link CompletableFuture<Boolean>} that will be completed with true if the character name is available and
   * false is the character name was unavailable.
   */
  static CompletableFuture<Boolean> IsAvailable(FListClient client, String characterName)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Boolean> rv = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(HTTPUtilities::IsDataOkay);

    HttpUriRequest req = RequestBuilder
            .get(CHARACTER_NAME_CHECK_URL)
            .addParameter("name", characterName)
            .build();

    client.executeRequest(req, raw_response);

    return rv;
  }

  /**
   * Attempts to create a new character with the {@param character Name} specified and using the cookies and settings
   * contained in the {@param client FListClient}. This does not automatically check if the character name itself is available
   * so it is suggested to use {@link CharacterAPI#IsAvailable(FListClient, String)} to check if the name can be taken
   * before calling this function. This automatically retrieves the required CSRF token as it is required.
   *
   * @param client Containing HTTP client and required cookies to execute a create request.
   * @param character Character name to attempt to create.
   * @return A {@link CompletableFuture<Integer>} that will be completed upon request completion or failure, if the request
   * fails this will be completed with -1 otherwise it will be the new character's ID.
   *
   */
  static CompletableFuture<Integer> Create(FListClient client, String character)
  {
    NameValuePair token = null;

    try
    {
      token = GetCSRFToken(client).get();
    }
    catch (InterruptedException e)
    {
      e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      e.printStackTrace();
    }

    return Create(client, token, character);
  }

  /**
   * Attempts to create a new character with the {@param character Name} specified and using the cookies and settings
   * contained in the {@param client FListClient}. This does not automatically check if the character name itself is available
   * so it is suggested to use {@link CharacterAPI#IsAvailable(FListClient, String)} to check if the name can be taken
   * before calling this function.
   *
   * @param client Containing HTTP client and required cookies to execute a create request.
   * @param csrf_token A {@link NameValuePair} containing the required CSRF token to execute a character creation request.
   * @param characterName Character name to attempt to create.
   * @return A {@link CompletableFuture<Integer>} that will be completed upon request completion or failure, if the request
   * fails this will be completed with -1 otherwise it will be the new character's ID.
   */
  static CompletableFuture<Integer> Create(FListClient client, NameValuePair csrf_token, String characterName)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Integer> sl = raw_response
            .thenApply(httpResponse -> {

              Optional<Matcher> m = Arrays
                      .stream(httpResponse.getAllHeaders())
                      .map(header -> ID_PATTERN.matcher(header.getValue()))
                      .filter(matcher -> matcher.matches())
                      .findFirst();

              return m.isPresent() ? m.get().group(1) : "-1";
            })
            .thenApply(Integer::parseInt);

    HttpUriRequest character_create_request = RequestBuilder
            .post(CREATE_URL)
            .setCharset(Consts.UTF_8)
            .addParameter("name", characterName)
            .addParameter(csrf_token)
            .build();

    client.executeRequest(character_create_request, raw_response);

    return sl;
  }

  /**
   * Scrapes a list of the currently logged in account's characters and their IDs by scraping the homepage for the
   * associated script tag that contains the data.
   *
   * @param client Containing HTTP client and required cookies to execute a create request.
   * @return List of {@link Pair<String, Integer>} containing all the characters that are on the currently logged account,
   * or an empty list if there was a failure or the client was not logged in.
   */
  static CompletableFuture<List<Pair<String, Integer>>> List(FListClient client)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<List<Pair<String, Integer>>> characters = raw_response
            .thenApply(HTTPUtilities::ResponseAsDocument)
            .thenApply(document -> {

              Optional<String> cdjson = document.getElementsByTag("script")
                      .stream()
                      .map(Element::html)
                      .map(CHARACTERDATA_PATTERN::matcher)
                      .filter(Matcher::matches)
                      .map(matcher -> matcher.group(1))
                      .findFirst();

              List<Pair<String, Integer>> chrs = new ArrayList<>();

              cdjson.ifPresent(s -> {
                try
                {
                  Map<String, Object> ed = LotusTree.getObjectMapper().readValue(s, new TypeReference<HashMap<String,String>>(){});

                  ed.forEach((s1, o) -> {
                    if(o instanceof String)
                    {
                      String v = (String) o;

                      chrs.add(new Pair<>(v, Integer.parseInt(s1)));
                    }
                  });
                }
                catch (IOException e)
                {
                  e.printStackTrace();
                }
              });

              return chrs;
            });

    HttpGet hpg = new HttpGet(LoginAPI.Site.HOMEPAGE_URL);

    client.executeRequest(hpg, raw_response);

    return characters;
  }

  /**
   * TODO: Document this, and write its data class up with better standards and cleaner code.
   *
   * @param client
   * @param characterName
   * @return
   */
  static CompletableFuture<CharacterData> View(FListClient client, String characterName)
  {
    String characterPage = HTTPUtilities.ComposeURL("c", characterName);

    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<CharacterData> data = raw_response
            .thenApply(HTTPUtilities::ResponseAsDocument)
            .thenApply(CharacterData::new);

    HttpUriRequest hpg = RequestBuilder
            .get(characterPage)
            .build();

    client.getCookies().addCookie(new BasicClientCookie("warning", "1"));
    client.executeRequest(hpg, raw_response);

    return data;
  }

  /**
   * TODO: Write this, and its backing class as well.
   *
   * @param client
   * @param characterId
   * @return
   */
  static CompletableFuture<CharacterEditableData> Edit(FListClient client, int characterId)
  {
    return null;
  }

  /**
   * TODO: Write this, and its backing class as well.
   *
   * @param client
   * @param data
   * @return
   */
  static CompletableFuture<Boolean> Submit(FListClient client, CharacterEditableData data)
  {
    return null;
  }


  interface CharacterPart
  {
    String GET_BASIC_DATA = HTTPUtilities.ComposeURL("json/api", "character-get.php");
    static CompletableFuture<BasicData> GetBasicData(FListClient client, String name)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<BasicData> rv = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, BasicData.class));

      HttpUriRequest req = RequestBuilder
              .post(GET_BASIC_DATA)
              .addParameter("name", name)
              .build();

      client.executeRequest(req, raw_response);

      return rv;
    }

    String GET_FRIEND_DATA = HTTPUtilities.ComposeURL("json", "profile-friends.json");
    static CompletableFuture<FriendData> GetFriendData(FListClient client, int character_id)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<FriendData> rv = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, FriendData.class));

      HttpUriRequest req = RequestBuilder
              .post(GET_FRIEND_DATA)
              .addParameter("character_id", character_id + "")
              .build();

      client.executeRequest(req, raw_response);

      return rv;
    }

    String GET_IMAGE_DATA = HTTPUtilities.ComposeURL("json", "profile-images.json");
    static CompletableFuture<ImageData> GetImageData(FListClient client, int character_id)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<ImageData> rv = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, ImageData.class));

      HttpUriRequest req = RequestBuilder
              .post(GET_IMAGE_DATA)
              .addParameter("character_id", character_id + "")
              .build();

      client.executeRequest(req, raw_response);

      return rv;
    }

    String GET_GROUP_DATA = HTTPUtilities.ComposeURL("json", "profile-groups.json");
    static CompletableFuture<GroupData> GetGroups(FListClient client, int character_id)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<GroupData> rv = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, GroupData.class));

      HttpUriRequest req = RequestBuilder
              .post(GET_GROUP_DATA)
              .addParameter("character_id", character_id + "")
              .build();

      client.executeRequest(req, raw_response);

      return rv;
    }
  }
}
