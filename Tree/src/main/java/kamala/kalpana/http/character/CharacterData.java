package kamala.kalpana.http.character;

import kamala.kalpana.http.character.dependent.*;
import kamala.kalpana.http.data.AbstractScrapingData;
import org.jsoup.nodes.Document;

public class CharacterData extends AbstractScrapingData
{
  private BasicData basicData;
  private InfoData infoData;
  private GroupData groupData;
  private ImageData imageData;
  private KinkData kinkData;
  private CustomKinkData customKinkData;

  public CharacterData(Document document)
  {
    super(document);
  }

  public CharacterData(BasicData basicData, InfoData infoData, KinkData kinkData, CustomKinkData customKinkData, ImageData imageData, GroupData groupData)
  {
    super(null); // Document isn't needed.

    this.basicData = basicData;
    this.infoData = infoData;
    this.groupData = groupData;
    this.imageData = imageData;
    this.kinkData = kinkData;
    this.customKinkData = customKinkData;
  }
}
