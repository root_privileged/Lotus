package kamala.kalpana.http.character.dependent;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class ImageData extends AbstractAPIData
{
  @JsonProperty("images")
  private List<ImageReference> images;

  public final List<ImageReference> getImageReferences() { return images; }

  @Override
  public String getType()
  {
    return "CharacterData:ImageData";
  }

  @Override
  public String toString()
  {
    return "ImageData{" +
            "images=" + images +
            '}';
  }

  static public class ImageReference
  {
    private int image_id;
    private String extension;
    private int width;
    private int height;
    private String description;

    public int getImageId() { return image_id; }

    public int getWidth()
    {
      return width;
    }

    public int getHeight()
    {
      return height;
    }

    public String getExtension()
    {
      return extension;
    }

    public String getDescription()
    {
      return description;
    }

    @Override
    public String toString()
    {
      return "ImageReference{" +
              "image_id=" + image_id +
              ", extension='" + extension + '\'' +
              ", width=" + width +
              ", height=" + height +
              ", description='" + description + '\'' +
              '}';
    }
  }
}
