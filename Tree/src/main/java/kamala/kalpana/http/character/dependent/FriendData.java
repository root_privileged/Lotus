package kamala.kalpana.http.character.dependent;

import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class FriendData extends AbstractAPIData
{
  private List<String> friends;

  public List<String> getFriends() { return friends; }

  @Override
  public String getType()
  {
    return "FriendData";
  }
}
