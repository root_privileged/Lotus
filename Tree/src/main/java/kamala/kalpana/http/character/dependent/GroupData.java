package kamala.kalpana.http.character.dependent;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class GroupData extends AbstractAPIData
{
  private List<String> groups;

  @JsonProperty("groups")
  private void setGroups(List<String> grps)
  {
    groups = grps;
  }
  public List<String> getGroups()
  {
    return groups;
  }

  @Override
  public String getType()
  {
    return "CharacterData:GroupData";
  }
}