package kamala.kalpana.http.data;

import org.jsoup.nodes.Document;

public abstract class AbstractScrapingData
{
  private Document document;

  protected AbstractScrapingData(Document document)
  {
    this.document = document;
  }

  public final Document getDocument() { return document; }
}
