package kamala.kalpana.http.note;

import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.note.notes.NoteList;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

import java.util.concurrent.CompletableFuture;

public interface SearchAPI
{
  String USER_SEARCH_URL = HTTPUtilities.ComposeURL("json", "notes-searchuser.json");
  String QUERY_SEARCH_URL = HTTPUtilities.ComposeURL("json", "notes-searchcontent.json");

  static CompletableFuture<NoteList> NotesByUser(FListClient client, String user, int offset, int amount)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<NoteList> noteList = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, NoteList.class));

    HttpUriRequest req = RequestBuilder
            .get(USER_SEARCH_URL)
            .addParameter("offset", offset + "")
            .addParameter("amount", amount + "")
            .addParameter("name", user)
            .build();

    client.executeRequest(req, raw_response);

    return noteList;
  }

  static CompletableFuture<NoteList> NotesByQuery(FListClient client, String query, int offset, int amount)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<NoteList> noteList = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, NoteList.class));

    HttpUriRequest req = RequestBuilder
            .get(QUERY_SEARCH_URL)
            .addParameter("offset", offset + "")
            .addParameter("amount", amount + "")
            .addParameter("query", query)
            .build();

    client.executeRequest(req, raw_response);

    return noteList;
  }

}