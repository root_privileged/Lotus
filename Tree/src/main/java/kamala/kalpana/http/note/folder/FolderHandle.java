package kamala.kalpana.http.note.folder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = {"0", "1", "2", "3"})
public class FolderHandle
{
  private String name;
  private int folder_id;
  private int notecount;
  private int unreadcount;

  @JsonProperty("name")
  public String getName() { return name; }
  public void setName(String name) { this.name = name; }

  @JsonProperty("folder_id")
  public int getID() { return folder_id; }
  public void setID(int folder_id) { this.folder_id = folder_id; }

  @JsonProperty("notecount")
  public int getNoteCount() { return notecount; }
  public void setNoteCount(int notecount) { this.notecount = notecount; }

  @JsonProperty("unreadcount")
  public int getUnreadCount() { return unreadcount; }
  public void setUnreadCount(int unreadcount) { this.unreadcount = unreadcount; }
}
