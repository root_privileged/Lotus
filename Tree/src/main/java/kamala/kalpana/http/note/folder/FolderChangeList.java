package kamala.kalpana.http.note.folder;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.ArrayList;
import java.util.List;

public class FolderChangeList extends AbstractAPIData
{
  private List<FolderChange> counts = new ArrayList<>();

  @JsonProperty("counts")
  public List<FolderChange> getFolderChanges() { return counts; }
  public void setFolderChanges(List<FolderChange> counts) { this.counts = counts; }

  @Override
  public String getType()
  {
    return "FolderChangeList";
  }
}
