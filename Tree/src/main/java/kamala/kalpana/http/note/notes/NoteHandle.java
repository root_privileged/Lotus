package kamala.kalpana.http.note.notes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.time.LocalDateTime;

@JsonIgnoreProperties(value = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"})
public class NoteHandle extends AbstractAPIData
{
  private int note_id;
  private String title;

  private int source_character_id;
  private String source_name;

  private String dest_name;
  private int dest_character_id;

  int dest_account_id;
  int source_account_id;

  LocalDateTime datetime_sent;

  int folder_id;
  boolean read;
  boolean starred;


  @JsonProperty("note_id")
  public int getNoteID()
  {
    return note_id;
  }
  public void setNoteID(int note_id)
  {
    this.note_id = note_id;
  }

  @JsonProperty("title")
  public String getTitle()
  {
    return title;
  }
  public void setTitle(String title)
  {
    this.title = title;
  }

  @JsonProperty("source_character_id")
  public int getSourceCharacterID()
  {
    return source_character_id;
  }
  public void setSourceCharacterID(int source_character_id)
  {
    this.source_character_id = source_character_id;
  }

  @JsonProperty("source_name")
  public String getSourceName()
  {
    return source_name;
  }
  public void setSourceName(String source_name)
  {
    this.source_name = source_name;
  }

  @JsonProperty("dest_name")
  public String getDestinationName()
  {
    return dest_name;
  }
  public void setDestinationName(String dest_name)
  {
    this.dest_name = dest_name;
  }

  @JsonProperty("dest_character_id")
  public int getDestinationCharacterID()
  {
    return dest_character_id;
  }
  public void setDestinationCharacterID(int dest_character_id)
  {
    this.dest_character_id = dest_character_id;
  }

  @JsonProperty("dest_account_id")
  public int getDestinationAccountID()
  {
    return dest_account_id;
  }
  public void setDestinationAccountID(int dest_account_id)
  {
    this.dest_account_id = dest_account_id;
  }

  @JsonProperty("source_account_id")
  public int getSourceAccountID()
  {
    return source_account_id;
  }
  public void setSourceAccountID(int source_account_id)
  {
    this.source_account_id = source_account_id;
  }

  @JsonProperty("datetime_sent")
  public LocalDateTime getDateTimeSent()
  {
    return datetime_sent;
  }
  public void setDateTimeSent(LocalDateTime datetime_sent)
  {
    this.datetime_sent = datetime_sent;
  }

  @JsonProperty("folder_id")
  public int getFolderID()
  {
    return folder_id;
  }
  public void setFolderID(int folder_id)
  {
    this.folder_id = folder_id;
  }

  @JsonProperty("read")
  public boolean isRead()
  {
    return read;
  }
  public void setRead(boolean read)
  {
    this.read = read;
  }

  @JsonProperty("starred")
  public boolean isStarred()
  {
    return starred;
  }
  public void setStarred(boolean starred)
  {
    this.starred = starred;
  }

  @Override
  public String getType()
  {
    return "NoteHandle";
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();

    sb.append("NoteHandle");

    sb.append("\n\tNote ID: " + getNoteID());
    sb.append("\n\tTitle: " + getTitle());
    sb.append("\n\tSource Character: " + getSourceName());
    sb.append("\n\tSource ID: " + getSourceCharacterID());
    sb.append("\n\tDestination Character: " + getDestinationName());
    sb.append("\n\tDestination ID: " + getDestinationCharacterID());
    sb.append("\n\tSource Account: " + getSourceAccountID());
    sb.append("\n\tDestination Account: " + getDestinationAccountID());
    sb.append("\n\tTime Sent: " + getDateTimeSent());
    sb.append("\n\tFolder ID: " + getFolderID());
    sb.append("\n\tRead: " + isRead());
    sb.append("\n\tStarred: " + isStarred());

    return sb.toString();
  }
}
