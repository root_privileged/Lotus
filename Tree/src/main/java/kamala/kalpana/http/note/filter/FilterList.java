package kamala.kalpana.http.note.filter;

import kamala.kalpana.http.json.EmptyData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FilterList
{
  private EmptyData emptyData;
  private List<FilterHandle> filters;

  public FilterList(EmptyData ed)
  {
    this.emptyData = ed;

    Map<String, Object> handles = ed.getUnknownProperties();

    filters = new ArrayList<>();

    for(Map.Entry<String, Object> ent : ((Map<String, Object>) handles.get("filters")).entrySet())
    {
      try
      {
        Integer.parseInt(ent.getKey());

        Map<String, Object> tv = (Map<String, Object>) ent.getValue();

        if(tv != null)
          filters.add(new FilterHandle(tv));
      }
      catch(NumberFormatException nfe) {}
    }
  }

  public final List<FilterHandle> getFilters() { return filters; }

  public final EmptyData getData() { return emptyData; }
}
