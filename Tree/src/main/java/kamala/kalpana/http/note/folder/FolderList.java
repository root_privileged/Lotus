package kamala.kalpana.http.note.folder;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class FolderList extends AbstractAPIData
{
  private List<FolderHandle> folders;

  @JsonProperty("folders")
  public List<FolderHandle> getFolders() { return folders; }
  public void setFolders(List<FolderHandle> folders) { this.folders = folders; }

  @Override
  public String getType()
  {
    return "FolderList";
  }
}
