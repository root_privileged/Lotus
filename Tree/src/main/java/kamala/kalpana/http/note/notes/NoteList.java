package kamala.kalpana.http.note.notes;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class NoteList extends AbstractAPIData
{
  private int total;
  private List<NoteHandle> notes;

  @JsonProperty("notes")
  public List<NoteHandle> getNotes() { return notes; }
  public void setNotes(List<NoteHandle> notes) { this.notes = notes; }

  @JsonProperty("total")
  public int getTotal() { return total; }
  public void setTotal(int newX) { total = newX; }

  @Override
  public String getType()
  {
    return "NoteList";
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();

    if(this.hasError())
      sb.append("Error in NoteList Request: " + this.getError());
    else
    {
      sb.append("\nTotal Notes: " + total);
      sb.append("\nNotes");

      if(getNotes() != null)
      {
        for(NoteHandle nh : getNotes())
          sb.append("\n\t" + nh);
      }
    }

    return sb.toString();
  }
}
