package kamala.kalpana.http.note;

import kamala.kalpana.http.CSRFToken;
import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.json.EmptyData;
import kamala.kalpana.http.note.folder.FolderChangeList;
import kamala.kalpana.http.note.notes.NoteHandle;
import kamala.kalpana.http.note.notes.NoteList;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.jsoup.nodes.Element;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface NoteAPI
{
  String READ_NOTES_URL = HTTPUtilities.ComposeURL("read_notes.php");

  String NOTES_GET_URL = HTTPUtilities.ComposeURL("json", "notes-get.json");

  String NOTE_VIEW_URL = HTTPUtilities.ComposeURL("view_note.php");

  String NOTE_SEND_URL = HTTPUtilities.ComposeURL("json", "notes-send.json");

  String NOTE_TRASH_URL = HTTPUtilities.ComposeURL("json", "notes-trash.json");

  String NOTE_EMPTY_TRASH_URL = HTTPUtilities.ComposeURL("json", "notes-emptytrash.json");

  String NOTE_SET_STARRED_URL = HTTPUtilities.ComposeURL("json", "notes-setstarred.json");

  String NOTES_GET_STARRED_URL = HTTPUtilities.ComposeURL("json", "notes-getstarred.php");

  String NOTES_GET_UNREAD_URL = HTTPUtilities.ComposeURL("json", "notes-getunread.json");

  static CompletableFuture<CSRFToken> GetCSRFToken(FListClient client)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<CSRFToken> body = raw_response.thenApply(HTTPUtilities::ResponseAsDocument).thenApply(document -> {
      Element csrf_token = document.getElementById("flcsrf-token");

      if (csrf_token != null)
        return new CSRFToken(csrf_token.attr("content"));

      return new CSRFToken("");
    });

    HttpGet hpg = new HttpGet(READ_NOTES_URL);

    client.executeRequest(hpg, raw_response);

    return body;
  }

  static CompletableFuture<NoteList> InboxNotes(FListClient client)
  {
    return InboxNotes(client, 0, 10);
  }

  static CompletableFuture<NoteList> InboxNotes(FListClient client, int offset, int amount)
  {
    return NotesFromFolder(client, NOTES_GET_URL, offset, amount, 1);
  }

  static CompletableFuture<NoteList> OutboxNotes(FListClient client)
  {
    return OutboxNotes(client, 0, 10);
  }

  static CompletableFuture<NoteList> OutboxNotes(FListClient client, int offset, int amount)
  {
    return NotesFromFolder(client, NOTES_GET_URL, offset, amount, 2);
  }

  static CompletableFuture<NoteList> TrashNotes(FListClient client)
  {
    return TrashNotes(client, 0, 10);
  }

  static CompletableFuture<NoteList> TrashNotes(FListClient client, int offset, int amount)
  {
    return NotesFromFolder(client, NOTES_GET_URL, offset, amount, 3);
  }

  static CompletableFuture<NoteList> StarredNotes(FListClient client)
  {
    return StarredNotes(client, 0, 10);
  }

  static CompletableFuture<NoteList> StarredNotes(FListClient client, int offset, int amount)
  {
    return NotesFromFolder(client, NOTES_GET_STARRED_URL, offset, amount, 1);
  }

  static CompletableFuture<NoteList> UnreadNotes(FListClient client)
  {
    return UnreadNotes(client, 0, 10);
  }

  static CompletableFuture<Boolean> SetStarred(FListClient client, NoteHandle note, boolean star)
  {
    return SetStarred(client, note, star);
  }

  static CompletableFuture<Boolean> SetStarred(FListClient client, CSRFToken token, NoteHandle note, boolean star)
  {
    return SetStarred(client, token, Collections.singletonList(note), star);
  }

  static CompletableFuture<Boolean> SetStarred(FListClient client, List<NoteHandle> notes, boolean star)
  {
    try
    {
      return SetStarred(client, GetCSRFToken(client).get(), notes, star);
    }
    catch (InterruptedException e)
    {
      //e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      //e.printStackTrace();
    }

    return CompletableFuture.completedFuture(false);
  }

  static CompletableFuture<Boolean> SetStarred(FListClient client, CSRFToken token, List<NoteHandle> notes, boolean star)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Boolean> sl = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(HTTPUtilities::IsDataOkay);

    RequestBuilder req = RequestBuilder
            .post(NOTE_SET_STARRED_URL)
            .addParameter(token)
            .addParameter("state", star ? "1" : "0");

    for(NoteHandle nh : notes)
      req = req.addParameter("notes[]", nh.getNoteID() + "");

    client.executeRequest(req.build(), raw_response);

    return sl;
  }

  static CompletableFuture<NoteList> UnreadNotes(FListClient client, int offset, int amount)
  {
    return NotesFromFolder(client, NOTES_GET_UNREAD_URL, offset, amount, 1);
  }

  static CompletableFuture<NoteList> NotesFromFolder(FListClient client, String url, int offset, int amount, int folder)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<NoteList> noteList = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, NoteList.class));

    HttpUriRequest req = RequestBuilder
            .get(url)
            .addParameter("offset", offset + "")
            .addParameter("amount", amount + "")
            .addParameter("folder", folder + "")
            .build();

    client.executeRequest(req, raw_response);

    return noteList;
  }

  static CompletableFuture<Boolean> Send(FListClient client, String title, String message, int sourceCharacterID, String destinationCharacter)
  {
    try
    {
      return Send(client, GetCSRFToken(client).get(), title, message, sourceCharacterID, destinationCharacter);
    }
    catch (InterruptedException e)
    {
      //e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      //e.printStackTrace();
    }

    return CompletableFuture.completedFuture(false);
  }

  static CompletableFuture<Boolean> Send(FListClient client, CSRFToken token, String title, String message, int sourceCharacterID, String destinationCharacter)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Boolean> sl = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(HTTPUtilities::IsDataOkay);

    HttpUriRequest req = RequestBuilder
            .post(NOTE_SEND_URL)
            .addParameter("title", title)
            .addParameter("message", message)
            .addParameter("dest", destinationCharacter)
            .addParameter("source", sourceCharacterID + "")
            .addParameter(token)
            .build();

    client.executeRequest(req, raw_response);

    return sl;
  }

  static CompletableFuture<String> View(FListClient client, NoteHandle target)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<String> rv = raw_response
            .thenApply(HTTPUtilities::ResponseAsDocument)
            .thenApply(document -> {
              Element panel = document.getElementsByClass("panel").first();

              if (panel != null)
              {
                if (panel.getElementById("Note_PageHeader") != null)
                {
                  Element formattedBlock = panel.getElementsByClass("FormattedBlock").first();

                  if (formattedBlock != null)
                  {
                    return formattedBlock.html();
                  }
                }
              }

              return "";
            });

    HttpUriRequest req = RequestBuilder
            .get(NOTE_VIEW_URL)
            .addParameter("note_id", target.getNoteID() + "")
            .build();

    client.executeRequest(req, raw_response);

    return rv;
  }

  static CompletableFuture<FolderChangeList> Trash(FListClient client, NoteHandle noteHandle)
  {
    return Trash(client, Collections.singletonList(noteHandle));
  }

  static CompletableFuture<FolderChangeList> Trash(FListClient client, List<NoteHandle> noteHandles)
  {
    try
    {
      return Trash(client, GetCSRFToken(client).get(), noteHandles);
    }
    catch (InterruptedException e)
    {
      //e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      //e.printStackTrace();
    }

    return CompletableFuture.completedFuture(null);
  }

  static CompletableFuture<FolderChangeList> Trash(FListClient client, CSRFToken token, List<NoteHandle> notes)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<FolderChangeList> sl = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, FolderChangeList.class));

    RequestBuilder req = RequestBuilder
            .post(NOTE_TRASH_URL)
            .addParameter(token);

    for(NoteHandle nh : notes)
      req = req.addParameter("notes[]", nh.getNoteID() + "");

    client.executeRequest(req.build(), raw_response);

    return sl;
  }

  static CompletableFuture<Boolean> EmptyTrash(FListClient client)
  {
    try
    {
      return EmptyTrash(client, GetCSRFToken(client).get());
    }
    catch (InterruptedException e)
    {
      //e.printStackTrace();
    }
    catch (ExecutionException e)
    {
      //e.printStackTrace();
    }

    return CompletableFuture.completedFuture(false);
  }

  static CompletableFuture<Boolean> EmptyTrash(FListClient client, CSRFToken token)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Boolean> sl = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(HTTPUtilities::IsDataOkay);

    HttpUriRequest req = RequestBuilder
            .post(NOTE_EMPTY_TRASH_URL)
            .addParameter(token)
            .build();

    client.executeRequest(req, raw_response);

    return sl;
  }
}
