package kamala.kalpana.http.note.filter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(value = {"0", "1", "2", "3"})
public class FilterHandle
{
  private int action_id;
  private int folder_id;
  private boolean read;
  private boolean starred;
  private List<Condition> conditions;

  private FilterHandle() {}

  public FilterHandle(Map<String, Object> data)
  {
    this.action_id = ((Integer) data.get("action_id")).intValue();
    this.folder_id = ((Integer) data.get("folder_id")).intValue();
    this.read = ((Integer) data.get("action_id")).intValue() == 1;
    this.starred = ((Integer) data.get("action_id")).intValue() == 1;

    conditions = new ArrayList<>();

    if(data != null)
    {
      List<Map<String, Object>> conds = (List<Map<String, Object>>) data.get("conditions");

      if(conds != null)
        conds.forEach(stringObjectMap -> conditions.add(new Condition(stringObjectMap)));
    }
  }

  @JsonIgnore
  public int getActionID()
  {
    return action_id;
  }
  @JsonProperty("action_id")
  public void setActionID(int action_id)
  {
    this.action_id = action_id;
  }

  @JsonProperty("folder_id")
  public int getFolderID()
  {
    return folder_id;
  }
  public void setFolderID(int folder_id)
  {
    this.folder_id = folder_id;
  }

  @JsonProperty("read")
  public boolean isRead()
  {
    return read;
  }
  public void setRead(boolean read)
  {
    this.read = read;
  }

  @JsonProperty("starred")
  public boolean isStarred()
  {
    return starred;
  }
  public void setStarred(boolean starred)
  {
    this.starred = starred;
  }

  public List<Condition> getConditions()
  {
    return conditions;
  }
  @JsonProperty("conditions")
  public void setConditions(List<Condition> conditions)
  {
    this.conditions = conditions;
  }

  @Override
  public String toString()
  {
    return "FilterHandle{" +
            "action_id=" + action_id +
            ", folder_id=" + folder_id +
            ", read=" + read +
            ", starred=" + starred +
            ", conditions=" + conditions +
            '}';
  }

  enum ConditionType
  {
    FromSource,
    ToDestination,
    InTitle;

    static ConditionType fromString(String src)
    {
      switch (src)
      {
        case "from-source":
          return FromSource;
        case "to-dest":
          return ToDestination;
        case "in-title":
          return InTitle;
      }

      return FromSource;
    }


    @Override
    public String toString()
    {
      switch(this)
      {
        case FromSource:
          return "from-source";
        case ToDestination:
          return "to-dest";
        case InTitle:
          return "in-title";
      }

      return "from-source";
    }
  }

  @JsonIgnoreProperties(value = {"0", "1", "2", "3"})
  class Condition
  {
    private int action_id;
    private int condition_id;
    private ConditionType type;
    private String value;

    private Condition() {}

    public Condition(Map<String, Object> data)
    {
      this.action_id = ((Integer) data.get("action_id")).intValue();
      this.condition_id = ((Integer) data.get("condition_id")).intValue();
      this.type = ConditionType.fromString((String) data.get("type"));
      this.value = (String) data.get("value");
    }

    @JsonIgnore
    public int getActionID()
    {
      return action_id;
    }
    @JsonProperty("action_id")
    public void setActionID(int action_id)
    {
      this.action_id = action_id;
    }

    @JsonIgnore
    public int getConditionID()
    {
      return condition_id;
    }
    @JsonProperty("condition_id")
    public void setConditionID(int condition_id)
    {
      this.condition_id = condition_id;
    }

    @JsonProperty("type")
    public void setType(String type)
    {
      this.type = ConditionType.fromString(type);
    }

    @JsonProperty("type")
    public String getStringType()
    {
      return this.type.toString();
    }

    @JsonIgnore
    public ConditionType getType()
    {
      return type;
    }

    @JsonProperty("value")
    public String getValue()
    {
      return value;
    }
    public void setValue(String value)
    {
      this.value = value;
    }

    @Override
    public String toString()
    {
      return "Condition{" +
              "action_id=" + action_id +
              ", condition_id=" + condition_id +
              ", type=" + type +
              ", value='" + value + '\'' +
              '}';
    }
  }
}
