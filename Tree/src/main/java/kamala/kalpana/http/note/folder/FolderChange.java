package kamala.kalpana.http.note.folder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FolderChange
{
  private int folder;
  private int total;
  private int unread;

  @JsonProperty("folder")
  public int getFolder()
  {
    return folder;
  }
  public void setFolder(int folder)
  {
    this.folder = folder;
  }

  @JsonProperty("total")
  public int getTotal()
  {
    return total;
  }
  public void setTotal(int total)
  {
    this.total = total;
  }

  @JsonProperty("unread")
  public int getUnread()
  {
    return unread;
  }
  public void setUnread(int unread)
  {
    this.unread = unread;
  }
}
