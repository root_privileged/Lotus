package kamala.kalpana;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kamala.kalpana.connection.actions.AbstractResponderAction;
import kamala.kalpana.connection.command.AbstractCommand;
import kamala.kalpana.connection.packet.AbstractPacket;
import kamala.kalpana.jackson.LotusModule;

import java.util.concurrent.ThreadFactory;

/**
 * Class used to define helpers and initializers that will be commonly used for the entire Lotus ecosystem.
 */
public final class LotusCore
{
  public static final String CNAME = "Lotus";
  public static final String CVERSION = "0.0.0-INCUBATING";

  private static Object mMutex = new Object();
  private static ObjectMapper mObjectMapper;
  public static ObjectMapper getObjectMapper()
  {
    if(mObjectMapper == null)
    {
      synchronized (mMutex)
      {
        mObjectMapper = new ObjectMapper();
        mObjectMapper.disable(com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT);

        mObjectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        mObjectMapper.registerModule(new LotusModule());
      }
    }

    return mObjectMapper;
  }

  /**
   * Uses reflection to initialize all default handlers and commands within the core.
   *
   * This is a one-time function that only needs to be ran once on startup and does not
   * need to be cleaned up before application close as it holds no external resources.
   */
  public static final void Initialize()
  {
    AbstractPacket.InitializePackets();
    AbstractCommand.InitializeCommands();
    AbstractResponderAction.InitializeActions();
  }


  public static final ThreadFactory DAEMON_THREAD_FACTORY = (r) ->
  {
    Thread t = new Thread(r);

    t.setDaemon(false);
    return t;
  };
}
