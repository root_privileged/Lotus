package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jsoup.nodes.Element;

import java.util.List;
import java.util.stream.Collectors;

public class KinkInfo
{
  private List<Fetish> favorites;
  private List<Fetish> yes;
  private List<Fetish> maybe;
  private List<Fetish> no;

  private KinkInfo() {}
  public KinkInfo(Element rootElement)
  {
    favorites = getFetishes(rootElement.getElementById("Character_FetishlistFave"));
    yes = getFetishes(rootElement.getElementById("Character_FetishlistYes"));
    maybe = getFetishes(rootElement.getElementById("Character_FetishlistMaybe"));
    no = getFetishes(rootElement.getElementById("Character_FetishlistNo"));
  }

  @JsonProperty("favorites")
  public final List<Fetish> getFavorites()
  {
    return favorites;
  }
  private void setFavorites(List<Fetish> favorites)
  {
    this.favorites = favorites;
  }

  @JsonProperty("yes")
  public final List<Fetish> getYes()
  {
    return yes;
  }
  private void setYes(List<Fetish> yes)
  {
    this.yes = yes;
  }

  @JsonProperty("maybe")
  public final List<Fetish> getMaybe()
  {
    return maybe;
  }
  private void setMaybe(List<Fetish> maybe)
  {
    this.maybe = maybe;
  }

  @JsonProperty("no")
  public final List<Fetish> getNo()
  {
    return no;
  }
  private void setNo(List<Fetish> no)
  {
    this.no = no;
  }

  @JsonIgnore
  private static List<Fetish> getFetishes(Element rootElement)
  {
    return rootElement.children().stream().filter(e -> e.tagName().equals("a")).map(cKink -> new Fetish(cKink, rootElement)).collect(Collectors.toList());
  }

}
