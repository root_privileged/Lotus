package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImageReference
{
  private int image_id;
  private String extension;
  private int width;
  private int height;
  private String description;

  @JsonProperty("image_id")
  private void setImageID(int image_id) { this.image_id = image_id; }
  public int getImageID() { return image_id; }

  @JsonProperty("extension")
  private void setExtension(String extension) { this.extension = extension; }
  public String getExtension() { return extension; }

  @JsonProperty("width")
  private void setWidth(int width) { this.width = width; }
  public int getWidth() { return width; }

  @JsonProperty("height")
  public void setHeight(int height) { this.height = height; }
  public int getHeight() { return height; }

  @JsonProperty("description")
  private void setDescription(String description) { this.description = description; }
  public String getDescription() { return description; }
}
