package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jsoup.nodes.Element;

import java.time.LocalDateTime;

public class GuestbookPost
{
  private int id;
  private String source;
  private LocalDateTime date;
  private String content;

  private GuestbookPost()
  {

  }

  public GuestbookPost(Element source)
  {
    this.id = Integer.parseInt(source.id().replace("Character_GuestbookPost", ""));

    Element charLink = source.getElementsByClass("CharacterLink").first();
    this.source = charLink.ownText();

    String dateText = source.getElementsByClass("GuestbookDatetime").first().ownText();
   // this.date = FListScraper.fromDelimited(dateText);

    Element contentBlock = source.getElementsByClass("FormattedBlock").first();
    this.content = contentBlock.ownText();
  }

  @JsonProperty("id")
  public int getId()
  {
    return id;
  }
  private final void setId(int id)
  {
    this.id = id;
  }

  @JsonProperty("source")
  public String getSource()
  {
    return source;
  }
  private final void setSource(String source)
  {
    this.source = source;
  }

  @JsonProperty("date")
  //@JsonSerialize(using = LocalDateTimeSerializer.class)
  public LocalDateTime getDate()
  {
    return date;
  }
 // @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  private final void setDate(LocalDateTime date)
  {
    this.date = date;
  }

  @JsonProperty("content")
  public String getContent()
  {
    return content;
  }
  private final void setContent(String content)
  {
    this.content = content;
  }
}
