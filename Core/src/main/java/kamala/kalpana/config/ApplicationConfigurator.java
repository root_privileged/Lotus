package kamala.kalpana.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ApplicationConfigurator
{
  private static String mApplication = "Lotus Chat";

  private static Object mMutex = new Object();
  private static ObjectMapper mObjectMapper;
  public static ObjectMapper getObjectMapper()
  {
    if(mObjectMapper == null)
    {
      synchronized (mMutex)
      {
        mObjectMapper = new ObjectMapper();
        mObjectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        mObjectMapper.disable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
      }
    }

    return mObjectMapper;
  }


  private static Path mConfigDirectory;

  /**
   *
   * @param cfgDirectory
   */
  public static void setConfigurationDirectory(String cfgDirectory)
  {
    mConfigDirectory = Paths.get(cfgDirectory);

    if(!Files.isDirectory(mConfigDirectory))
      mConfigDirectory = null;
  }

  /**
   * Sets the internal application name for the directory to be used for configuration directory.
   *
   * This should be called before any calls to {@link #getConfigurationDirectory() getConfigurationDirectory} method
   * to make sure that your configuration ends up where you intend it to go.
   * @param appName Name of application directory.
   */
  public static final void setApplication(String appName)
  {
    mApplication = appName;
  }

  public static Path getConfigurationDirectory()
  {
    if(mConfigDirectory == null)
    {
      switch(ApplicationPlatform.getOS())
      {
        case Windows:
          String appData = System.getenv("APPDATA");
          mConfigDirectory = FileSystems.getDefault().getPath(appData, mApplication);
          break;
        case Unix:
        case Mac:
          String usrHome = System.getenv("user.home");
          mConfigDirectory = FileSystems.getDefault().getPath(usrHome, "." + mApplication);
          break;
      }

      if(mConfigDirectory != null)
      {
        try
        {
          if(Files.notExists(mConfigDirectory))
            mConfigDirectory = Files.createDirectories(mConfigDirectory);
        }
        catch(IOException ioe)
        {
          // TODO: Error Handling.
        }

        if(!Files.isWritable(mConfigDirectory))
        {
          // TODO: Error Handling, why can't we write our own config directory?
        }
      }
    }

    return mConfigDirectory;
  }

  public static String getDatabaseURL()
  {
    return (new File(ApplicationConfigurator.getConfigurationDirectory().toFile(), "lotus_chat")).toString();
  }

  public static File getCacheDirectory()
  {
    Path cacheDirectory = FileSystems.getDefault().getPath(getConfigurationDirectory().toString(), "cache");

    try
    {
      if(Files.notExists(cacheDirectory))
        cacheDirectory = Files.createDirectories(cacheDirectory);
    }
    catch(IOException ioe)
    {
      // TODO: Error Handling.
    }

    return cacheDirectory.toFile();
  }

  public static File getImageCacheDirectory()
  {
    File cacheDirectory = new File(getCacheDirectory(), "image");

    if(!cacheDirectory.exists())
      cacheDirectory.mkdirs();

    return cacheDirectory;
  }
}
