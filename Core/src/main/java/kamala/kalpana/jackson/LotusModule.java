package kamala.kalpana.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;

import java.time.LocalDateTime;

public class LotusModule extends SimpleModule
{
  public LotusModule()
  {
    super.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
    super.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
  }
}
