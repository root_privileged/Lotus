package kamala.kalpana.connection.api;

public class FListAPIClient
{
  public static final String REQUEST_SCHEME = "https";
  public static final String HOST = "www.f-list.net";

  private static final String GROUP_QUERY_PATH = "/json/profile-groups.json";
  private static final String IMAGE_QUERY_PATH = "/json/profile-images.json";
  private static final String FRIEND_QUERY_PATH = "/json/profile-friends.json";

  private static final String PARAM_CID = "character_id";

  /*
  private static final ResponseHandler<String> STRING_RESPONSE_HANDLER = response ->
  {
    StatusLine status = response.getStatusLine();
    if(status.getStatusCode() >= 300)
    {
      throw new HttpResponseException(status.getStatusCode(), status.getReasonPhrase());
    }

    HttpEntity ent = response.getEntity();
    if(ent == null)
      throw new ClientProtocolException("Response Contained No Content!");

    ContentType contentType = ContentType.getOrDefault(ent);
    Charset charset = contentType.getCharset();
    Reader reader = new InputStreamReader(ent.getContent(), charset);

    StringWriter sw = new StringWriter();
    IOUtils.copy(reader, sw);
    return sw.toString();
  };

  public static String ExecuteRequest(String url, Map<String, String> parameters) throws IOException
  {
    try
    {
      URI uri = new URI(url);

      List<NameValuePair> formParams = parameters.entrySet().stream().map(entry -> new BasicNameValuePair(entry.getKey(), entry.getValue())).collect(Collectors.toList());

      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, STRING_RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  public static ImageReferenceData GetImageReferences(CharacterProfile forCharacter)
  {
    try
    {
      URI uri = new URIBuilder()
              .setScheme(REQUEST_SCHEME)
              .setHost(HOST)
              .setPath(IMAGE_QUERY_PATH)
              .build();

      List<NameValuePair> formParams = new ArrayList<>();
      formParams.add(new BasicNameValuePair(PARAM_CID, forCharacter.getId() + ""));


      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, ImageReferenceData.RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    } catch (ClientProtocolException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  public static FriendData GetFriendInformation(CharacterProfile forCharacter)
  {
    try
    {
      URI uri = new URIBuilder()
              .setScheme(REQUEST_SCHEME)
              .setHost(HOST)
              .setPath(FRIEND_QUERY_PATH)
              .build();

      List<NameValuePair> formParams = new ArrayList<>();
      formParams.add(new BasicNameValuePair(PARAM_CID, forCharacter.getId() + ""));


      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, FriendData.RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    } catch (ClientProtocolException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  public static GroupData GetGroupInformation(CharacterProfile forCharacter)
  {
    try
    {
      URI uri = new URIBuilder()
              .setScheme(REQUEST_SCHEME)
              .setHost(HOST)
              .setPath(GROUP_QUERY_PATH)
              .build();

      List<NameValuePair> formParams = new ArrayList<>();
      formParams.add(new BasicNameValuePair(PARAM_CID, forCharacter.getId() + ""));


      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, GroupData.RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    } catch (ClientProtocolException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    return null;
  }*/
}
