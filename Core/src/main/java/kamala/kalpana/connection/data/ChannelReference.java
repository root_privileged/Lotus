package kamala.kalpana.connection.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.packet.server.internal.InternalPrivateChannel;
import kamala.kalpana.connection.packet.server.internal.InternalPublicChannel;

public class ChannelReference
{
  public static final String PRIVATE_CHANNEL = "PRIVATE";

  private ChatContext chatContext;

  private String title;
  private String identifier;

  private StringProperty description;
  private StringProperty mode;
  private IntegerProperty characters;

  private ChannelReference()
  {
    description = new SimpleStringProperty(this, "description", "");
    characters = new SimpleIntegerProperty(this, "characters", 0);
    mode = new SimpleStringProperty(this, "mode", "");
  }

  // CTor for public channels.
  public ChannelReference(ChatContext chatContext, String title, String description)
  {
    this(chatContext, title, title, description);
  }

  // CTor for private channels.
  public ChannelReference(ChatContext chatContext, String title, String identifier, String description)
  {
    this();

    this.chatContext = chatContext;
    this.title = title;
    this.identifier = identifier;

    this.description.set(description);
    mode.set(PRIVATE_CHANNEL);
  }

  public ChannelReference(ChatContext chatContext, InternalPublicChannel ipc)
  {
    this(chatContext, ipc.getName(), ipc.getName(), "");

    this.characters.set(ipc.getCharacters());
    this.mode.set(ipc.getMode());
  }

  public ChannelReference(ChatContext chatContext, InternalPrivateChannel ipc)
  {
    this(chatContext, ipc.getTitle(), ipc.getName(), "");

    this.characters.set(ipc.getCharacters());
    this.mode.set(PRIVATE_CHANNEL);
  }

  public final boolean isPrivateChannel()
  {
    return !identifier.equals(title);
  }

  public final ChatContext getChatContext()
  {
    return chatContext;
  }

  public final String getTitle()
  {
    return title;
  }

  public final String getIdentifier()
  {
    return identifier;
  }


  public final StringProperty descriptionProperty()
  {
    return description;
  }
  public final String getDescription()
  {
    return description.get();
  }
  public final void setDescription(String desc)
  {
    description.set(desc);
  }

  public final StringProperty modeProperty()
  {
    return mode;
  }
  public final String getMode()
  {
    return mode.get();
  }
  public final void setMode(String newMode)
  {
    this.mode.set(newMode);
  }

  public final IntegerProperty charactersProperty()
  {
    return characters;
  }
  public final int getCharacters()
  {
    return characters.get();
  }
}
