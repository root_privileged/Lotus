package kamala.kalpana.connection.command;

import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.data.CharacterReference;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class AbstractCommand
{
  protected static final Logger cmdLogger = LoggerFactory.getLogger(AbstractCommand.class);

  private static Reflections reflector =
          new Reflections(new ConfigurationBuilder()
                  .setUrls(ClasspathHelper.forPackage("kamala.kalpana.connection.command"))
                  .setScanners(
                          new SubTypesScanner(),
                          new TypeAnnotationsScanner())
          );
  private static Map<String, Class<? extends AbstractCommand>> commandCache = new HashMap<>();

  public static void InitializeCommands()
  {
    Set<Class<? extends AbstractCommand>> cmds = reflector.getSubTypesOf(AbstractCommand.class);

    cmds.stream()
            .filter(clz -> clz.isAnnotationPresent(CommandIdentifier.class))
            .forEach((clz) -> {
              CommandIdentifier cid = getIdentifier(clz);

              commandCache.put(cid.Command(), clz);
            });

    cmdLogger.info("Commands Cached: " + commandCache.size());
  }

  public static CommandIdentifier getIdentifier(Class<? extends AbstractCommand> cmd)
  {
    if (cmd.isAnnotationPresent(CommandIdentifier.class))
      return cmd.getAnnotation(CommandIdentifier.class);
    else return null;
  }

  public static boolean hasCommand(String cmd)
  {
    return commandCache.containsKey(cmd);
  }

  public static Optional<AbstractCommand> getCommandHandler(String cmd)
  {
    try
    {
      return Optional.of(commandCache.get(cmd).newInstance());
    }
    catch (InstantiationException e)
    {
      // TODO: Better error handling.
      e.printStackTrace();
    }
    catch (IllegalAccessException e)
    {
      // TODO: Better error handling.
      e.printStackTrace();
    }

    return Optional.empty();
  }

  /**
   * Override to enable/disable usage of this command on the console tab in the UI environment
   * this defaults to true.
   *
   * @param chatContext
   * @param arguments
   * @return
   */
  public boolean isConsoleUsable(ChatContext chatContext, List<String> arguments)
  {
    return true;
  }

  /**
   * Override to enable/disable usage of this command in channel tabs in the UI environment.
   *
   * @param chatContext The chat context this command was executed within.
   * @param channelContext
   * @param characterReference
   * @param arguments
   * @return
   */
  public boolean isChannelUsable(ChatContext chatContext, ChannelContext channelContext, CharacterReference characterReference, List<String> arguments)
  {
    if(characterReference.isOperator() || characterReference.isChannelModerator(channelContext)) return true;

    return false;
  }

  public boolean isConversationUsable(ChatContext chatContext, CharacterReference characterReference, List<String> arguments)
  {
    if(characterReference.isOperator()) return true;

    return false;
  }

  public abstract boolean execute(ChatContext chatContext, ChannelContext channel, CharacterReference source, List<String> arguments);
}
