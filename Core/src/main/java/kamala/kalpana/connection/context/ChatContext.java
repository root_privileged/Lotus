package kamala.kalpana.connection.context;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.Subscribe;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import kamala.kalpana.LotusCore;
import kamala.kalpana.connection.FChatSocketClient;
import kamala.kalpana.connection.actions.AbstractResponderAction;
import kamala.kalpana.connection.actions.OnChannelJoin;
import kamala.kalpana.connection.actions.OnPrivateChannelList;
import kamala.kalpana.connection.actions.OnPublicChannelList;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.connection.data.Status;
import kamala.kalpana.connection.packet.IPacketReceiver;
import kamala.kalpana.connection.packet.client.IdentificationRequest;
import kamala.kalpana.connection.packet.client.PrivateChannelListRequest;
import kamala.kalpana.connection.packet.client.PublicChannelListRequest;
import kamala.kalpana.connection.packet.server.*;
import kamala.kalpana.javafx.property.FilteredMapChangeListener;
import kamala.kalpana.javafx.property.IntegerMapListeningProperty;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

// Individual character chat context.
public class ChatContext implements IPacketReceiver, ChangeListener<FChatSocketClient.SocketState>
{
  private static final String NO_SSL_URL = "ws://chat.f-list.net:9722";
  private static final String SSL_URL = "wss://chat.f-list.net:9799";
  private static final String TEST_URL = "ws://chat.f-list.net:8722";

  private String characterName;
  private FChatContext parentContext;
  private FChatSocketClient socketClient;
  private AsyncEventBus actionBus;

  private boolean botMode;

  // Character Name to Character References
  private IntegerMapListeningProperty characterCount;
  private ObservableMap<String, CharacterReference> characterReferences;

  // Channel Identifier (Not Title) to Channel References
  private IntegerMapListeningProperty publicChannelCount;
  private IntegerMapListeningProperty privateChannelCount;
  private ObservableMap<String, ChannelReference> channelReferences;

  private ObservableMap<ChannelReference, ChannelContext> channelContexts;

  private ObservableList<String> chatOperators;

  private ChatContext()
  {
    this.characterReferences = FXCollections.observableMap(new TreeMap<>(String.CASE_INSENSITIVE_ORDER));
    this.channelReferences = FXCollections.observableHashMap();
    this.chatOperators = FXCollections.observableArrayList();
    this.channelContexts = FXCollections.observableHashMap();

    this.actionBus = new AsyncEventBus(Executors.newCachedThreadPool(LotusCore.DAEMON_THREAD_FACTORY), (exception, context) -> {
      exception.printStackTrace();
    });

    //this.actionBus = new EventBus();
    getActionBus().register(this);

    characterCount = new IntegerMapListeningProperty(this, "characters", 0);
    publicChannelCount = new IntegerMapListeningProperty(this, "publicChannels", 0);
    privateChannelCount = new IntegerMapListeningProperty(this, "privateChannels", 0);

    // Auto update chat operators when oplist is modified
    getChatOperators().addListener((ListChangeListener<String>) c -> {
      while(c.next())
      {
        if(c.wasRemoved())
        {
          c.getRemoved().stream().filter(rem -> getCharacters().containsKey(rem)).forEach(rem -> {
            getCharacters().get(rem).setOperator(false);
          });
        }
        else if(c.wasAdded())
        {
          c.getAddedSubList().stream().filter(rem -> getCharacters().containsKey(rem)).forEach(rem -> {
            getCharacters().get(rem).setOperator(true);
          });
        }
      }
    });

    getCharacters().addListener(characterCount);
    getCharacters().addListener((MapChangeListener<String, CharacterReference>) change -> {
      if (change.wasAdded())
      {
        CharacterReference cr = change.getValueAdded();
        cr.setOperator(getChatOperators().contains(cr.getName()));
      }
    });

    getChannels().addListener(new FilteredMapChangeListener(o -> !((ChannelReference) o).isPrivateChannel(), publicChannelCount));
    getChannels().addListener(new FilteredMapChangeListener(o -> ((ChannelReference) o).isPrivateChannel(), privateChannelCount));
  }

  private ChatContext(FChatContext parentContext, String characterName, FChatSocketClient socketClient, boolean botMode)
  {
    this();

    this.characterName = characterName;
    this.parentContext = parentContext;
    this.socketClient = socketClient;

    this.botMode = botMode;

    this.socketClient.stateProperty().addListener(this);
    this.socketClient.receiverBus().register(this);
  }
  public static ChatContext createContext(FChatContext parentContext, String characterName)
  {
    // TODO: Defaults to SSL server.
    return createContext(parentContext, characterName, SSL_URL);
  }

  public static ChatContext createContext(FChatContext parentContext, String characterName, boolean botMode)
  {
    // TODO: Defaults to SSL server.
    return createContext(parentContext, characterName, SSL_URL, botMode);
  }

  public static ChatContext createContext(FChatContext parentContext, String characterName, String url)
  {
    return createContext(parentContext, characterName, url, false);
  }

  public static ChatContext createContext(FChatContext parentContext, String characterName, String url, boolean botMode)
  {
    try
    {
      FChatSocketClient client = new FChatSocketClient(new URI(url), url.contains("wss"));
      ChatContext ctx = new ChatContext(parentContext, characterName, client, botMode);
      client.setContext(ctx);

      return ctx;
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  @JsonIgnore
  public final boolean isBot()
  {
    return botMode;
  }

  @JsonIgnore
  public final ObservableMap<String, CharacterReference> getCharacters()
  {
    return characterReferences;
  }

  @JsonIgnore
  public final ObservableMap<String, ChannelReference> getChannels()
  {
    return channelReferences;
  }

  @JsonIgnore
  public final ObservableList<String> getChatOperators()
  {
    return chatOperators;
  }

  @JsonIgnore
  public final ObservableMap<ChannelReference, ChannelContext> getChannelContexts()
  {
    return channelContexts;
  }

  public final String getCharacterName()
  {
    return characterName;
  }

  public final CharacterReference getCharacterReference() { return getCharacters().get(getCharacterName()); }

  public AsyncEventBus getActionBus() { return actionBus; }
  public <T extends AbstractResponderAction> void onAction(T act)
  {
    getActionBus().post(act);
  }


  public final FChatSocketClient getSocketClient()
  {
    return socketClient;
  }

  public ObjectProperty<FChatSocketClient.SocketState> getConnectionState()
  {
    return socketClient.stateProperty();
  }

  @Subscribe
  public void onOperatorList(OperatorList opList)
  {
    Platform.runLater(() -> getChatOperators().addAll(opList.getOperators()));
  }

  @Subscribe
  public void onOperatorAdded(ChatOpPromotion cop)
  {
    Platform.runLater(() -> getChatOperators().add(cop.getCharacter()));
  }

  @Subscribe
  public void onOperatorRemoved(ChatOpDemotion cod)
  {
    Platform.runLater(() -> getChatOperators().remove(cod.getCharacter()));
  }

  // We need the channel lists always.
  @Subscribe
  public void onIdentified(IdentificationResponse idnResponse)
  {
    getSocketClient().sendCommand(new PublicChannelListRequest());
    getSocketClient().sendCommand(new PrivateChannelListRequest());
  }

  @Subscribe
  public void onCharacterList(OnlineCharacterList ocl)
  {
    getCharacters().putAll(ocl.getCharacters().stream()
            .map((l) -> new CharacterReference(this, l))
            .collect(Collectors.toMap(CharacterReference::getName, Function.identity())));
  }

  @Subscribe
  public void onCharacterConnected(UserConnected ucnt)
  {
    getCharacters().put(ucnt.getIdentity(), new CharacterReference(this, ucnt.getIdentity(), ucnt.getGender(), ucnt.getStatus()));
  }

  @Subscribe
  public void onCharacterDisconnected(CharacterOffline co)
  {
    getCharacters().remove(co.getCharacter());
  }

  @Subscribe
  public void onCharacterStatusChange(UserStatusChange usc)
  {
    CharacterReference cr = getCharacters().get(usc.getCharacter());
    cr.setStatus(Status.fromString(usc.getStatus()));
    cr.setStatusMessage(usc.getStatusmsg());
  }

  @Subscribe
  public void onChannelJoined(OnChannelJoin ocj)
  {
    channelContexts.put(channelReferences.get(ocj.getChannelID()), ocj.getChannelContext());
  }

  @Subscribe
  public void onPublicChannelList(OnPublicChannelList opcl)
  {
    getChannels().putAll(opcl.getChannels().stream().collect(Collectors.toMap(ChannelReference::getIdentifier, Function.identity())));
  }

  @Subscribe
  public void onPrivateChannelList(OnPrivateChannelList opcl)
  {
    getChannels().putAll(opcl.getChannels().stream().collect(Collectors.toMap(ChannelReference::getIdentifier, Function.identity())));
  }

  @Subscribe
  public void onChannelDescriptionUpdate(ChannelDescriptionUpdate cdu)
  {
    getChannels().get(cdu.getChannel()).setDescription(cdu.getDescription());
  }

  public final IntegerProperty characterCountProperty()
  {
    return characterCount;
  }

  public final IntegerProperty publicChannelCountProperty()
  {
    return publicChannelCount;
  }

  public final IntegerProperty privateChannelCountProperty()
  {
    return privateChannelCount;
  }

  @Override
  public void changed(ObservableValue<? extends FChatSocketClient.SocketState> observable, FChatSocketClient.SocketState oldValue, FChatSocketClient.SocketState newValue)
  {
    switch(newValue)
    {
      case UNCONNECTED:
        // TODO: Should I bother notifying about unconnected state?
        break;
      case OPENED:
        getSocketClient().sendCommand(new IdentificationRequest(parentContext.getTicket(), characterName));
        break;
      case CLOSED:
        // TODO: Notify disconnected.
        System.out.println("Connection Disconnected");
        break;
      case ERROR:
        // TODO: Notify Error
        System.out.println("Connection Error");
        break;
    }
  }
}
