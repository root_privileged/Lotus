package kamala.kalpana.connection.context;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kamala.kalpana.connection.data.ChannelMode;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.connection.packet.server.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.stream.Collectors;

public class ChannelContext implements Closeable
{
  private static final Logger logger = LoggerFactory.getLogger(ChannelContext.class);

  private ChatContext chatContext;
  private ChannelReference channelReference;

  private StringProperty owner;
  private StringProperty description;
  private ObjectProperty<ChannelMode> mode;
  // TODO: Mode Property
  private ObservableList<String> moderators;

  private ListProperty<CharacterReference> charactersProperty;
  private ObservableList<CharacterReference> characters;

  public ChannelContext(ChatContext chatContext, ChannelReference channelReference)
  {
    this.chatContext = chatContext;
    this.channelReference = channelReference;

    this.owner = new SimpleStringProperty(this, "owner", "");
    this.description = new SimpleStringProperty(this, "description", "");
    this.mode = new SimpleObjectProperty<>(this, "mode", ChannelMode.Chat);
    this.moderators = FXCollections.observableArrayList();

    this.characters = FXCollections.observableArrayList();
    this.charactersProperty = new SimpleListProperty<>(this, "characters", characters);

    getChatContext().getActionBus().register(this);
    getChatContext().getSocketClient().senderBus().register(this);
    getChatContext().getSocketClient().receiverBus().register(this);
  }

  @Override
  public void close() throws IOException
  {
    getChatContext().getActionBus().unregister(this);
    getChatContext().getSocketClient().senderBus().unregister(this);
    getChatContext().getSocketClient().receiverBus().unregister(this);
  }

  public ReadOnlyIntegerProperty characterCountProperty()
  {
    return charactersProperty.sizeProperty();
  }

  public final ChatContext getChatContext()
  {
    return chatContext;
  }
  public final ChannelReference getReference()
  {
    return channelReference;
  }

  public final StringProperty ownerProperty()
  {
    return owner;
  }
  public final String getOwner()
  {
    return owner.get();
  }
  private final void setOwner(String owner)
  {
    this.owner.set(owner);
  }

  public final ObjectProperty<ChannelMode> modeProperty() { return mode; }
  public final ChannelMode getMode() { return mode.get(); }
  private final void setMode(ChannelMode desc) { this.mode.set(desc);}

  public final StringProperty descriptionProperty() { return description; }
  public final String getDescription() { return description.get(); }
  private final void setDescription(String desc) { this.description.set(desc);}

  // Read-Only List
  public final ObservableList<String> getModerators()
  {
    return moderators;
  }

  public final ObservableList<CharacterReference> getCharacters()
  {
    return characters;
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onChannelOperatorList(ChannelOperatorList col)
  {
    if(col.isValid(getReference()))
    {
      logger.trace("Received Channel Operator List for Channel {}", getReference().getTitle());

      setOwner(col.getOwner());
      getModerators().addAll(col.getModerators());
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onChannelModeChange(ChangeRoomMode crm)
  {
    if(crm.isValid(getReference()))
    {
      logger.trace("Received Mode Change for Channel {}", getReference().getTitle());

      mode.setValue(ChannelMode.fromString(crm.getMode()));
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onChannelOperatorPromotion(ChannelOpPromotion cop)
  {
    if(cop.isValid(getReference()))
    {
      logger.trace("Received Channel Operator Promotion for Channel {} for Character {}",
              getReference().getTitle(), cop.getCharacter());

      getModerators().add(cop.getCharacter());
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onChannelOperatorDemotion(ChannelOpDemotion cod)
  {
    if(cod.isValid(getReference()))
    {
      logger.trace("Received Channel Operator Demotion for Channel {} for Character {}",
              getReference().getTitle(), cod.getCharacter());

      getModerators().remove(cod.getCharacter());
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onChannelDescriptionUpdate(ChannelDescriptionUpdate cdu)
  {
    if(cdu.isValid(getReference()))
    {
      logger.trace("Received Channel Description Update for Channel {}", getReference().getTitle());

      description.set(cdu.getDescription());
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onChannelData(InitialChannelData icd)
  {
    if(icd.isValid(getReference()))
    {
      logger.trace("Received Channel Data for Channel {}", icd.getChannel());

      characters.addAll(icd.getUsers().stream()
              .map(iu -> getChatContext().getCharacters().get(iu.getIdentity())).collect(Collectors.toList()));
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onCharacterJoin(CharacterJoinChannel ucj)
  {
    if(ucj.isValid(getReference()))
    {
      logger.trace("Character {} joined Channel {}.", ucj.getCharacter().getIdentity(), ucj.getChannel());

      String ident = ucj.getCharacter().getIdentity();

      if(getChatContext().getCharacters().containsKey(ident))
      {
        if(characters.add(getChatContext().getCharacters().get(ident)))
        {
          logger.trace("Character {} successfully added to channel {}.", ucj.getCharacter().getIdentity(), ucj.getChannel());
        }
        else
          logger.error("Character {} was not successfully added to channel {}.", ucj.getCharacter().getIdentity(), ucj.getChannel());
      }
      else
        logger.error("Character {} was not found... Wtf?", ucj.getCharacter());
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onCharacterLeave(CharacterLeftChannel clc)
  {
    logger.trace("Character {} left Channel {}.", clc.getCharacter(), clc.getChannel());
    characters.remove(getChatContext().getCharacters().get(clc.getCharacter()));
  }
}
