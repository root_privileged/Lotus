package kamala.kalpana.connection.context;

import kamala.kalpana.connection.api.response.LoginTicket;

import java.util.HashMap;
import java.util.Map;

// Master context that is created after login to manage Chat Contexts.
public class FChatContext
{
  private static final Object lock = new Object();
  private static FChatContext instance;

  private LoginTicket ticket;
  private Map<String, ChatContext> chatContexts;

  private FChatContext(LoginTicket ticket)
  {
    this.ticket = ticket;
    this.chatContexts = new HashMap<>();
  }

  public ChatContext getChatContext(String forCharacter)
  {
    return getChatContext(forCharacter, false);
  }

  public ChatContext getChatContext(String forCharacter, boolean botMode)
  {
    ChatContext ctx = ChatContext.createContext(this, forCharacter, botMode);

    chatContexts.put(forCharacter, ctx);

    return ctx;
  }

  /*
  public void createChatWindow(String forCharacter)
  {
    ChatContext chatCtx = getChatContext(forCharacter);
    LotusChatController lcc = new LotusChatController(this, chatCtx);

    SeamlessWindow sWind = SeamlessWindow.buildWithController("/fxml/lotus_chat.fxml", lcc, 1366, 768, forCharacter, 5.0, true);

    lcc.setWindow(sWind);

    Runnable r = () -> {
      try
      {
        chatCtx.getSocketClient().connectBlocking();
        sWind.getStage().show();
      }
      catch (NumberFormatException nfx)
      {
        // TODO: Server is Full (And this is how we know.. For some reason)
        // TODO: This doesn't seem to actually work?
      }
      catch (InterruptedException e)
      {
        // TODO: Better error handling here, this is a crucial point.
        e.printStackTrace();
      }

    };

    r.run();
  }*/

  public LoginTicket getTicket()
  {
    return ticket;
  }

  public static FChatContext createContext(LoginTicket loginTicket)
  {
    FChatContext fcc = new FChatContext(loginTicket);

    synchronized (lock)
    {
      if(instance == null)
        instance = fcc;
    }

    return getContext();
  }

  public static FChatContext getContext()
  {
    return instance;
  }
}
