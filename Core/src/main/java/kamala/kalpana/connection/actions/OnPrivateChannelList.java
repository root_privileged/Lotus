package kamala.kalpana.connection.actions;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.packet.ActionIdentifier;
import kamala.kalpana.connection.packet.server.ChannelListPrivate;

import java.util.List;
import java.util.stream.Collectors;

@ActionIdentifier(OnCommand = "ORS")
public class OnPrivateChannelList extends AbstractResponderAction
{
  private ChannelListPrivate response;
  private List<ChannelReference> channels;

  @Subscribe
  @AllowConcurrentEvents
  public void onPrivateChannelList(ChannelListPrivate clp)
  {
    this.response = clp;
    this.channels = clp.getChannels().stream()
            .map(ipc -> new ChannelReference(getContext(), ipc)).collect(Collectors.toList());

    getContext().onAction(this);
    unregisterListener();
  }

  public final ChannelListPrivate getResponse()
  {
    return response;
  }

  public final List<ChannelReference> getChannels()
  {
    return channels;
  }
}
