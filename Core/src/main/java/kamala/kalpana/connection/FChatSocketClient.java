package kamala.kalpana.connection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.AsyncEventBus;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import kamala.kalpana.LotusCore;
import kamala.kalpana.connection.actions.AbstractResponderAction;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.packet.*;
import kamala.kalpana.connection.packet.client.PingRequest;
import kamala.kalpana.connection.packet.server.Ping;
import kamala.kalpana.connection.packet.server.ServerVariable;
import org.java_websocket.client.DefaultSSLWebSocketClientFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

public class FChatSocketClient extends WebSocketClient implements IPacketReceiver
{
  protected static final Logger socketLogger = LoggerFactory.getLogger(FChatSocketClient.class);

  public enum SocketState
  {
    UNCONNECTED,
    OPENED,
    CLOSED,
    ERROR
  }

  private ObjectProperty<SocketState> currentState;
  private AsyncEventBus messageReceiverBus;
  private AsyncEventBus messageSenderBus;

  private Map<String, Instant> lastReceived;
  private Map<String, Instant> lastSent;

  private ObservableMap<String, Float> connectionVars;

  private ChatContext chatContext;

  public FChatSocketClient(URI serverUri)
  {
    this(serverUri, false);
  }

  public FChatSocketClient(URI serverUri, boolean useSSL)
  {
    super(serverUri, new Draft_10());

    connectionVars = FXCollections.observableHashMap();
    lastReceived = new HashMap<>();
    lastSent = new HashMap<>();

    currentState = new SimpleObjectProperty<>(this, "state", SocketState.UNCONNECTED);

    // TODO: use an event bus factory to handle the exception and context issues.
    messageReceiverBus = new AsyncEventBus(Executors.newCachedThreadPool(LotusCore.DAEMON_THREAD_FACTORY), (exception, context) -> {
      exception.printStackTrace();
    });
    messageSenderBus = new AsyncEventBus(Executors.newCachedThreadPool(LotusCore.DAEMON_THREAD_FACTORY), (exception, context) -> {
      exception.printStackTrace();
    });

    messageSenderBus.register(this);
    messageReceiverBus.register(this);

    if(useSSL)
    {
      SSLContext sslContext = null;

      try
      {
        sslContext = SSLContext.getInstance( "TLS" );
        sslContext.init( null, null, null );
      }
      catch (NoSuchAlgorithmException e)
      {
        e.printStackTrace();
      }
      catch (KeyManagementException e)
      {
        e.printStackTrace();
      }

      this.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sslContext));
    }

    // For annoying debugging times.
    //WebSocketImpl.DEBUG = true;
  }

  @JsonIgnore
  public final ObservableMap<String, Float> getVariables()
  {
    return connectionVars;
  }

  public final void markSent(String identifier)
  {
    lastSent.put(identifier, Instant.now());
  }
  public final void markReceived(String identifier)
  {
    lastReceived.put(identifier, Instant.now());
  }

  public final Instant lastReceived(String identifier)
  {
    if(lastReceived.containsKey(identifier))
      return lastReceived.get(identifier);
    else return null;
  }

  public final Instant lastSent(String identifier)
  {
    if(lastSent.containsKey(identifier))
      return lastSent.get(identifier);
    else return null;
  }

  public final ChatContext getContext()
  {
    return chatContext;
  }
  public void setContext(ChatContext ctx)
  {
    chatContext = ctx;
  }

  public ObjectProperty<SocketState> stateProperty()
  {
    return currentState;
  }
  public SocketState getState()
  {
    return stateProperty().get();
  }

  public AsyncEventBus receiverBus()
  {
    return messageReceiverBus;
  }

  public AsyncEventBus senderBus()
  {
    return messageSenderBus;
  }

  public <T extends ClientPacket> void sendCommand(T cmd)
  {
    senderBus().post(cmd);
  }

  public void handleSendRaw(String rawCommand)
  {
    AbstractResponderAction aa = AbstractResponderAction.parseActionFrom(rawCommand.substring(0, 3), getContext());

    if(aa != null)
      receiverBus().register(aa);

    super.send(rawCommand);
  }

  @Override
  @AllowConcurrentEvents
  public void handleClientCommand(ClientPacket cmd)
  {
    if(cmd.getClass().isAnnotationPresent(PacketIdentifier.class))
    {
      PacketIdentifier cid = cmd.getClass().getAnnotation(PacketIdentifier.class);

      AbstractResponderAction aa = AbstractResponderAction.parseActionFrom(cid.Identifier(), getContext());

      if(aa != null)
        receiverBus().register(aa);

      try
      {
        if(cid.HeaderOnly())
          send(cid.Identifier());
        else
          send(cid.Identifier() + " " + cmd.getData());

        markSent(cid.Identifier());
      } catch (JsonProcessingException e)
      {
        e.printStackTrace();
      }
    }
  }

  @Override
  @AllowConcurrentEvents
  public void handleServerCommand(ServerPacket serverPacket)
  {
    if(serverPacket instanceof Ping)
    {
       sendCommand(new PingRequest());
    }
    else if(serverPacket instanceof ServerVariable)
    {
      ServerVariable sv = (ServerVariable) serverPacket;
      connectionVars.put(sv.getVariable(), sv.getValue());
    }
  }

  @Override
  public void onOpen(ServerHandshake handshakedata)
  {
    currentState.set(SocketState.OPENED);
  }

  @Override
  public void onMessage(String message)
  {
    ServerPacket serverPacket = AbstractPacket.parsePacket(message);

    serverPacket.setSocketClient(this);

    markReceived(message.substring(0, 3));

    messageReceiverBus.post(serverPacket);
  }

  @Override
  public void onClose(int code, String reason, boolean remote)
  {
    currentState.set(SocketState.CLOSED);
  }

  @Override
  public void onError(Exception ex)
  {
    currentState.set(SocketState.ERROR);
    ex.printStackTrace();
  }
}
