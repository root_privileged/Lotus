package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

// TODO: Annoying.
@PacketIdentifier(Identifier = "IGN", Description = "Ignore Commands")
public class IgnoreRequest extends ClientPacket
{
  private String action;
  private String character;

  public IgnoreRequest(String action, String character)
  {
    this.action = action;
    this.character = character;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("action")
  public String getAction()
  {
    return action;
  }
  public void setAction(String action)
  {
    this.action = action;
  }
}
