package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "RLD", Description = "Reloads certain server configuration files.")
public class ReloadConfigurationRequest extends ClientPacket
{
  private String save;

  public ReloadConfigurationRequest(String save)
  {
    this.save = save;
  }

  @JsonProperty("save")
  public final String getSave()
  {
    return save;
  }
  private void setSave(String newValue)
  {
    save = newValue;
  }
}
