package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "CON", Description = "Gives a count of connected users..")
public class ConnectedUsers extends ServerPacket
{
  private int count;

  @JsonProperty("message")
  public final int getCount()
  {
    return count;
  }
  private void setCount(int count)
  {
    this.count = count;
  }

  @Override
  public String toString()
  {
    return "CON: Count: " + getCount();
  }
}
