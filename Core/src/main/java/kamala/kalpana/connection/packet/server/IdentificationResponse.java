package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "IDN", Description = "Informs the user that the character they are logged on with successfully identified.")
public class IdentificationResponse extends ServerPacket
{
  private String character;

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @Override
  public String toString()
  {
    return "IDN: Character: " + getCharacter();
  }
}
