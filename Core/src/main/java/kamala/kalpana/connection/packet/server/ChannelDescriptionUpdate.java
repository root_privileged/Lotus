package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CDS", Description = "Notifies the client that the specified channels description has changed")
public class ChannelDescriptionUpdate extends AbstractChannelPacket
{
  private String description;

  @JsonProperty("description")
  public final String getDescription()
  {
    return description;
  }
  private void setDescription(String newValue)
  {
    description = newValue;
  }

  @Override
  public String toString()
  {
    return "CDS: Channel: " + getChannel() + " Description: " + getDescription();
  }
}
