package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CSO", Description = "Sets the owner of the specified channel to the character provided.")
public class SetChannelOwner extends AbstractChannelPacket
{
  private String character;

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @Override
  public String toString()
  {
    return "CSO: Channel: " + getChannel() + " Character: " + getCharacter();
  }
}
