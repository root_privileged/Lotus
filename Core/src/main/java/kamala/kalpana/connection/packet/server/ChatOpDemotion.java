package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "DOP", Description = "Demotes the specified character of chat operator status.")
public class ChatOpDemotion extends ServerPacket
{
  private String character;

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @Override
  public String toString()
  {
    return "DOP: Character: " + getCharacter();
  }
}
