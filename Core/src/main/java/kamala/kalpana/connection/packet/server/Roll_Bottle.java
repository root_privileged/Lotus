package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

import java.util.List;

@PacketIdentifier(Identifier = "RLL", Description = "Dice roll packet or bottle spin packet.")
public class Roll_Bottle extends ServerPacket
{
  private String character;
  private String message;
  private String channel;
  private String type;

  @JsonProperty("character")
  public String getCharacter()
  {
    return character;
  }
  private void setCharacter(String character)
  {
    this.character = character;
  }

  @JsonProperty("message")
  public String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  private void setChannel(String channel)
  {
    this.channel = channel;
  }

  @JsonProperty("type")
  public String getType()
  {
    return type;
  }
  private void setType(String type)
  {
    this.type = type;
  }

  public List<String> getRolls()
  {
    return (List<String>) getUnknownProperty("rolls");
  }

  public int getEndResult()
  {
    return ((Integer) getUnknownProperty("endresult")).intValue();
  }

  public List<Integer> getResults()
  {
    return (List<Integer>) getUnknownProperty("results");
  }

  public String getTarget()
  {
    return (String) getUnknownProperty("target");
  }

  @Override
  public String toString()
  {
    return "RLL: Unimplemented";
  }
}
