package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.server.UserTypingStatus;

@PacketIdentifier(Identifier = "TPN", Description = "Notifies the server that you have started/stopped typing.")
public class TypingNotificationRequest extends ClientPacket
{
  private String character;
  private UserTypingStatus.TypingStatus status;

  public TypingNotificationRequest(String character)
  {
    this.character = character;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("status")
  public String getStatus()
  {
    switch(status)
    {
      case Clear:
        return "clear";
      case Paused:
        return "paused";
      case Typing:
        return "typing";
    }

    return "clear";
  }
  public void setStatus(UserTypingStatus.TypingStatus status)
  {
    this.status = status;
  }
}
