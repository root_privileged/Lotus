package kamala.kalpana.connection.packet;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PacketIdentifier
{
  public String Identifier();
  public String Description() default "";
  public boolean HeaderOnly() default false;
}
