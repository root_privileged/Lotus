package kamala.kalpana.connection.packet.client;

import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CHA", HeaderOnly = true, Description = "Requests a list of all public channels.")
public class PublicChannelListRequest extends ClientPacket
{}
