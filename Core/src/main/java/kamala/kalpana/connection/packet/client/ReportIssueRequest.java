package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

// TODO: Simulate webclient way of submitting reports.
//  It is suspected that third-party clients cannot upload logs.
@PacketIdentifier(Identifier = "SFC", Description = "Alerts admins and chat operators of an issue.")
public class ReportIssueRequest extends ClientPacket
{
  private String action;
  private String report;
  private String character;

  public ReportIssueRequest(String action, String report, String character)
  {
    this.action = action;
    this.report = report;
    this.character = character;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("report")
  public String getReport()
  {
    return report;
  }
  public void setReport(String report)
  {
    this.report = report;
  }
}
