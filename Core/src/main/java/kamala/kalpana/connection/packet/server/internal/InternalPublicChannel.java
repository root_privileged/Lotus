package kamala.kalpana.connection.packet.server.internal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InternalPublicChannel
{
  private String name;
  private String mode;
  private int characters;

  @JsonProperty("characters")
  public int getCharacters()
  {
    return characters;
  }
  private void setCharacters(int characters)
  {
    this.characters = characters;
  }

  @JsonProperty("mode")
  public String getMode()
  {
    return mode;
  }
  private void setMode(String mode)
  {
    this.mode = mode;
  }

  @JsonProperty("name")
  public String getName()
  {
    return name;
  }
  private void setName(String name)
  {
    this.name = name;
  }
}
