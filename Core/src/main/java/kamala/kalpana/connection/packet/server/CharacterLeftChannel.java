package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "LCH", Description = "Notifies that a character has left the specified channel, also can be your own character..")
public class CharacterLeftChannel extends AbstractChannelPacket
{
  private String character;

  @JsonProperty("character")
  public String getCharacter()
  {
    return character;
  }
  private void setCharacter(String character)
  {
    this.character = character;
  }

  @Override
  public String toString()
  {
    return "LCH: Channel: " + getChannel() + " Character: " + getCharacter();
  }
}
