package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

import java.util.List;

@PacketIdentifier(Identifier = "LIS", Description = "Sends an array of all online character names, genders, status, and status messages.")
public class OnlineCharacterList extends ServerPacket
{
  private List<List<String>> characters;

  @JsonProperty("characters")
  public List<List<String>> getCharacters()
  {
    return characters;
  }
  private void setCharacters(List<List<String>> characters)
  {
    this.characters = characters;
  }

  @Override
  public String toString()
  {
    return "LIS: Characters: " + getCharacters().size();
  }
}
