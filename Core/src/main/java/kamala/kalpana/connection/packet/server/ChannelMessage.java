package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "MSG", Description = "Channel message received from another user.")
public class ChannelMessage extends AbstractChannelPacket
{
  private String character;
  private String message;

  @JsonProperty("character")
  public String getCharacter()
  {
    return character;
  }
  private void setCharacter(String character)
  {
    this.character = character;
  }

  @JsonProperty("message")
  public String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }

  @Override
  public String toString()
  {
    return "MSG: Channel: " + getChannel() + " Character: " + getCharacter() + " Message: " + getMessage();
  }
}
