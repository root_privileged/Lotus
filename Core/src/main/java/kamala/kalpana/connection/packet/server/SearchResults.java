package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;
import kamala.kalpana.connection.packet.server.internal.InternalKink;

import java.util.List;

@PacketIdentifier(Identifier = "FKS", Description = "Search Results.")
public class SearchResults extends ServerPacket
{
  private List<String> characters;

  // the big kink thing here..
  private List<InternalKink> kinks;

  private List<String> genders;
  private List<String> roles;
  private List<String> orientations;
  private List<String> positions;
  private List<String> languages;

  @JsonProperty("characters")
  public final List<String> getCharacters() { return characters; }
  private void setCharacters(List<String> channels) { this.characters = channels; }

  @JsonProperty("kinks")
  public List<InternalKink> getKinks()
  {
    return kinks;
  }
  private void setKinks(List<InternalKink> kinks)
  {
    this.kinks = kinks;
  }

  @JsonProperty("genders")
  public List<String> getGenders()
  {
    return genders;
  }
  private void setGenders(List<String> genders)
  {
    this.genders = genders;
  }

  @JsonProperty("roles")
  public List<String> getRoles()
  {
    return roles;
  }
  private void setRoles(List<String> roles)
  {
    this.roles = roles;
  }

  @JsonProperty("orientations")
  public List<String> getOrientations()
  {
    return orientations;
  }
  private void setOrientations(List<String> orientations)
  {
    this.orientations = orientations;
  }

  @JsonProperty("positions")
  public List<String> getPositions()
  {
    return positions;
  }
  private void setPositions(List<String> positions)
  {
    this.positions = positions;
  }

  @JsonProperty("languages")
  public List<String> getLanguages()
  {
    return languages;
  }
  private void setLanguages(List<String> languages)
  {
    this.languages = languages;
  }

  @Override
  public String toString()
  {
    return "FKS: Unimplemented";
  }
}
