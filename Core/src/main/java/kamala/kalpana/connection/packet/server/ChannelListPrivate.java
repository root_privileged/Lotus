package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;
import kamala.kalpana.connection.packet.server.internal.InternalPrivateChannel;

import java.util.List;

@PacketIdentifier(Identifier = "ORS", Description = "List of all open private channels.")
public class ChannelListPrivate extends ServerPacket
{
  private List<InternalPrivateChannel> channels;

  @JsonProperty("channels")
  public final List<InternalPrivateChannel> getChannels() { return channels; }
  private void setChannels(List<InternalPrivateChannel> channels) { this.channels = channels; }

  @Override
  public String toString()
  {
    return "ORS: Channels: " + getChannels().size();
  }
}
