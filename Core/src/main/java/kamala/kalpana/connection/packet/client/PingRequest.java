package kamala.kalpana.connection.packet.client;

import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "PIN", HeaderOnly = true, Description = "Responds to the servers ping request.")
public class PingRequest extends ClientPacket
{}
