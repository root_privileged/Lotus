package kamala.kalpana.javafx.property;

import javafx.collections.MapChangeListener;

import java.util.function.Predicate;

public class FilteredMapChangeListener implements MapChangeListener
{
  private Predicate filterFunction;
  private MapChangeListener secondaryListener;

  public FilteredMapChangeListener(Predicate p, MapChangeListener secListener)
  {
    filterFunction = p;
    secondaryListener = secListener;
  }

  @Override
  public void onChanged(Change change)
  {
    if(change.wasAdded() && filterFunction.test(change.getValueAdded()))
      secondaryListener.onChanged(change);
    else if(change.wasRemoved() && filterFunction.test(change.getValueRemoved()))
      secondaryListener.onChanged(change);

  }
}
