package kamala.kalpana;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.sockjs.SockJSServer;
import io.vertx.ext.sockjs.SockJSServerOptions;

public class Mirror extends AbstractVerticle
{
  private HttpServer server;
  private SockJSServer sockJSServer;

  @Override
  public void start(Future<Void> startFuture) throws Exception
  {
    server = getVertx().createHttpServer();

    server.requestHandler(request -> {
      if (request.path().equalsIgnoreCase("/"))
        request.response().sendFile("index.html");
    });

    sockJSServer = SockJSServer.sockJSServer(getVertx(), server);
    sockJSServer.installApp(new SockJSServerOptions().setPrefix("/testapp"), socket -> {

      System.out.println("Socket Connected.");

      socket
              .endHandler(event1 -> System.out.println("WebSocket Closed"))
              .handler(event2 -> socket.write(event2));
    });

    server.listen(8080);

    super.start(startFuture);
  }

  @Override
  public void stop(Future<Void> stopFuture) throws Exception
  {
    sockJSServer.close();
    server.close();

    super.stop(stopFuture);
  }
}
