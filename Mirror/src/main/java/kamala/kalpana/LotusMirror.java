package kamala.kalpana;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LotusMirror
{
  public static void main(String[] args)
  {
    VertxOptions vxo = new VertxOptions();

    Vertx vertx = Vertx.vertx(vxo);

    Mirror m = new Mirror();

    vertx.deployVerticle(m);


    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    try
    {
      String r = br.readLine();

      if(r.equalsIgnoreCase("exit")) vertx.close();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
}
