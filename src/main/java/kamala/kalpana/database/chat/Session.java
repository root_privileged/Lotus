package kamala.kalpana.database.chat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.LotusDatabase;
import kamala.kalpana.connection.api.FListAPIClient;
import kamala.kalpana.database.DBEntity;

import java.io.IOException;
import java.util.List;

public class Session implements DBEntity
{
  private String character;
  private List<String> pinnedTabs;

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public final void setCharacter(String character)
  {
    this.character = character;
  }

  @JsonProperty("pinnedTabs")
  public final List<String> getPinnedTabs()
  {
    return pinnedTabs;
  }
  public final void setPinnedTabs(List<String> pinnedTabs)
  {
    this.pinnedTabs = pinnedTabs;
  }

  @Override
  @JsonIgnore
  public String getKey()
  {
    return "session_" + character;
  }

  public static Session getSession(String character)
  {
    byte[] rawValue = null;

    rawValue = LotusDatabase.getDatabase().get(("session_" + character).getBytes());

    if(rawValue != null)
    {
      try
      {
        return LotusDatabase.getObjectMapper().readValue(rawValue, Session.class);
      }
      catch (IOException e) { return null; }
    }
    else
      return null;
  }
}
