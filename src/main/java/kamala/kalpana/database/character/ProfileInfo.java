package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Element;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Contains the information stored in a profile on the 'info' tab.
 */
public class ProfileInfo
{
  private static final Pattern STAT_COLUMN_SELECTOR = Pattern.compile("<span class=\"taglabel\">(?<statTitle>[A-Za-z /]+):</span> (?<statValue>[\\(\\),A-Za-z 0-9\\- \\.:]+)");

  private Map<String, String> generalDetails;
  private Map<String, String> contactDetails;
  private Map<String, String> rpPreferences;
  private Map<String, String> sexualDetails;

  private ProfileInfo() {}

  public ProfileInfo(Element rootElement)
  {
    generalDetails = new LinkedHashMap<>();
    contactDetails = new LinkedHashMap<>();
    rpPreferences = new LinkedHashMap<>();
    sexualDetails = new LinkedHashMap<>();

    {
      for(Element infoGroup : rootElement.children().stream().filter(c -> c.hasClass("itgroup")).collect(Collectors.toList()))
      {
        Element infoTagHeader = infoGroup.getElementsByClass("Character_InfotagGroup").first();

        Map<String, String> targetMap = null;

        switch(infoTagHeader.ownText())
        {
          case "General details":
            targetMap = generalDetails;
            break;
          case "Contact details/Sites":
            targetMap = contactDetails;
            break;
          case "RPing preferences":
            targetMap = rpPreferences;
            break;
          case "Sexual details":
            targetMap = sexualDetails;
            break;
        }

        Matcher mtch = STAT_COLUMN_SELECTOR.matcher(infoGroup.toString());

        while (mtch.find())
          targetMap.put(mtch.group("statTitle"), StringEscapeUtils.unescapeHtml4(mtch.group("statValue")));
      }
    }
  }

  @JsonProperty("general")
  public Map<String, String> getGeneralDetails()
  {
    return generalDetails;
  }
  public void setGeneralDetails(Map<String, String> generalDetails)
  {
    this.generalDetails = generalDetails;
  }

  @JsonProperty("contact")
  public Map<String, String> getContactDetails()
  {
    return contactDetails;
  }
  public void setContactDetails(Map<String, String> contactDetails)
  {
    this.contactDetails = contactDetails;
  }

  @JsonProperty("rp_preferences")
  public Map<String, String> getRpPreferences()
  {
    return rpPreferences;
  }
  public void setRpPreferences(Map<String, String> rpPreferences)
  {
    this.rpPreferences = rpPreferences;
  }

  @JsonProperty("sexual_details")
  public Map<String, String> getSexualDetails()
  {
    return sexualDetails;
  }
  public void setSexualDetails(Map<String, String> sexualDetails)
  {
    this.sexualDetails = sexualDetails;
  }
}
