package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import kamala.kalpana.LotusDatabase;
import kamala.kalpana.connection.api.FListAPIClient;
import kamala.kalpana.database.DBEntity;
import kamala.kalpana.etc.LocalDateTimeDeserializer;
import kamala.kalpana.etc.LocalDateTimeSerializer;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CharacterProfile implements DBEntity
{
  private static final Pattern STAT_LABEL_SELECTOR = Pattern.compile("<span class=\"taglabel\">(?<statTitle>[A-Za-z /]+)</span>:");
  private static final Pattern STAT_VALUE_SELECTOR = Pattern.compile("(?<statValue>[A-Za-z0-9, -/]+)\\s+<br />");

  private int characterId;
  private String name;

  private LocalDateTime lastRetrieved;

  private Map<String, String> charStats;
  private List<String> badges;

  private ProfileInfo profileInfo;
  private KinkInfo kinkInfo;

  private ImageReferenceData imageReferences;
  private FriendData friendData;
  private GroupData groupData;

  private List<GuestbookPost> guestbookPosts;

  private CharacterProfile() { }

  public CharacterProfile(Document src)
  {
    lastRetrieved = LocalDateTime.now();

    charStats = new LinkedHashMap<>();
    badges = new ArrayList<>();
    guestbookPosts = new ArrayList<>();


    this.characterId = Integer.parseInt(src.getElementById("profile-character-id").attr("value"));
    this.name = src.getElementById("profile-character-name").attr("value");

    {
      Element statBox = src.getElementsByClass("statbox").first();

      Matcher lblMatch = STAT_LABEL_SELECTOR.matcher(statBox.toString());
      Matcher valueMatch = STAT_VALUE_SELECTOR.matcher(statBox.toString());

      while (lblMatch.find() && valueMatch.find())
        charStats.put(lblMatch.group("statTitle"), StringEscapeUtils.unescapeHtml4(valueMatch.group("statValue")).trim());
    }

    profileInfo = new ProfileInfo(src.getElementsByClass("infodatabox").first());

    {
      Element badgeContainer = src.getElementById("charabadges");

      Elements badgeElements = badgeContainer.getElementsByClass("span");

      if(badgeElements != null)
        badges.addAll(badgeElements.stream().map(Element::ownText).collect(Collectors.toList()));
    }

    kinkInfo = new KinkInfo(src);

    {
      Elements gbElements = src.getElementsByClass("Character_GuestbookPost");
      if(gbElements != null)
      {
        guestbookPosts.addAll(gbElements.stream().map(GuestbookPost::new).collect(Collectors.toList()));
      }
    }

    imageReferences = FListAPIClient.GetImageReferences(this);
    friendData = FListAPIClient.GetFriendInformation(this);
    groupData = FListAPIClient.GetGroupInformation(this);
  }

  @JsonProperty("id")
  public final int getId()
  {
    return characterId;
  }
  private final void setId(int characterId)
  {
    this.characterId = characterId;
  }

  @JsonProperty("name")
  public final String getName() { return name; }
  private final void setName(String name)
  {
    this.name = name;
  }

  @JsonProperty("last_retrieved")
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  public LocalDateTime getLastRetrieved()
  {
    return lastRetrieved;
  }
  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  public void setLastRetrieved(LocalDateTime last_retrieved)
  {
    this.lastRetrieved = last_retrieved;
  }

  @JsonProperty("stats")
  public final Map<String, String> getCharacterStats() { return charStats; }
  private final void setCharacterStats(Map<String, String> charStats)
  {
    this.charStats = charStats;
  }

  @JsonProperty("kinks")
  public final KinkInfo getKinks()
  {
    return kinkInfo;
  }
  private final void setKinks(KinkInfo kinkInfo)
  {
    this.kinkInfo = kinkInfo;
  }

  @JsonProperty("info")
  public final ProfileInfo getProfileInfo()
  {
    return profileInfo;
  }
  private final void setProfileInfo(ProfileInfo profileInfo)
  {
    this.profileInfo = profileInfo;
  }

  @JsonProperty("images")
  public final ImageReferenceData getImageReferences()
  {
    return imageReferences;
  }
  private final void setImageReferences(ImageReferenceData imageReferences)
  {
    this.imageReferences = imageReferences;
  }

  @JsonProperty("friends")
  public FriendData getFriendData()
  {
    return friendData;
  }
  private final void setFriendData(FriendData friendData)
  {
    this.friendData = friendData;
  }

  @JsonProperty("groups")
  public GroupData getGroupData()
  {
    return groupData;
  }
  private final void setGroupData(GroupData groupData)
  {
    this.groupData = groupData;
  }

  @JsonProperty("guestbook")
  public final List<GuestbookPost> getGuestbookPosts() { return guestbookPosts; }
  private final void setGuestbookPosts(List<GuestbookPost> guestbookPosts)
  {
    this.guestbookPosts = guestbookPosts;
  }

  @JsonProperty("badges")
  public final List<String> getBadges() { return badges; }
  private final void setBadges(List<String> badges)
  {
    this.badges = badges;
  }

  @Override
  public String getKey()
  {
    return name;
  }

  public static CharacterProfile getCharacter(String character)
  {
    byte[] rawValue = null;

    rawValue = LotusDatabase.getDatabase().get(character.getBytes());

    if(rawValue != null)
    {
      try
      {
        return LotusDatabase.getObjectMapper().readValue(rawValue, CharacterProfile.class);
      }
      catch (IOException e) { return null; }
    }
    else
      return null;
  }
}
