package kamala.kalpana.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import kamala.kalpana.LotusDatabase;
import org.iq80.leveldb.DB;

public interface DBEntity
{
  public String getKey();

  public default void writeToDatabase()
  {
    DB db = LotusDatabase.getDatabase();

    if(db != null)
    {
      try
      {
        LotusDatabase.getDatabase().put(getKey().getBytes(), LotusDatabase.getObjectMapper().writeValueAsBytes(this));
      }
      catch (JsonProcessingException e)
      {
        e.printStackTrace();
      }
    }
  }
}
