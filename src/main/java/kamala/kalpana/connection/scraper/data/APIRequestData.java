package kamala.kalpana.connection.scraper.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class APIRequestData
{
  private String error;

  @JsonProperty("error")
  private void setError(String err)
  {
    if(error == null)
    {
      error = null;
    }
    else
      error = err.isEmpty() ? null : err;
  }
  public String getError()
  {
    return error;
  }

  @JsonIgnore
  public boolean hasError()
  {
    return error != null;
  }
}
