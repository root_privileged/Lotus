package kamala.kalpana.connection.scraper;

import kamala.kalpana.database.character.CharacterProfile;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FListScraper
{
  public static CharacterProfile getCharacterInformation(String characterName)
  {
    try
    {
      Document chrPage = Jsoup
              .connect("https://www.f-list.net/c/" + characterName)
              .cookie("warning", "1")
              .method(Connection.Method.GET)
              .get();

      return new CharacterProfile(chrPage);
    }
    catch (IOException e)
    {
      return null;
    }
  }

  private static final Pattern YEAR = Pattern.compile("((?<years>[0-9]+)y)");
  private static final Pattern MONTHS = Pattern.compile("((?<months>[0-9]+)mo)");
  private static final Pattern DAYS = Pattern.compile("((?<days>[0-9]+)d)");
  private static final Pattern HOURS = Pattern.compile("((?<hours>[0-9]+)h)");
  private static final Pattern MINUTES = Pattern.compile("((?<minutes>[0-9]+)m)");
  private static final Pattern SECONDS = Pattern.compile("((?<seconds>[0-9]+)s)");

  public static LocalDateTime fromDelimited(String timeAgo)
  {
    LocalDateTime date = LocalDateTime.now();

    String dateText = new String(timeAgo);

    Matcher mtch = YEAR.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusYears(Integer.parseInt(mtch.group("years")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = MONTHS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusMonths(Integer.parseInt(mtch.group("months")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = DAYS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusDays(Integer.parseInt(mtch.group("days")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = HOURS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusHours(Integer.parseInt(mtch.group("hours")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = MINUTES.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusMinutes(Integer.parseInt(mtch.group("minutes")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = SECONDS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusSeconds(Integer.parseInt(mtch.group("seconds")));
      dateText = dateText.replace(mtch.group(), "");
    }

    return date;
  }
}
