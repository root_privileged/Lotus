package kamala.kalpana.connection;

public enum FChatError
{
  Success(0, "Operation completed successfully."),
  SyntaxError(1, "Syntax error."),
  No_Free_Slots(2, "There are no free slots left for you to connect to."),
  No_Login(3, "This packet requires that you have logged in."),
  Login_Failure(4, "Identification failed."),
  Chat_Interval(5, "You must wait one second between sending channel messages."),
  Character_Not_Found(6, "The character requested was not found."),
  Profile_Request_Interval(7, "You must wait ten seconds between requesting profiles."),
  UnknownCommand(8, "Unknown packet."),
  Banned(9, "You are banned from the server."),
  NotAdmin(10, "This packet requires that you be an administrator."),
  AlreadyLoggedIn(11, "Already identified."),
  Kink_Request_Interval(13, "You must wait ten seconds between requesting kinks."),
  Message_Length_Exceeded(15, "Message exceeded the maximum length."),
  Character_Already_GlobalMod(16, "This character is already a global moderator."),
  Character_Not_GlobalMod(17, "This character is not a global moderator."),
  NoResults(18, "There were no search results."),
  Moderator_Required(19, "This packet requires that you be a moderator."),
  You_Are_Ignored(20, "<character name> does not wish to receive messages from you."),
  User_Immune(21, "This action can not be used on a moderator or administrator."),
  Channel_Not_Found(26, "Could not locate the requested channel."),
  Already_Within_Channel(28, "You are already in the requested channel."),
  IP_Limited(30, "There are too many connections from your IP."),
  Login_Limitation(31, "You have been disconnected because this character has been logged in at another location."),
  Already_Banned(32, "That account is already banned."),
  Unknown_Authentication(33, "Unknown authentication method requested."),
  Roll_Error(36, "There was a problem with your roll packet."),
  Timeout_Range_Error(38, "The time given for the timeout was invalid. It must be a number between 1 and 90 minutes."),
  Timed_Out(39, "You have been given a time out by <moderator name> for <length> minute(s). The reason given was: <reason>"),
  Chat_Kicked(40, "You have been kicked from chat."),
  Already_Banned_Channel(41, "This character is already banned from the channel."),
  Not_Banned_Channel(42, "This character is not currently banned from the channel."),
  Invite_Only(44, "You may only join the requested channel with an invite."),
  No_External_Messages(45, "You must be in a channel to send messages to it."),
  No_Invite_Public(47, "You may not invite others to a public channel."),
  You_Are_Banned_Channel(48, "You are banned from the requested channel."),
  Character_Not_Found_Channel(49, "That character was not found in the channel."),
  Search_Interval(50, "You must wait five seconds between searches."),
  Moderator_Call_Interval(54, "Please wait two minutes between calling moderators. If you need to make an addition or a correction to a report, please contact a moderator directly."),
  Ad_Interval(56, "You may only post a role play ad to a channel every ten minutes."),
  No_Ads(59, "This channel does not allow role play ads, only chat messages."),
  No_Chat(60, "This channel does not allow chat messages, only role play ads."),
  Search_Terms_Exceeded(61, "There were too many search terms."),
  No_Login_Slots(62, "There are currently no free login slots."),
  Ignore_List_Size_Limited(64, "Your ignore list may not exceed 100 people."),
  Channel_Title_Size_Limit(67, "Channel titles may not exceed 64 characters in length."),
  Too_Many_Search_Results(72, "There are too many search results, please narrow your search."),
  Fatal_Error(-1, "Fatal internal error."),
  Fatal_Command_Error(-2, "An error occurred while processing your packet."),
  Command_Not_Implemented(-3, "This packet has not been implemented yet."),
  Login_Timeout(-4, "A connection to the login server timed out. Please try again in a moment."),
  Unknown_Error(-5, "An unknown error occurred."),
  No_Dice_or_Spin(-10, "You may not roll dice or spin the bottle in Frontpage.");


  private final int code;
  private final String description;
  FChatError(int errCode, String errDescription)
  {
    this.code = errCode;
    this.description = errDescription;
  }

  public static FChatError fromCode(int targetCode)
  {
    switch(targetCode)
    {
      case 0:
        return Success;
      case 1:
        return SyntaxError;
      case 2:
        return No_Free_Slots;
      case 3:
        return No_Login;
      case 4:
        return Login_Failure;
      case 5:
        return Chat_Interval;
      case 6:
        return Character_Not_Found;
      case 7:
        return Profile_Request_Interval;
      case 8:
        return UnknownCommand;
      case 9:
        return Banned;
      case 10:
        return NotAdmin;
      case 11:
        return AlreadyLoggedIn;
      case 13:
        return Kink_Request_Interval;
      case 15:
        return Message_Length_Exceeded;
      case 16:
        return Character_Already_GlobalMod;
      case 17:
        return Character_Not_GlobalMod;
      case 18:
        return NoResults;
      case 19:
        return Moderator_Required;
      case 20:
        return You_Are_Ignored;
      case 21:
        return User_Immune;
      case 26:
        return Channel_Not_Found;
      case 28:
        return Already_Within_Channel;
      case 30:
        return IP_Limited;
      case 31:
        return Login_Limitation;
      case 32:
        return Already_Banned;
      case 33:
        return Unknown_Authentication;
      case 36:
        return Roll_Error;
      case 38:
        return Timeout_Range_Error;
      case 39:
        return Timed_Out;
      case 40:
        return Chat_Kicked;
      case 41:
        return Already_Banned_Channel;
      case 42:
        return Not_Banned_Channel;
      case 44:
        return Invite_Only;
      case 45:
        return No_External_Messages;
      case 47:
        return No_Invite_Public;
      case 48:
        return You_Are_Banned_Channel;
      case 49:
        return Character_Not_Found_Channel;
      case 50:
        return Search_Interval;
      case 54:
        return Moderator_Call_Interval;
      case 56:
        return Ad_Interval;
      case 59:
        return No_Ads;
      case 60:
        return No_Chat;
      case 61:
        return Search_Terms_Exceeded;
      case 62:
        return No_Login_Slots;
      case 64:
        return Ignore_List_Size_Limited;
      case 67:
        return Channel_Title_Size_Limit;
      case 72:
        return Too_Many_Search_Results;

      case -1:
        return Fatal_Error;
      case -2:
        return Fatal_Command_Error;
      case -3:
        return Command_Not_Implemented;
      case -4:
        return Login_Timeout;
      case -10:
        return No_Dice_or_Spin;

      default:
        return Unknown_Error;
    }
  }
}
