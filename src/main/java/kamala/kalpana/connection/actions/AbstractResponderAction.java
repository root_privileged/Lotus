package kamala.kalpana.connection.actions;

import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.packet.ActionIdentifier;
import kamala.kalpana.connection.packet.IPacketReceiver;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
  Base class for actions to be invoked on packets received, this allows for easier parsing of multipart packets
  and each action is responsible to remove itself.
 */
public abstract class AbstractResponderAction implements IPacketReceiver
{
  protected static final Logger actionLogger = LoggerFactory.getLogger(AbstractResponderAction.class);

  private static Reflections reflector =
          new Reflections(new ConfigurationBuilder()
                  .setUrls(ClasspathHelper.forPackage("kamala.kalpana.connection.actions"))
                  .setScanners(
                          new SubTypesScanner(),
                          new TypeAnnotationsScanner())
          );
  private static Map<String, Class<? extends AbstractResponderAction>> actionCommandCache = new HashMap<>();

  public static void InitializeActions()
  {
    Set<Class<? extends AbstractResponderAction>> acts = reflector.getSubTypesOf(AbstractResponderAction.class);

    acts.stream()
            .filter(clz -> clz.isAnnotationPresent(ActionIdentifier.class))
            .forEach((clz) -> {
              ActionIdentifier cid = getIdentifier(clz);

              actionCommandCache.put(cid.OnCommand(), clz);
            });

    actionLogger.info("Responder Actions Cached: {}", actionCommandCache.size());
  }

  public static ActionIdentifier getIdentifier(Class<? extends AbstractResponderAction> cmd)
  {
    if (cmd.isAnnotationPresent(ActionIdentifier.class))
      return cmd.getAnnotation(ActionIdentifier.class);
    else return null;
  }

  public static AbstractResponderAction parseActionFrom(String identifier, ChatContext withinContext)
  {
    AbstractResponderAction aa = null;

    if(actionCommandCache.containsKey(identifier))
    {
      Class<? extends AbstractResponderAction> cClass = actionCommandCache.get(identifier);

      try
      {
        aa = cClass.newInstance();
        aa.setContext(withinContext);
        actionLogger.trace("Created Action {}", aa.getClass().getName());
      }
      catch (InstantiationException e)
      {
        // TODO: Better error handling.
        e.printStackTrace();
      }
      catch (IllegalAccessException e)
      {
        // TODO: Better error handling.
        e.printStackTrace();
      }
    }

    return aa;
  }


  private ChatContext ctx;

  public AbstractResponderAction() { }

  public final void setContext(ChatContext ctx) { this.ctx = ctx; }
  public final ChatContext getContext()
  {
    return ctx;
  }

  /**
   * Should be called by all AbstractResponderActions when they are done handling packets and after they
   * have dispatched themselves back into the program.
   */
  protected void unregisterListener()
  {
    getContext().getSocketClient().receiverBus().unregister(this);
  }
}
