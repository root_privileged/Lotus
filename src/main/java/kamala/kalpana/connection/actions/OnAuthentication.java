package kamala.kalpana.connection.actions;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import kamala.kalpana.connection.packet.ActionIdentifier;
import kamala.kalpana.connection.packet.server.IdentificationResponse;

@ActionIdentifier(OnCommand = "IDN")
public class OnAuthentication extends AbstractResponderAction
{
  private IdentificationResponse response;

  @Subscribe
  @AllowConcurrentEvents
  public void onIdentificationResponse(IdentificationResponse idn)
  {
    this.response = idn;
    getContext().onAction(this);
    unregisterListener();
  }

  public final IdentificationResponse getResponse()
  {
    return response;
  }
}
