package kamala.kalpana.connection.command;

import javafx.collections.ObservableMap;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.connection.packet.client.ChannelJoinRequest;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@CommandIdentifier(Command = "join", Description = "Tries to join the specified channel, this will automatically perform a lookup to try and find the channel in the public private channel list if a public channel isn't found.")
public class JoinCommand extends AbstractCommand
{
  @Override
  public boolean isChannelUsable(ChatContext chatContext, ChannelContext channelContext, CharacterReference source, List<String> arguments)
  {
    return true;
  }

  @Override
  public boolean isConversationUsable(ChatContext chatContext, CharacterReference characterReference, List<String> arguments)
  {
    return true;
  }

  @Override
  public boolean execute(ChatContext chatContext, ChannelContext channel, CharacterReference source, List<String> arguments)
  {
    String target = String.join(" ", arguments).trim();

    ObservableMap<String, ChannelReference> channels = chatContext.getChannels();

    Optional<Map.Entry<String, ChannelReference>> oc = channels.entrySet().stream()
            .filter(e -> e.getValue().getTitle().equals(target)).findFirst();

    if(oc.isPresent())
    {
      cmdLogger.trace("Sending Join Request to Channel {}", oc.get().getValue().getIdentifier());

      chatContext.getSocketClient().sendCommand(new ChannelJoinRequest(oc.get().getValue()));

      return true;
    }


    return false;
  }
}
