package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CBL", Description = "Requests the ban list for the specified channel.")
public class ChannelBanlistRequest extends ClientPacket
{
  private String channel;

  public ChannelBanlistRequest(String channel)
  {
    this.channel = channel;
  }

  @JsonProperty("channel")
  public final String getChannel()
  {
    return channel;
  }
  private void setChannel(String channel)
  {
    this.channel = channel;
  }
}
