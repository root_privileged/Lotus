package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "PRO", Description = "Requests profile tags from the character.")
public class ProfileTagRequest extends ClientPacket
{
  private String character;

  public ProfileTagRequest(String character)
  {
    this.character = character;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }
}
