package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CTU", Description = "Temporarily bans user from channel.")
public class ChannelTemporaryBanRequest extends ClientPacket
{
  private String channel;
  private String character;
  private int minutes;

  public ChannelTemporaryBanRequest(String channel, String character, int minutes)
  {
    this.channel = channel;
    this.character = character;
    this.minutes = minutes;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }

  @JsonProperty("length")
  public int getMinutes()
  {
    return minutes;
  }
  public void setMinutes(int minutes)
  {
    this.minutes = minutes;
  }
}
