package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "RLL", Description = "Rolls dice or spins bottle in the specified manner.")
public class Roll_BottleRequest extends ClientPacket
{
  private String channel;
  private String dice;

  public Roll_BottleRequest(String channel, String dice)
  {
    this.channel = channel;
    this.dice = dice;
  }

  public Roll_BottleRequest(String channel)
  {
    this(channel, "bottle");
  }

  public Roll_BottleRequest(String channel, int diceCount, int sides)
  {
    this(channel, diceCount + "d" + sides);
  }

  public Roll_BottleRequest(String channel, int diceCount, int sides, int additional)
  {
    this(channel, diceCount + "d" + sides + "+" + additional);
  }

  @JsonProperty("channel")
  public final String getChannel()
  {
    return channel;
  }
  private void setChannel(String channel)
  {
    this.channel = channel;
  }
}
