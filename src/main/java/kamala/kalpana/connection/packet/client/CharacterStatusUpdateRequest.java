package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "STA", Description = "Request a new status to be set for your statusmsg.")
public class CharacterStatusUpdateRequest extends ClientPacket
{
  private String status;
  private String statusmsg;

  public CharacterStatusUpdateRequest(String status, String statusmsg)
  {
    this.status = status;
    this.statusmsg = statusmsg;
  }

  @JsonProperty("status")
  public String getStatus()
  {
    return status;
  }
  public void setStatus(String status)
  {
    this.status = status;
  }

  @JsonProperty("statusmsg")
  public final String getStatusmsg()
  {
    return statusmsg;
  }
  public void setStatusmsg(String newValue)
  {
    statusmsg = newValue;
  }

}
