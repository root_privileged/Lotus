package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CIU", Description = "Sends a channel invite to the target character.")
public class ChannelInviteRequest extends ClientPacket
{
  private String channel;
  private String character;

  public ChannelInviteRequest(ChannelReference channelReference, CharacterReference characterReference)
  {
    this.channel = channelReference.getIdentifier();
    this.character = characterReference.getName();
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
