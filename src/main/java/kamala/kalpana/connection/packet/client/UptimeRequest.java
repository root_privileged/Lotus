package kamala.kalpana.connection.packet.client;

import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "UPT", HeaderOnly = true, Description = "Requests server stats and up-time.")
public class UptimeRequest extends ClientPacket
{}
