package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "TMO", Description = "Times out a user for a set amount of minutes.")
public class ServerTimeoutRequest extends ClientPacket
{
  private String character;
  private int time;
  private String reason;

  public ServerTimeoutRequest(String character, int time, String reason)
  {
    this.character = character;
    this.time = time;
    this.reason = reason;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("time")
  public int getTime()
  {
    return time;
  }
  public void setTime(int time)
  {
    this.time = time;
  }

  @JsonProperty("reason")
  public String getReason()
  {
    return reason;
  }
  public void setReason(String reason)
  {
    this.reason = reason;
  }
}
