package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "BRO", Description = "Broadcasts a message to all connected clients.")
public class BroadcastRequest extends ClientPacket
{
  private String message;

  public BroadcastRequest(String message)
  {
    this.message = message;
  }

  @JsonProperty("message")
  public final String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }
}
