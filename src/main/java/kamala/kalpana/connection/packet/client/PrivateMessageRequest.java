package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "PRI", Description = "Sends a message to the specified character.")
public class PrivateMessageRequest extends ClientPacket
{
  private String recipient;
  private String message;

  public PrivateMessageRequest(String recipient, String message)
  {
    this.recipient = recipient;
    this.message = message;
  }

  @JsonProperty("message")
  public final String getMessage()
  {
    return message;
  }
  public void setMessage(String newValue)
  {
    message = newValue;
  }

  @JsonProperty("recipient")
  public String getRecipient()
  {
    return recipient;
  }
  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }
}
