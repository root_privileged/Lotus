package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "JCH", Description = "Request to join channel.")
public class ChannelJoinRequest extends ClientPacket
{
  private String channel;

  public ChannelJoinRequest(ChannelReference channelReference)
  {
    this.channel = channelReference.getIdentifier();
  }

  @JsonProperty("channel")
  public final String getChannel()
  {
    return channel;
  }
  private void setChannel(String channel)
  {
    this.channel = channel;
  }
}
