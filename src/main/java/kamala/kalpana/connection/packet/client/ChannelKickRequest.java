package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CKU", Description = "Kicks target character from the specified channel.")
public class ChannelKickRequest extends ClientPacket
{
  private String channel;
  private String character;

  public ChannelKickRequest(String channel, String character)
  {
    this.channel = channel;
    this.character = character;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
