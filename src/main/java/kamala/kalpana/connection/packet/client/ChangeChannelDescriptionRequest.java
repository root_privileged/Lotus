package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CDS", Description = "Changes a channels description text..")
public class ChangeChannelDescriptionRequest extends ClientPacket
{
  private String channel;
  private String description;

  public ChangeChannelDescriptionRequest(String channel, String description)
  {
    this.channel = channel;
    this.description = description;
  }

  @JsonProperty("description")
  public final String getDescription()
  {
    return description;
  }
  public void setDescription(String newValue)
  {
    description = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
