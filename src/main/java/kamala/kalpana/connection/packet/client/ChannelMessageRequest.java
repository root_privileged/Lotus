package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "MSG", Description = "Sends a message to the channel.")
public class ChannelMessageRequest extends ClientPacket
{
  private String channel;
  private String message;

  public ChannelMessageRequest(String channel, String message)
  {
    this.channel = channel;
    this.message = message;
  }

  @JsonProperty("message")
  public final String getMessage()
  {
    return message;
  }
  public void setMessage(String newValue)
  {
    message = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
