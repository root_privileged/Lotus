package kamala.kalpana.connection.packet.client;

import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "ORS", HeaderOnly = true, Description = "Requests a list of all open private channels.")
public class PrivateChannelListRequest extends ClientPacket
{}
