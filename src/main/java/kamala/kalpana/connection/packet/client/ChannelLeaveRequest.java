package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "LCH", Description = "Request to leave channel.")
public class ChannelLeaveRequest extends ClientPacket
{
  private String channel;

  public ChannelLeaveRequest(String channel)
  {
    this.channel = channel;
  }

  @JsonProperty("channel")
  public final String getChannel()
  {
    return channel;
  }
  private void setChannel(String channel)
  {
    this.channel = channel;
  }
}
