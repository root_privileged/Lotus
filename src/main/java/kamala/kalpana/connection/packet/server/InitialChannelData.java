package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;
import kamala.kalpana.connection.packet.server.internal.InternalUser;

import java.util.List;

@PacketIdentifier(Identifier = "ICH", Description = "Initial channel database.")
public class InitialChannelData extends AbstractChannelPacket
{
  private String mode;
  private List<InternalUser> users;

  @JsonProperty("character")
  public final String getMode()
  {
    return mode;
  }
  private void setMode(String mode)
  {
    this.mode = mode;
  }

  @JsonProperty("users")
  public List<InternalUser> getUsers()
  {
    return users;
  }
  private void setUsers(List<InternalUser> users)
  {
    this.users = users;
  }

  @Override
  public String toString()
  {
    return "ICH: '" + getChannel() + "' Mode: " + getMode() + " Characters: " + getUsers().size();
  }
}
