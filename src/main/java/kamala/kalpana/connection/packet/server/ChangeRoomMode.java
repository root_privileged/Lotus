package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "RMO", Description = "Room mode change.")
public class ChangeRoomMode extends AbstractChannelPacket
{
  private String mode;

  @JsonProperty("mode")
  public String getMode()
  {
    return mode;
  }
  public void setMode(String mode)
  {
    this.mode = mode;
  }


  @Override
  public String toString()
  {
    return "RMO: Channel: " + getChannel() + " Mode: " + getMode();
  }
}
