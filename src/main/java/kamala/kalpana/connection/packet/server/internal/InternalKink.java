package kamala.kalpana.connection.packet.server.internal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InternalKink
{
  private String fetish_id;
  private String name;

  @JsonProperty("name")
  public String getName()
  {
    return name;
  }
  private void setName(String name)
  {
    this.name = name;
  }

  @JsonProperty("fetish_id")
  public String getFetish_id()
  {
    return fetish_id;
  }
  private void setFetish_id(String fetish_id)
  {
    this.fetish_id = fetish_id;
  }

}
