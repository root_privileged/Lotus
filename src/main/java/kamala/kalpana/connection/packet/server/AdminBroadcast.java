package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "BRO", Description = "Admin server broadcast message.")
public class AdminBroadcast extends ServerPacket
{
  private String message;

  @JsonProperty("message")
  public final String getMessage()
  {
    return message;
  }
  private void setMessage(String newValue)
  {
    message = newValue;
  }

  @Override
  public String toString()
  {
    return "BRO: " + getMessage();
  }
}
