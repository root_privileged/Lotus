package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "PRD", Description = "Profile Data.")
public class ProfileData extends ServerPacket
{
  private String type;
  private String message;
  private String key;
  private String value;

  @JsonProperty("type")
  public String getType()
  {
    return type;
  }
  private void setType(String type)
  {
    this.type = type;
  }

  @JsonProperty("message")
  public String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }

  @JsonProperty("key")
  public String getKey()
  {
    return key;
  }
  private void setKey(String key)
  {
    this.key = key;
  }

  @JsonProperty("value")
  public String getValue()
  {
    return value;
  }
  private void setValue(String value)
  {
    this.value = value;
  }

  @Override
  public String toString()
  {
    return "PRD: Unimplemented";
  }
}
