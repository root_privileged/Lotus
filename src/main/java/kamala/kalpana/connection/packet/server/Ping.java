package kamala.kalpana.connection.packet.server;

import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "PIN", HeaderOnly = true, Description = "Server ping packet, requires a response to keep connection alive.")
public class Ping extends ServerPacket
{
  @Override
  public String toString()
  {
    return "PIN: Pong";
  }
}
