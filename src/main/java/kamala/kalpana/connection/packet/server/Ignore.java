package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

import java.util.List;

@PacketIdentifier(Identifier = "IGN", Description = "Handles ignore packet, its very obnoxious.")
public class Ignore extends ServerPacket
{
  private String action;

  @JsonProperty("action")
  public String getAction()
  {
    return action;
  }
  private void setAction(String action)
  {
    this.action = action;
  }

  @JsonIgnore
  public String getCharacter()
  {
    if(action.equals("add") || action.equals("delete"))
      return (String) getUnknownProperty("character");
    else return null;
  }

  @JsonIgnore
  public List<String> getCharacters()
  {
    if(action.equals("init"))
      return (List<String>) getUnknownProperty("characters");
    else return null;
  }

  @Override
  public String toString()
  {
    return "IGN: Action: " + getAction();
  }
}
