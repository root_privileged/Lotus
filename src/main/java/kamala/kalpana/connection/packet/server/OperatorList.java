package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

import java.util.List;

@PacketIdentifier(Identifier = "ADL", Description = "List containing a list of the current chat operators.")
public class OperatorList extends ServerPacket
{
  private List<String> ops;

  @JsonProperty("ops")
  public final List<String> getOperators() { return ops; }
  private void setOperators(List<String> ops) { this.ops = ops; }

  @Override
  public String toString()
  {
    return "ADL: " + getOperators().size();
  }
}
