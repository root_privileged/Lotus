package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "CIU", Description = "Invites a user to a title.")
public class ChannelInvite extends ServerPacket
{
  private String sender;
  private String title;
  private String name;

  @JsonProperty("sender")
  public final String getSender()
  {
    return sender;
  }
  private void setSender(String sender)
  {
    this.sender = sender;
  }

  @JsonProperty("title")
  public final String getTitle()
  {
    return title;
  }
  private void setTitle(String title)
  {
    this.title = title;
  }

  @JsonProperty("name")
  public final String getName()
  {
    return name;
  }
  private void setName(String name)
  {
    this.name = name;
  }

  @Override
  public String toString()
  {
    return "CIU: Sender: " + getSender() + " Title: " + getTitle() + " Name: " + getName();
  }
}
