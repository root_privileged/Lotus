package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "FRL", Description = "Initial friends list.")
public class InitialFriendsList extends ServerPacket
{
  private String characters;

  @JsonProperty("message")
  public final String getCharacters()
  {
    return characters;
  }
  private void setCharacters(String characters)
  {
    this.characters = characters;
  }

  @Override
  public String toString()
  {
    return "FRL: Characters: " + getCharacters();
  }
}
