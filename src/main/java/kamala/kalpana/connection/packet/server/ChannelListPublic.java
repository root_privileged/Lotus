package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;
import kamala.kalpana.connection.packet.server.internal.InternalPublicChannel;

import java.util.List;

@PacketIdentifier(Identifier = "CHA", Description = "List of all public channels.")
public class ChannelListPublic extends ServerPacket
{
  private List<InternalPublicChannel> channels;

  @JsonProperty("channels")
  public final List<InternalPublicChannel> getChannels() { return channels; }
  private void setChannels(List<InternalPublicChannel> channels) { this.channels = channels; }

  @Override
  public String toString()
  {
    return "CHA: Channels: " + getChannels().size();
  }
}
