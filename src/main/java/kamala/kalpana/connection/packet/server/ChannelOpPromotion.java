package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "COA", Description = "Promotes a user to channel operator.")
public class ChannelOpPromotion extends AbstractChannelPacket
{
  private String character;

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @Override
  public String toString()
  {
    return "COA: Channel: " + getChannel() + " Character: " + getCharacter();
  }
}
