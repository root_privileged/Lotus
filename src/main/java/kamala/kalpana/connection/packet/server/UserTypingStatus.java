package kamala.kalpana.connection.packet.server;

import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "TPN", Description = "User typing notification.")
public class UserTypingStatus extends ServerPacket
{
  public enum TypingStatus
  {
    Clear,
    Paused,
    Typing
  }

  private String character;
  private TypingStatus status;

  public String getCharacter()
  {
    return character;
  }
  public void setCharacter(String character)
  {
    this.character = character;
  }

  public TypingStatus getStatus()
  {
    return status;
  }
  private void setStatus(String status)
  {
    switch(status)
    {
      case "clear":
        this.status = TypingStatus.Clear;
        break;
      case "paused":
        this.status = TypingStatus.Paused;
        break;
      case "typing":
        this.status = TypingStatus.Typing;
        break;
    }
  }

  @Override
  public String toString()
  {
    return "TPN: Not Implemented";
  }
}
