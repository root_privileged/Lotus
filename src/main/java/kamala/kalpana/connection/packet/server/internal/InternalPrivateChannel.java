package kamala.kalpana.connection.packet.server.internal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InternalPrivateChannel
{
  // Identifier aka ADH-20 Hexadecimal characters.
  private String name;

  // User-Friendly Name
  private String title;
  private int characters;

  @JsonProperty("characters")
  public int getCharacters()
  {
    return characters;
  }
  private void setCharacters(int characters)
  {
    this.characters = characters;
  }

  @JsonProperty("title")
  public String getTitle()
  {
    return title;
  }
  private void setTitle(String title)
  {
    this.title = title;
  }

  @JsonProperty("name")
  public String getName()
  {
    return name;
  }
  private void setName(String name)
  {
    this.name = name;
  }


}
