package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "VAR", Description = "Server variable.")
public class ServerVariable extends ServerPacket
{

  // Use EnumSet<Permission> here.
  public enum Permissions
  {
    Admin,
    Chat_ChatOP,
    Chat_ChanOP,
    Helpdesk_Chat,
    Helpdesk_General,
    Moderation_Site,
    Reserved,
    Misc_GroupRequests,
    Misc_NewsPosts,
    Misc_ChangeLog,
    Misc_FeatureRequests,
    Dev_BugReports,
    Dev_Kinks,
    Developer,
    Tester,
    Subscriptions,
    Former_Staff
  }

  private String variable;
  private float value;

  @JsonProperty("variable")
  public String getVariable()
  {
    return variable;
  }
  public void setVariable(String variable)
  {
    this.variable = variable;
  }

  @JsonProperty("value")
  public float getValue()
  {
    return value;
  }
  public void setValue(float value)
  {
    this.value = value;
  }

  @Override
  public String toString()
  {
    return "VAR: Variable: " + getVariable() + " Value: " + getValue();
  }
}
