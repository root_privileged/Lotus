package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

import java.util.List;

@PacketIdentifier(Identifier = "KID", Description = "Kinks Data.")
public class KinkData extends ServerPacket
{
  private String type;
  private String message;
  private List<Integer> key;
  private List<Integer> value;

  @JsonProperty("type")
  public String getType()
  {
    return type;
  }
  private void setType(String type)
  {
    this.type = type;
  }

  @JsonProperty("message")
  public String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }

  @JsonProperty("key")
  public List<Integer> getKey()
  {
    return key;
  }
  private void setKey(List<Integer> key)
  {
    this.key = key;
  }

  @JsonProperty("value")
  public List<Integer> getValue()
  {
    return value;
  }
  private void setValue(List<Integer> value)
  {
    this.value = value;
  }

  @Override
  public String toString()
  {
    return "KID: Unimplemented.";
  }
}
