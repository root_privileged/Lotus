package kamala.kalpana.connection.packet;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

public interface IPacketReceiver
{
  @Subscribe
  @AllowConcurrentEvents
  public default void handleCommand(AbstractPacket cmd) { }

  @Subscribe
  @AllowConcurrentEvents
  public default void handleClientCommand(ClientPacket cmd) { }

  @Subscribe
  @AllowConcurrentEvents
  public default void handleServerCommand(ServerPacket serverPacket) { }
}
