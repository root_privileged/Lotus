package kamala.kalpana.connection.packet;

public class RawPacket extends ServerPacket
{
  private String raw;

  public RawPacket(String raw)
  {
    this.raw = raw;
  }

  public final String getRaw()
  {
    return raw;
  }
}
