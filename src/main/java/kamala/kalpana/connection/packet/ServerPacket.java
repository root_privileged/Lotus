package kamala.kalpana.connection.packet;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import kamala.kalpana.connection.FChatSocketClient;

import java.util.HashMap;
import java.util.Map;

/*
  Base class for all commands listed

  https://wiki.f-list.net/F-Chat_Server_Commands
 */
public abstract class ServerPacket extends AbstractPacket
{
  private FChatSocketClient socketClient;
  private Map<String, Object> unknownProperties;

  protected ServerPacket()
  {
    unknownProperties = new HashMap<>();
  }

  public final FChatSocketClient getSocketClient()
  {
    return socketClient;
  }
  public void setSocketClient(FChatSocketClient socketClient)
  {
    this.socketClient = socketClient;
  }

  @JsonAnySetter
  public void setUnknownProperty(String key, Object value)
  {
    unknownProperties.put(key, value);
  }
  protected Object getUnknownProperty(String key)
  {
    return unknownProperties.get(key);
  }
}
