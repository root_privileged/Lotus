package kamala.kalpana.connection.api.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.LotusDatabase;
import kamala.kalpana.connection.api.APIResponse;
import kamala.kalpana.connection.api.FListAPIClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginTicket extends APIResponse
{
  private static String requestURL = "http://www.f-list.net/json/getApiTicket.php";

  public static LoginTicket GetTicket(String username, String password)
  {
    Map<String, String> params = new HashMap<>();
    params.put("account", username);
    params.put("password", password);

    try
    {
      LoginTicket lt = LotusDatabase.getObjectMapper().readValue(FListAPIClient.ExecuteRequest(requestURL, params), LoginTicket.class);

      lt.accountName = username;

      return lt;
    }
    catch (IOException e)
    {
      LoginTicket lTicket = new LoginTicket();
      lTicket.setError("Connectivity could not be established.");
      return lTicket;
    }
  }

  private String accountName;

  private List<String> characters;

  private String default_character;
  private String account_id;
  private String ticket;

  private List<FriendEntry> friends;
  private List<BookmarkEntry> bookmarks;

  @JsonIgnore
  public String getAccountName()
  {
    return accountName;
  }

  @JsonProperty("default_character")
  public String getDefaultCharacter() { return default_character; }
  public void setDefaultCharacter(String newValue) { default_character = newValue; }

  @JsonProperty("account_id")
  public String getAccountID() { return account_id; }
  private void setAccountID(String newValue) { account_id = newValue; }

  @JsonProperty("ticket")
  public String getTicket() { return ticket; }
  private void getTicket(String newValue) { ticket = newValue; }

  @JsonProperty("characters")
  public List<String> getCharacters() { return characters; }
  public void setCharacters(List<String> newValue) { characters = newValue; }

  @JsonProperty("friends")
  public List<FriendEntry> getFriends() { return friends; }
  public void setFriends(List<FriendEntry> newValue) { friends = newValue; }

  @JsonProperty("bookmarks")
  public List<BookmarkEntry> getBookmarks() { return bookmarks; }
  public void setBookmarks(List<BookmarkEntry> newValue) { bookmarks = newValue; }

  @Override
  @JsonIgnore
  public String getType()
  {
    return "LoginTicket";
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();

    if(this.hasError())
      sb.append("Error in Login Ticket Request: " + this.getError());
    else
    {
      sb.append("\nAccount ID: " + getAccountID());
      sb.append("\nTicket: " + getTicket());
      sb.append("\nDefault Character: " + getDefaultCharacter());
      sb.append("\nCharacters\n\t" + String.join(", ", getCharacters()));
    }

    return sb.toString();
  }
}
