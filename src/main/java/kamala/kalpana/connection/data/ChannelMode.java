package kamala.kalpana.connection.data;

public enum ChannelMode
{
  Chat,
  Ads,
  Both;

  public static ChannelMode fromString(String modeStr)
  {
    switch(modeStr.toLowerCase())
    {
      case "chat":
        return Chat;
      case "ads":
        return Ads;
      case "both":
        return Both;
    }

    return Chat;
  }
}
