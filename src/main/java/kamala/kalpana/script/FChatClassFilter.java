package kamala.kalpana.script;

import jdk.nashorn.api.scripting.ClassFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FChatClassFilter implements ClassFilter
{
  private ScriptPrivilegeLevel scriptPrivilegeLevel;

  private static final List<String> MINIMAL_PRIVILEGE_WHITELIST;
  private static final List<String> DEFAULT_PRIVILEGE_WHITELIST;

  static {
    MINIMAL_PRIVILEGE_WHITELIST = new ArrayList<>(Arrays.asList(
            "java.util",
            "kamala.kalpana.connection.actions.OnChannelJoin")
    );

    DEFAULT_PRIVILEGE_WHITELIST = new ArrayList<>(MINIMAL_PRIVILEGE_WHITELIST);
  }

  public FChatClassFilter()
  {
    this(ScriptPrivilegeLevel.Default);
  }

  public FChatClassFilter(ScriptPrivilegeLevel scriptPrivilegeLevel)
  {
    this.scriptPrivilegeLevel = scriptPrivilegeLevel;
  }

  @Override
  public boolean exposeToScripts(String s)
  {
    if(scriptPrivilegeLevel == ScriptPrivilegeLevel.Full) return true;

    List<String> targetPrivileges = null;

    switch(scriptPrivilegeLevel)
    {
      case Minimal:
        return MINIMAL_PRIVILEGE_WHITELIST.stream().anyMatch(x -> s.startsWith(x));
      case Default:
        return DEFAULT_PRIVILEGE_WHITELIST.stream().anyMatch(x -> s.startsWith(x));
    }

    if(targetPrivileges != null)
    {
      for(String p : targetPrivileges)
      {


        if(s.startsWith(p)) return true;
      }
    }


    // If something went so wrong as to go outside of the script privilege level system deny everyone.
    return false;
  }
}
