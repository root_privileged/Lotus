package kamala.kalpana.script;

import kamala.kalpana.connection.context.FChatContext;

import javax.script.ScriptEngine;
import javax.script.SimpleScriptContext;

/**
 * Script context designed to allow a headless client (or bot) to connect.
 */
public class HostedScriptContext extends AbstractFChatScriptContext
{
  public HostedScriptContext(FChatContext fChatContext, String targetCharacter, ScriptEngine scriptEngine)
  {
    this(fChatContext, targetCharacter, scriptEngine, ScriptPrivilegeLevel.Default);
  }

  public HostedScriptContext(FChatContext fChatContext, String targetCharacter, ScriptEngine scriptEngine, ScriptPrivilegeLevel scriptPrivilegeLevel)
  {
    super(fChatContext, targetCharacter, scriptEngine, new SimpleScriptContext(), scriptPrivilegeLevel);

    super.initializeInternalBindings();
  }

  @Override
  protected void initializeBindings()
  {

  }
}
