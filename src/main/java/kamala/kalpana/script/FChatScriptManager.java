package kamala.kalpana.script;

import jdk.nashorn.api.scripting.ClassFilter;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

import javax.script.*;

public class FChatScriptManager
{
  private NashornScriptEngineFactory nashornScriptEngineFactory;
  private ScriptEngine scriptEngine;

  private ClassFilter classFilter;

  public FChatScriptManager()
  {
    this.nashornScriptEngineFactory = new NashornScriptEngineFactory();

    this.classFilter = new FChatClassFilter();

    this.scriptEngine = nashornScriptEngineFactory.getScriptEngine(classFilter);
  }

  public ScriptObjectMirror getEnvironment()
  {
    return (ScriptObjectMirror) scriptEngine.get("LotusEnvironment");
  }

  public ScriptObjectMirror getBlankObject()
  {
    ScriptObjectMirror blankFunction = (ScriptObjectMirror) getEnvironment().get("createBlankObject");

    if(blankFunction != null && blankFunction.isFunction())
    {
      return (ScriptObjectMirror) blankFunction.call(null);
    }

    return null;
  }

  public ScriptEngine getScriptEngine()
  {
    return scriptEngine;
  }
}
