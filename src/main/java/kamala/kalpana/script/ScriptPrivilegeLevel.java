package kamala.kalpana.script;

public enum ScriptPrivilegeLevel
{
  /**
   * Minimal privileges, only the bare-bones required to interact only with the client itself
   * and through minimal F-List scraping interface.
   */
  Minimal,

  /**
   * Default privileges, allows for everything within the bare bones in addition to some limited
   * file and database I/O that only reaches within the client's json directories and even then
   * only directly through fixed access points already defined within the script context.
   */
  Default,

  /**
   * Disables any access restrictions, this is more something for debugging and such rather than
   * anything else; this will not be able to be enabled outside of a full program restart with
   * an explicit command-line argument.
   */
  Full
}