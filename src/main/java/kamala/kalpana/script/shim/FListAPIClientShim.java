package kamala.kalpana.script.shim;

import kamala.kalpana.script.AbstractFChatScriptContext;

public class FListAPIClientShim extends AbstractShim
{

  public FListAPIClientShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    initializeBindings();
  }

  @Override
  protected void initializeBindings()
  {

  }
}
