package kamala.kalpana.script.shim;

import kamala.kalpana.LotusChat;
import kamala.kalpana.script.AbstractFChatScriptContext;

import java.util.Arrays;

public class OwnerShim extends AbstractShim
{
  private String ownerName;

  public OwnerShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    ownerName = LotusChat.getApplicationParameters().getOwnerName();

    initializeBindings();
  }

  @Override
  protected void initializeBindings()
  {
    exposeObject("Name", ownerName.contains(",") ? Arrays.asList(ownerName.split(",")) : ownerName);
  }

  public boolean isOnline()
  {
    return ownerName != null && getScriptContext().getChatContext().getCharacters().containsKey(ownerName);
  }
}
