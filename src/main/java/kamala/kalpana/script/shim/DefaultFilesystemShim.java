package kamala.kalpana.script.shim;

import kamala.kalpana.script.AbstractFChatScriptContext;

import java.io.FileReader;
import java.io.FileWriter;

public class DefaultFilesystemShim extends AbstractShim
{
  public DefaultFilesystemShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    initializeBindings();
  }

  public FileWriter getFileWriteHandle(String fileName)
  {
    return null;
  }

  public FileReader getFileReader(String fileName)
  {
    if(getScriptContext().allowCallbacks())
    {

    }

    return null;
  }

  private String makeFileIdentifier(String name)
  {
    if(getScriptContext().allowCallbacks())
    {

    }

    return getScriptContext().getChatContext().getCharacterName() + "_" + name;
  }

  @Override
  protected void initializeBindings()
  {

  }
}
