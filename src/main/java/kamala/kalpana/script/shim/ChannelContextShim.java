package kamala.kalpana.script.shim;

import com.google.common.eventbus.Subscribe;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.data.ChannelMode;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.connection.packet.client.ChannelMessageRequest;
import kamala.kalpana.connection.packet.server.*;

import java.io.Closeable;
import java.io.IOException;

public class ChannelContextShim extends AbstractShim
        implements Closeable
{
  private ChatContextShim chatContextShim;
  private ChannelReference channelReference;
  private ChannelContext channelContext;

  public ChannelContextShim(ChatContextShim chatContextShim, ChannelReference channelReference, ChannelContext channelContext)
  {
    super(chatContextShim.getScriptContext(), chatContextShim.getScriptPrivilegeLevel());

    this.chatContextShim = chatContextShim;
    this.channelReference = channelReference;
    this.channelContext = channelContext;

    getContext().getSocketClient().receiverBus().register(this);

    initializeBindings();
  }

  @Override
  public void close() throws IOException
  {
    getContext().getSocketClient().receiverBus().unregister(this);
  }

  public ChatContext getContext()
  {
    return chatContextShim.getContext();
  }

  public ChannelContext getChannelContext()
  {
    return channelContext;
  }

  public void sendMessage(String message)
  {
    chatContextShim.getSocketClient().sendCommand(new ChannelMessageRequest(channelReference.getIdentifier(), message));
  }

  @Subscribe
  public void onChannelMessage(ChannelMessage cm)
  {
    if(getScriptContext().allowCallbacks() && cm.isValid(channelContext.getReference()))
    {
      CharacterReference from = getContext().getCharacters().get(cm.getCharacter());

      executeFunction("onMessage", this, from, cm.getMessage());
    }
  }

  @Subscribe
  public void onChannelDescription(ChannelDescriptionUpdate cdu)
  {
    if(getScriptContext().allowCallbacks() && cdu.isValid(channelContext.getReference()))
    {
      executeFunction("onDescription", this, cdu.getDescription());
    }
  }

  @Subscribe
  public void onChannelModeChange(ChangeRoomMode crm)
  {
    if(getScriptContext().allowCallbacks() && crm.isValid(channelContext.getReference()))
    {
      executeFunction("onMode", this, ChannelMode.fromString(crm.getMode()));
    }
  }

  @Subscribe
  public final void onCharacterJoin(CharacterJoinChannel ucj)
  {
    if(getScriptContext().allowCallbacks() && ucj.isValid(channelContext.getReference()))
    {
      CharacterReference from = getContext().getCharacters().get(ucj.getCharacter().getIdentity());
      executeFunction("onCharacterJoin", this, from);
    }
  }

  @Subscribe
  public final void onCharacterLeave(CharacterLeftChannel clc)
  {
    if(getScriptContext().allowCallbacks() && clc.isValid(channelContext.getReference()))
    {
      CharacterReference from = getContext().getCharacters().get(clc.getCharacter());
      executeFunction("onCharacterLeave", this, from);
    }
  }

  @Override
  protected void initializeBindings()
  {
    exposeObject("Name", channelContext.getReference().getTitle());
    exposeObject("Owner", channelContext.ownerProperty());
    exposeObject("Moderators", channelContext.getModerators());
    exposeObject("Description", channelContext.descriptionProperty());
    exposeObject("Characters", channelContext.getCharacters());

    exposeClass(ChannelMode.class);
  }
}
