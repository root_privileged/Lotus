package kamala.kalpana.script.shim;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import kamala.kalpana.LotusDatabase;
import kamala.kalpana.script.AbstractFChatScriptContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

public class DatabaseShim extends AbstractShim
{
  private static final Charset DB_CHARSET = Charset.forName("UTF-8");

  public DatabaseShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());
  }

  public void saveString(String key, String value)
  {
    LotusDatabase.getDatabase().put(key.getBytes(DB_CHARSET), value.getBytes(DB_CHARSET));
  }

  public boolean save(String key, ScriptObjectMirror obj)
  {
    try
    {
      String strObject = LotusDatabase.getObjectMapper().writeValueAsString(obj);

      if(strObject != null)
      {
        saveString(key, strObject);
        return true;
      }
      else return false;
    }
    catch (JsonProcessingException e)
    {
      return false;
    }
  }

  public String loadString(String key)
  {
    return new String(LotusDatabase.getDatabase().get(key.getBytes(DB_CHARSET)), DB_CHARSET);
  }

  public ScriptObjectMirror load(String key)
  {
    try
    {
      Map<String, Object> mapObject = LotusDatabase.getObjectMapper().readValue(loadString(key), new TypeReference<Map<String, Object>>() {});

      ScriptObjectMirror som = getScriptContext().getBlankObject();

      if(som != null)
      {
        for(Map.Entry<String, Object> e : mapObject.entrySet())
        {
          som.put(e.getKey(), e.getValue());
        }

        return som;
      }

      return null;
    }
    catch (IOException e)
    {
      return null;
    }
  }

  @Override
  protected void initializeBindings()
  {

  }
}
