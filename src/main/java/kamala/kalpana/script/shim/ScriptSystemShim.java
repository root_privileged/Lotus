package kamala.kalpana.script.shim;

import kamala.kalpana.script.AbstractFChatScriptContext;

public class ScriptSystemShim extends AbstractShim
{
  public ScriptSystemShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    initializeBindings();
  }

  @Override
  protected void initializeBindings()
  {

  }

  public boolean evaluate(String src)
  {
    return getScriptContext().evaluate(src);
  }

  public boolean include(String src)
  {
    return getScriptContext().evaluateFile(src);
  }
}
