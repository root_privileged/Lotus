package kamala.kalpana.script.shim;

import jdk.internal.dynalink.beans.StaticClass;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import kamala.kalpana.script.AbstractFChatScriptContext;
import kamala.kalpana.script.ScriptPrivilegeLevel;

import java.util.HashMap;

public abstract class AbstractShim extends HashMap<String, Object>
{
  private AbstractFChatScriptContext fChatScriptContext;
  private ScriptPrivilegeLevel scriptPrivilegeLevel;

  public AbstractShim(AbstractFChatScriptContext fChatScriptContext, ScriptPrivilegeLevel scriptPrivilegeLevel)
  {
    this.fChatScriptContext = fChatScriptContext;
    this.scriptPrivilegeLevel = scriptPrivilegeLevel;
  }

  protected abstract void initializeBindings();

  protected AbstractFChatScriptContext getScriptContext()
  {
    return fChatScriptContext;
  }

  public ScriptPrivilegeLevel getScriptPrivilegeLevel()
  {
    return scriptPrivilegeLevel;
  }

  protected ScriptObjectMirror getObjectMirror(String str)
  {
    return containsKey(str) ? (ScriptObjectMirror) get(str) : null;
  }

  /**
   * Helper function to execute a JS side function that has been attached to this object
   * under the specified name using this object as the this pointer and the specified
   * arguments as the arguments to the function, does nothing with the return value by default.
   *
   * @param functionObjectName
   * @param args
   */
  protected ScriptObjectMirror executeFunction(String functionObjectName, Object... args)
  {
    ScriptObjectMirror fnc = getObjectMirror(functionObjectName);

    if(fnc != null)
    {
      if(fnc.isFunction())
      {
        Object rv = fnc.call(this, args);

        if(rv instanceof ScriptObjectMirror)
          return (ScriptObjectMirror) rv;
        else
          return null;
      }
      else if(fnc.isArray())
      {
        fnc.values().stream()
                .filter(x -> x instanceof ScriptObjectMirror)
                .map(f -> (ScriptObjectMirror) f)
                .filter(ScriptObjectMirror::isFunction)
                .forEach(m -> m.call(this, args));

        return null;
      }
    }

    return null;
  }

  /**
   * Gets a ScriptObjectMirror that is a function from the underlying map contained in this
   * shim if it exists, and only if it is a function.
   *
   * @param str
   * @return
   */
  protected ScriptObjectMirror getFunctionMirror(String str)
  {
    ScriptObjectMirror som = getObjectMirror(str);

    return som != null && som.isFunction() ? som : null;
  }

  /**
   * Exposes the specified object under the target key on this object shim in the JS environment.
   *
   * @param key
   * @param obj
   */
  protected void exposeObject(String key, Object obj)
  {
    this.put(key, obj);
  }

  protected void exposeClass(Class clazz)
  {
    String className = clazz.getSimpleName();
    this.put(className, StaticClass.forClass(clazz));
  }
}
