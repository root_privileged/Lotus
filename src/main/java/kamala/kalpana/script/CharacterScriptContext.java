package kamala.kalpana.script;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;

public class CharacterScriptContext
{
  private ScriptEngine scriptEngine;
  private ScriptContext scriptContext;

  private CharacterScriptContext(ScriptEngine scriptEngine, ScriptContext scriptContext)
  {
    this.scriptEngine = scriptEngine;
    this.scriptContext = scriptContext;
  }
}
