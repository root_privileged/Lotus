package kamala.kalpana;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import kamala.kalpana.config.ApplicationConfigurator;
import org.iq80.leveldb.DB;
import org.iq80.leveldb.DBFactory;
import org.iq80.leveldb.Options;
import org.iq80.leveldb.impl.Iq80DBFactory;

import java.io.File;
import java.io.IOException;

public class LotusDatabase
{
  private static final Object lock = new Object();

  private static DB database;
  private static DBFactory factory;

  private static Object mMutex = new Object();
  private static ObjectMapper mObjectMapper;
  public static ObjectMapper getObjectMapper()
  {
    if(mObjectMapper == null)
    {
      synchronized (mMutex)
      {
        mObjectMapper = new ObjectMapper();
        mObjectMapper.disable(SerializationFeature.INDENT_OUTPUT);

        mObjectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
      }
    }

    return mObjectMapper;
  }

  public static final void InitializeDatabase()
  {
    synchronized (lock)
    {
      factory = new Iq80DBFactory();

      try
      {
        database = factory.open(new File(ApplicationConfigurator.getDatabaseURL()), getOptions());
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  public static DB getDatabase()
  {
    return database;
  }

  public static final void ShutdownDatabase()
  {
    synchronized (lock)
    {
      try
      {
        database.close();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }

  private static Options getOptions()
  {
    Options opt = new Options();
    opt.createIfMissing(true);
    opt.cacheSize(64 * 1048576); // 64 MB

    return opt;
  }
}
