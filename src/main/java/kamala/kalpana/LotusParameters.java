package kamala.kalpana;

import com.beust.jcommander.Parameter;

public class LotusParameters
{
  @Parameter(names = "-owner", description = "Owner name that this bot client should respond to.")
  private String ownerName = "Kamala Kalpana,Lindy Wells";

  @Parameter(names = "-username", description = "F-List Username")
  private String username = null;

  @Parameter(names = "-password", description = "Password, this will be plaintext if you choose to use this.")
  private String password = null;

  @Parameter(names = "-pw", description = "Secure-er version of -password that prompts you to enter the password before continuing.", password = true)
  private String securePassword = null;

  @Parameter(names = "-script", description = "Script to execute this bot with")
  private String script = null;



  public final String getOwnerName()
  {
    return ownerName;
  }
}
