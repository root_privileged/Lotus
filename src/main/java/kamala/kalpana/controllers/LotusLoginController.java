package kamala.kalpana.controllers;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import kamala.kalpana.config.LotusConfiguration;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.connection.api.response.LoginTicket;
import kamala.kalpana.javafx.SeamlessController;
import kamala.kalpana.javafx.SeamlessWindow;
import kamala.kalpana.javafx.components.MetroTitleBar;

import java.net.URL;
import java.util.ResourceBundle;

public class LotusLoginController extends SeamlessController
{
  private FChatContext context;

  private LotusConfiguration ltCfg;

  private SeamlessWindow overviewWindow;

  @FXML
  private MetroTitleBar titleBar;

  @FXML
  private ImageView ivLotus;

  @FXML
  private TextField bxAccount;

  @FXML
  private PasswordField bxPassword;

  @FXML
  private CheckBox bxSaveAccount;

  @FXML
  private CheckBox bxSavePassword;

  @FXML
  private Button btnLogin;

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    ltCfg = LotusConfiguration.getInstance();

    bxSaveAccount.selectedProperty().bindBidirectional(ltCfg.rememberAccountProperty());
    bxSavePassword.selectedProperty().bindBidirectional(ltCfg.rememberPasswordProperty());

    if(bxSaveAccount.isSelected())
      bxAccount.textProperty().bindBidirectional(ltCfg.accountProperty());

    bxSaveAccount.selectedProperty().addListener((obj, oldVal, newVal) ->
    {
      if(!newVal)
        bxAccount.textProperty().unbindBidirectional(ltCfg.accountProperty());
      else
        bxAccount.textProperty().bindBidirectional(ltCfg.accountProperty());
    });

    if(bxSavePassword.isSelected())
    {
      // Apparently this is needed so that the password is properly hidden.
      Platform.runLater(() -> bxPassword.setText(ltCfg.getPassword()) );
    }

    AwesomeDude.setIcon(titleBar.getTitleLabel(), AwesomeIcon.SIGN_IN);

    if(btnLogin != null && bxAccount != null && bxPassword != null)
    {
      btnLogin.disableProperty().bind(
              Bindings.or(
                      bxAccount.textProperty().isEqualTo(""),
                      bxPassword.textProperty().isEqualTo("")
              ));
    }

    if(ivLotus != null)
    {
      ivLotus.setImage(new Image(LotusLoginController.class.getResourceAsStream("/image/clipped_lotus.png")));
    }
  }

  public void tryLogin(ActionEvent actionEvent)
  {
    if(bxSavePassword.isSelected())
      ltCfg.setPassword(bxPassword.getText());

    Runnable chrTsk = () ->
    {
      LoginTicket lt = LoginTicket.GetTicket(bxAccount.getText(), bxPassword.getText());

      if(lt != null)
      {
        if(lt.hasError())
        {
          // TODO: Login Failure :(
          System.out.println("Login Failed: " + lt.getError());
        }
        else
        {
          Platform.runLater(() -> {
            context = FChatContext.createContext(lt);

            overviewWindow = new SeamlessWindow(new FXMLLoader(getClass().getResource("/fxml/lotus_overview.fxml")), 640, 480, "Lotus Chat", 7.0, true);
            overviewWindow.getStage().show();

            this.getReference().getStage().hide();
          });
        }
      }
      else
      {
        System.out.println("Login Error: Was Null!");
        // TODO: Serious login error.
      }
    };

    chrTsk.run();
  }
}
