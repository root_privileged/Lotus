package kamala.kalpana.controllers;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import kamala.kalpana.connection.FChatSocketClient;
import kamala.kalpana.connection.api.response.FriendEntry;
import kamala.kalpana.connection.api.response.LoginTicket;
import kamala.kalpana.javafx.SeamlessController;
import org.tbee.javafx.scene.layout.fxml.MigPane;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class LotusDebugController extends SeamlessController
{
  @FXML
  private MigPane debugPane, debugBody, ticketRequestPane, characterLoginPane;

  @FXML
  private Button btnLogin;

  @FXML
  private TextField bxAccount;

  @FXML
  private PasswordField bxPassword;

  @FXML
  public TreeView<String> trFriends;

  @FXML
  private ListView<String> lstCharacters;

  private LoginTicket lt;
  private FChatSocketClient fsc;

  private Map<String, List<String>> charFriendMapping = new HashMap<>();

  @FXML
  private void executeLoginAttempt(ActionEvent actionEvent)
  {
    Platform.runLater(() ->
    {
      lt = LoginTicket.GetTicket(bxAccount.getText(), bxPassword.getText());

      if(lt.hasError())
      {
        lt = null;
        bxAccount.clear();
        bxPassword.clear();
      }
      else
      {
        //lt.getCharacters()
        lstCharacters.setItems(FXCollections.observableArrayList("T1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t10"));
        lstCharacters.getSelectionModel().select(lt.getDefaultCharacter());

        TreeItem<String> rootItem = new TreeItem<>("Test Tree"); // lt.getAccountName() + "'s Friend Tree"


        List<String> mycharswithfriends = lt.getFriends().stream().map(FriendEntry::getMyCharacter).distinct().collect(Collectors.toList());

        for(String mc : mycharswithfriends)
          charFriendMapping.put(mc, lt.getFriends().stream().filter(f -> f.getMyCharacter() == mc).map(FriendEntry::getTheirCharacter).collect(Collectors.toList()));

        for(Map.Entry<String, List<String>> mEntry : charFriendMapping.entrySet())
        {
          String pk1 = "DamnBuggyLayout";//mEntry.getKey();

          TreeItem<String> newChr = new TreeItem<>(pk1);

          for(String fc : mEntry.getValue())
          {
            String pk2 = "DamnBuggyTree"; // fc

            newChr.getChildren().add(new TreeItem<>(pk2));
          }

          rootItem.getChildren().add(newChr);
        }

        trFriends.setRoot(rootItem);
      }

      ticketRequestPane.setDisable(lt != null);
    });
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    btnLogin.disableProperty().bind(
            Bindings.or(
                    bxAccount.textProperty().isEqualTo(""),
                    bxPassword.textProperty().isEqualTo("")
            ));

    characterLoginPane.disableProperty().bind(Bindings.not(ticketRequestPane.disabledProperty()));

    AwesomeDude.setIcon(btnLogin, AwesomeIcon.COGS);
  }
}
