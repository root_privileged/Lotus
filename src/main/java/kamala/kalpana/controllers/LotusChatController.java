package kamala.kalpana.controllers;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.IPacketReceiver;
import kamala.kalpana.connection.packet.ServerPacket;
import kamala.kalpana.connection.actions.OnChannelJoin;
import kamala.kalpana.connection.packet.server.PrivateMessage;
import kamala.kalpana.javafx.SeamlessController;
import kamala.kalpana.javafx.SeamlessWindow;
import kamala.kalpana.javafx.chat.ChannelTab;
import kamala.kalpana.javafx.chat.ConsoleTab;
import kamala.kalpana.javafx.chat.PrivateMessageTab;
import kamala.kalpana.javafx.components.MetroTitleBar;
import org.eclipse.fx.ui.controls.tabpane.DndTabPaneFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tbee.javafx.scene.layout.fxml.MigPane;

import java.net.URL;
import java.util.ResourceBundle;

public class LotusChatController extends SeamlessController implements IPacketReceiver
{
  private static final Logger logger = LoggerFactory.getLogger(LotusChatController.class);


  public static final double ICON_SIZE = 45.0;

  private SeamlessWindow selfWindow;
  private FChatContext fChatContext;
  private ChatContext chatContext;

  @FXML
  private MigPane chatWindowPane, chatInternalPane;

  @FXML
  private MetroTitleBar titleBar;

  @FXML
  private Button btnChannelList;

  @FXML
  private HBox controlBarPane;


  private Pane draggableTabPaneRoot;
  private TabPane draggableTabPane;


  private ConsoleTab consoleTab;

  public LotusChatController(FChatContext fChatContext, ChatContext chatContext)
  {
    this.fChatContext = fChatContext;
    this.chatContext = chatContext;

    chatContext.getActionBus().register(this);
    chatContext.getSocketClient().receiverBus().register(this);
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    titleBar.setText("Lotus Chat - " + chatContext.getCharacterName());

    chatInternalPane.setCols("[grow, fill]");
    chatInternalPane.setRows("[grow, fill]");

    setupTabPane();
  }

  private void setupTabPane()
  {
    HBox h = new HBox();

    {
      draggableTabPaneRoot = DndTabPaneFactory.createDefaultDnDPane(DndTabPaneFactory.FeedbackType.MARKER, this::initializeTabs);
      HBox.setHgrow(draggableTabPaneRoot, Priority.ALWAYS);
      h.getChildren().add(draggableTabPaneRoot);
    }

    draggableTabPane.setTabMaxHeight(60.0);
    draggableTabPane.setTabMinHeight(60.0);
    draggableTabPane.setTabMaxWidth(50.0);
    draggableTabPane.setTabMinWidth(50.0);
    draggableTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

    draggableTabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>()
    {
      @Override
      public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue)
      {
        if(oldValue != newValue)
        {
          Button defaultBtn = (Button) newValue.getContent().lookup(".defaultButton");

          if(defaultBtn != null)
          {
            // Why disable then enable? Go figure.
            defaultBtn.setDefaultButton(false);
            defaultBtn.setDefaultButton(true);
          }
        }
      }
    });

    chatInternalPane.add(draggableTabPane, "cell 0 0, grow, push");
  }

  private void initializeTabs(TabPane tb)
  {
    draggableTabPane = tb;

    draggableTabPane.getTabs().add(consoleTab = new ConsoleTab(fChatContext, chatContext));
  }

  public SeamlessWindow getWindow()
  {
    return selfWindow;
  }
  public void setWindow(SeamlessWindow selfWindow)
  {
    this.selfWindow = selfWindow;
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onJoin(OnChannelJoin ucj)
  {
    Platform.runLater(() -> {
      logger.trace("Creating Channel Tab: {}", ucj.getChannelID());

      ChannelContext cctx = ucj.getChannelContext();
      draggableTabPane.getTabs().add(new ChannelTab(cctx));
    });
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onPrivateMessage(PrivateMessage pm)
  {
    // TODO: PM Ignore Handling.

    Platform.runLater(() -> {
      if(!draggableTabPane.getTabs().filtered(t -> t instanceof PrivateMessageTab).stream().anyMatch(pmt ->
              ((PrivateMessageTab) pmt).getCharacterReference().getName().equals(pm.getCharacter()))
      )
      {
        PrivateMessageTab pmt = new PrivateMessageTab(fChatContext, chatContext, chatContext.getCharacters().get(pm.getCharacter()));
        pmt.postMessage(pm.getCharacter(), pm.getMessage());

        draggableTabPane.getTabs().add(pmt);
      }
    });
  }
}
