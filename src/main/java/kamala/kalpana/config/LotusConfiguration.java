package kamala.kalpana.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.*;

import java.io.File;
import java.io.IOException;

public class LotusConfiguration
{
  private static String cfgFilename = "cfg.json";
  private static File cfgFile;

  private static LotusConfiguration mInstance;
  public static LotusConfiguration getInstance()
  {
    if(mInstance == null)
    {
      cfgFile = new File(ApplicationConfigurator.getConfigurationDirectory().toFile(), cfgFilename);

      try
      {
        if(cfgFile.exists())
        {
          mInstance = ApplicationConfigurator.getObjectMapper().readValue(cfgFile, LotusConfiguration.class);
        }
        else
        {
          mInstance = new LotusConfiguration();
          mInstance.saveConfiguration();
        }
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }

    return mInstance;
  }

  private DoubleProperty mPositionX, mPositionY, mWidth, mHeight;

  private BooleanProperty rememberAccount, rememberPassword;
  private StringProperty account, password;

  private LotusConfiguration()
  {
    this.mPositionX = new SimpleDoubleProperty(150);
    this.mPositionY = new SimpleDoubleProperty(150);
    this.mWidth = new SimpleDoubleProperty(1336);
    this.mHeight = new SimpleDoubleProperty(768);

    this.rememberAccount = new SimpleBooleanProperty(true);
    this.rememberPassword = new SimpleBooleanProperty(false);

    this.account = new SimpleStringProperty("");
    this.password = new SimpleStringProperty("");
  }

  public void saveConfiguration()
  {
    try
    {
      ApplicationConfigurator.getObjectMapper().writeValue(cfgFile, mInstance);
    } catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  @JsonIgnore
  public DoubleProperty xProperty() { return mPositionX; }

  @JsonIgnore
  public DoubleProperty yProperty() { return mPositionY; }

  @JsonIgnore
  public DoubleProperty widthProperty() { return mWidth; }

  @JsonIgnore
  public DoubleProperty heightProperty() { return mHeight; }

  @JsonIgnore
  public BooleanProperty rememberAccountProperty() { return rememberAccount; }

  @JsonIgnore
  public BooleanProperty rememberPasswordProperty() { return rememberPassword; }

  @JsonIgnore
  public StringProperty accountProperty() { return account; }

  @JsonIgnore
  public StringProperty passwordProperty() { return password; }

  @JsonProperty("position_x")
  public double getX() { return mPositionX.get(); }
  public void setX(double newX) { mPositionX.set(newX); }

  @JsonProperty("position_y")
  public double getY() { return mPositionY.get(); }
  public void setY(double newY) { mPositionY.set(newY); }

  @JsonProperty("width")
  public double getWidth() { return mWidth.get(); }
  public void setWidth(double nWidth) { mWidth.set(nWidth); }

  @JsonProperty("height")
  public double getHeight() { return mHeight.get(); }
  public void setHeight(double nHeight) { mHeight.set(nHeight); }

  @JsonProperty("remember_account")
  public boolean getRememberAccount() { return rememberAccount.get(); }
  public void setRememberAccount(boolean val) { rememberAccount.set(val); }

  @JsonProperty("remember_password")
  public boolean getRememberPassword() { return rememberPassword.get(); }
  public void setRememberPassword(boolean val) { rememberPassword.set(val); }

  @JsonProperty("account")
  public String getAccount() { return account.get(); }
  public void setAccount(String val) { account.set(val); }

  @JsonProperty("password")
  public String getPassword() { return password.get(); }
  public void setPassword(String val) { password.set(val); }
}
