package kamala.kalpana.javafx.property;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.MapChangeListener;

public class IntegerMapListeningProperty extends SimpleIntegerProperty implements MapChangeListener
{
  public IntegerMapListeningProperty() {}

  public IntegerMapListeningProperty(int initialValue)
  {
    super(initialValue);
  }

  public IntegerMapListeningProperty(Object bean, String name)
  {
    super(bean, name);
  }

  public IntegerMapListeningProperty(Object bean, String name, int initialValue)
  {
    super(bean, name, initialValue);
  }

  @Override
  public void onChanged(Change change)
  {
    if(change.wasAdded())
      super.set(super.get() + 1);
    else if(change.wasRemoved())
      super.set(super.get() - 1);
  }
}
