package kamala.kalpana.javafx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class SeamlessWindow
{
  private SeamlessController controller;
  private FXMLLoader loader;
  private Parent root;
  private Stage stage;
  private Scene scene;

  private ResizeListener resizeHandler;

  public static SeamlessWindow buildWindow(String FXMLFilename, Stage primaryStage, String title)
  {
   return null;
  }

  public static <T extends SeamlessController> SeamlessWindow buildWithController(String FXMLFilename, T controller, double width, double height, String title, double resizeBorder, boolean allowResize)
  {
    SeamlessWindow newWindow = new SeamlessWindow();

    newWindow.loader = new FXMLLoader(SeamlessWindow.class.getResource(FXMLFilename));

    newWindow.stage = new Stage(StageStyle.UNDECORATED);
    newWindow.stage.setTitle(title);
    newWindow.loader.setController(controller);
    newWindow.controller = controller;

    controller.setReference(newWindow);

    try
    {
      newWindow.root = newWindow.loader.load();
      newWindow.scene = new Scene(newWindow.root, width, height);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    if(allowResize)
    {
      newWindow.resizeHandler = new ResizeListener(newWindow.stage, newWindow.scene, resizeBorder);

      newWindow.scene.setOnMouseMoved(newWindow.resizeHandler);
      newWindow.scene.setOnMousePressed(newWindow.resizeHandler);
      newWindow.scene.setOnMouseDragged(newWindow.resizeHandler);
    }

    newWindow.stage.setScene(newWindow.scene);


    return newWindow;
  }

  private SeamlessWindow()
  {
  }


  public SeamlessWindow(FXMLLoader loader, Stage primaryStage, String title)
  {
    this(loader, primaryStage, 640, 480, title);

  }

  public SeamlessWindow(FXMLLoader loader, Stage primaryStage, double width, double height, String title)
  {
    this(loader, primaryStage, width, height, title, 7.0, true);
  }

  public SeamlessWindow(FXMLLoader loader, Stage primaryStage, double width, double height, String title, double resizeBorder, boolean allowResize)
  {
    this.stage = primaryStage;

    primaryStage.initStyle(StageStyle.UNDECORATED);

    this.loader = loader;

    this.stage.setTitle(title);

    try
    {
      this.root = loader.load();
      this.scene = new Scene(root, width, height);

      final SeamlessController sc = loader.getController();
      this.controller = sc;
      sc.setReference(this);
    } catch (IOException e)
    {
      e.printStackTrace();
    }


    if(allowResize)
    {
      resizeHandler = new ResizeListener(this.stage, this.scene, resizeBorder);

      this.scene.setOnMouseMoved(resizeHandler);
      this.scene.setOnMousePressed(resizeHandler);
      this.scene.setOnMouseDragged(resizeHandler);
    }

    this.stage.setScene(this.scene);
  }

  public SeamlessWindow(FXMLLoader loader, String title)
  {
    this(loader, 640, 480, title);
  }

  public SeamlessWindow(FXMLLoader loader, double width, double height, String title)
  {
    this(loader, width, height, title, 7.0, true);
  }

  public SeamlessWindow(FXMLLoader loader, double width, double height, String title, double resizeBorder, boolean allowResize)
  {
    this.loader = loader;

    this.stage = new Stage(StageStyle.UNDECORATED);
    this.stage.setTitle(title);

    try
    {
      this.root = loader.load();
      this.scene = new Scene(root, width, height);

      final SeamlessController sc = loader.getController();
      this.controller = sc;
      sc.setReference(this);
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    if(allowResize)
    {
      resizeHandler = new ResizeListener(this.stage, this.scene, resizeBorder);

      this.scene.setOnMouseMoved(resizeHandler);
      this.scene.setOnMousePressed(resizeHandler);
      this.scene.setOnMouseDragged(resizeHandler);
    }

    this.stage.setScene(this.scene);
  }

  public final Parent getRoot() { return root; }
  public final Stage getStage() { return stage; }
  public final Scene getScene() { return scene; }
  public final SeamlessController getController() { return controller; }
}
