package kamala.kalpana.javafx.chat;

import kamala.kalpana.connection.context.FChatContext;

public class LoginAvatarGridCell extends AvatarGridCell
{
  private FChatContext context;

  public LoginAvatarGridCell(FChatContext ctx)
  {
    super();

    this.context = ctx;

    this.setOnMouseClicked(evt -> {


      ctx.createChatWindow(getCharacterName());
    });
  }

  public LoginAvatarGridCell(boolean preserveImageProperties)
  {
    super(preserveImageProperties);
  }
}
