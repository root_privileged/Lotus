package kamala.kalpana.javafx.chat;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.packet.IPacketReceiver;
import kamala.kalpana.connection.packet.client.ChannelMessageRequest;
import kamala.kalpana.connection.packet.server.*;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.controllers.LotusChatController;
import kamala.kalpana.javafx.components.ManagedTab;
import kamala.kalpana.javafx.event.ShiftKeyFilter;
import kamala.kalpana.javafx.forward.ListForwardingListener;

import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.ResourceBundle;

public class ChannelTab extends ManagedTab implements Initializable, IPacketReceiver
{
  private static final Rectangle2D DOOR_BOUNDS = new Rectangle2D(22.0, 19.0, 32.0, 38.0);

  private static final Image DOOR_OPEN = new Image(ChannelTab.class.getResourceAsStream("/image/metro_icons/appbar.door.lock.open.png"));
  private static final Image DOOR_CLOSED = new Image(ChannelTab.class.getResourceAsStream("/image/metro_icons/appbar.door.lock.closed.png"));

  private ChatContext chatContext;
  private ChannelContext channelContext;
  private ChannelReference channelReference;

  private ImageView icon;

  private FXMLLoader loader;

  //////////////////////////////////////////////////////////////////////
  private ObservableList<CharacterReference> characterList;
  private SortedList<CharacterReference> sortedCharacterList;
  private Comparator<CharacterReference> characterReferenceComparator;
  private ListForwardingListener<CharacterReference> characterForwardingListener;

  private ListProperty<CharacterReference> characterReferenceListProperty;
  //////////////////////////////////////////////////////////////////////

  @FXML
  private ListView<String> conversationView;

  @FXML
  private ListView<CharacterReference> characterListView;

  @FXML
  private TextArea chatInput;

  @FXML
  private Label lblCharacterCount;

  @FXML
  private Button btnSubmit;

  public ChannelTab(ChannelContext channelContext)
  {
    super(channelContext.getReference().getTitle());

    this.chatContext = channelContext.getChatContext();
    this.channelReference = channelContext.getReference();
    this.channelContext = channelContext;

    if(channelReference.isPrivateChannel())
      icon = new ImageView(DOOR_CLOSED);
    else
      icon = new ImageView(DOOR_OPEN);

    icon.setViewport(DOOR_BOUNDS);
    icon.setPreserveRatio(true);
    icon.setFitWidth(LotusChatController.ICON_SIZE);
    icon.setCache(true);

    super.setGraphic(icon);


    // Copy all current characters into the new character list before anything.
    this.characterList = FXCollections.observableArrayList(channelContext.getCharacters());
    this.characterReferenceComparator = Comparator.comparing(CharacterReference::getName);
    this.sortedCharacterList = characterList.sorted(characterReferenceComparator);
    this.characterForwardingListener = new ListForwardingListener<>(characterList);

    this.characterReferenceListProperty = new SimpleListProperty<>(this, "characters", characterList);

    chatContext.getActionBus().register(this);
    chatContext.getSocketClient().receiverBus().register(this);
    chatContext.getSocketClient().senderBus().register(this);

    channelContext.getCharacters().addListener(characterForwardingListener);

    this.loader = new FXMLLoader(getClass().getResource("/fxml/tabs/lotus_channel_tab.fxml"));
    loader.setController(this);

    try
    {
      super.setContent(loader.load());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    super.setOnClosed(evt -> {
      chatContext.getActionBus().unregister(this);
      chatContext.getSocketClient().receiverBus().unregister(this);
      chatContext.getSocketClient().senderBus().unregister(this);

      channelContext.getCharacters().removeListener(characterForwardingListener);
    });
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    lblCharacterCount.textProperty().bind(Bindings.format("Characters: %d", characterReferenceListProperty.sizeProperty()));

    characterListView.setItems(sortedCharacterList);
    characterListView.setCellFactory(param -> new CharacterCell());

    chatInput.addEventFilter(KeyEvent.KEY_PRESSED, new ShiftKeyFilter(chatInput));

    btnSubmit.disableProperty().bind(chatInput.textProperty().isEqualTo(""));
    btnSubmit.setOnAction(evt -> {
      conversationView.getItems().add(chatContext.getCharacterName() + " : " + chatInput.getText());
      chatContext.getSocketClient().sendCommand(new ChannelMessageRequest(channelReference.getIdentifier(), chatInput.getText()));
      chatInput.setText("");
    });
  }

  static class CharacterCell extends ListCell<CharacterReference>
  {
    @Override
    protected void updateItem(CharacterReference item, boolean empty)
    {
      super.updateItem(item, empty);

      if(item != null && !empty)
      {
        setGraphic(new Label(item.getName()));
      }
      else
        setGraphic(null);
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onKick(ChannelKick ck)
  {
    if(ck.getChannel().equals(channelReference.getIdentifier()))
    {
      Platform.runLater(() -> {
        this.getTabPane().getTabs().remove(this);
      });
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onMessage(ChannelMessage cm)
  {
    if(cm.getChannel().equals(channelReference.getIdentifier()))
    {
      Platform.runLater(() -> {
        conversationView.getItems().add(cm.getCharacter() + " : " + cm.getMessage());
      });
    }
  }
}
