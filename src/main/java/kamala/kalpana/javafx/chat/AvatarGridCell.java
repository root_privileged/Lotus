package kamala.kalpana.javafx.chat;

import org.controlsfx.control.GridCell;

public class AvatarGridCell extends GridCell<String>
{
  private AvatarView imageView;
  private String characterName;


  private final boolean preserveImageProperties;


  /**
   * Creates a default ImageGridCell instance, which will preserve image properties
   */
  public AvatarGridCell()
  {
    this(true);
  }

  /**
   * Create ImageGridCell instance
   * @param preserveImageProperties if set to true will preserve image aspect ratio and smoothness
   */
  public AvatarGridCell( boolean preserveImageProperties )
  {
    getStyleClass().add("image-grid-cell"); //$NON-NLS-1$

    this.preserveImageProperties = preserveImageProperties;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void updateItem(String item, boolean empty)
  {
    super.updateItem(item, empty);

    if (empty)
      setGraphic(null);
    else
    {
      characterName = item;

      imageView = new AvatarView(item);

      imageView.fitHeightProperty().bind(heightProperty());
      imageView.fitWidthProperty().bind(widthProperty());

      if (preserveImageProperties)
      {
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
      }

      setGraphic(imageView);
    }
  }

  public final String getCharacterName() { return characterName; }
}
