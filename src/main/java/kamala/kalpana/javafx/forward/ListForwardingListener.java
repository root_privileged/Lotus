package kamala.kalpana.javafx.forward;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class ListForwardingListener<E> implements ListChangeListener<E>
{
  private ObservableList<E> targetList;

  public ListForwardingListener(ObservableList<E> targetList)
  {
    this.targetList = targetList;
  }

  @Override
  public void onChanged(Change<? extends E> c)
  {
    Platform.runLater(() -> {
      while(c.next())
      {
        for(E removedItem : c.getRemoved())
        {
          targetList.remove(removedItem);
        }
        for(E addedItem : c.getAddedSubList())
        {
          targetList.add(addedItem);
        }
      }
    });
  }
}