package kamala.kalpana.javafx.components;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.control.Tab;
import org.eclipse.fx.ui.controls.DraggableTab;

public class ManagedTab extends DraggableTab
{
  public ManagedTab() { }

  public ManagedTab(String text)
  {
    super(text);
  }

  public void close()
  {
    Platform.runLater(() ->
    {
      if (getOnClosed() != null)
      {
        Event.fireEvent(this, new Event(Tab.CLOSED_EVENT));
      }

      this.getTabPane().getTabs().remove(this);
    });
  }
}
