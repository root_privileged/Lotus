package kamala.kalpana.javafx.components;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.tbee.javafx.scene.layout.fxml.MigPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class MetroStatusBar extends MigPane implements Initializable
{
  private static final Logger logger = Logger.getLogger(MetroStatusBar.class.getName());

  @FXML
  private Parent root;

  private Label lblStatus;
  private Image dragImage;

  public MetroStatusBar()
  {
    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/components/MetroStatusBar.fxml"));

    loader.setRoot(this);
    loader.setController(this);

    lblStatus = new Label();
    dragImage = new Image(getClass().getResourceAsStream("/image/resizeSE.png"));

    try
    {
      root = loader.load();
    }
    catch(IOException exception)
    {
      throw new RuntimeException(exception);
    }
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    this.add(lblStatus, "cell 0 0");
    this.add(new ImageView(dragImage), "id di, pos 100%-(di.w * 1.2) 100%-(di.h * 0.8)");
  }

  public Label getStatusLabel() { return lblStatus; }

  public void setText(String ee) { lblStatus.textProperty().set(ee); }
  public String getText() { return lblStatus.textProperty().get(); }
  public StringProperty textProperty() { return lblStatus.textProperty(); }
}
