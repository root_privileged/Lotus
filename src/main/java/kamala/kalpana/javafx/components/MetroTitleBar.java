package kamala.kalpana.javafx.components;


import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.tbee.javafx.scene.layout.fxml.MigPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class MetroTitleBar extends MigPane implements Initializable
{
  private static final Logger logger = Logger.getLogger(MetroTitleBar.class.getName());

  private static final EventHandler<ActionEvent> DEFAULT_EXIT_BEHAVIOR = (evt) -> Platform.exit();

  @FXML
  private Parent root;

  private double xOffset, yOffset;

  private BooleanProperty enableExitButton;
  private BooleanProperty enableMinimizeButton;
  private BooleanProperty enableFullscreenButton;

  private BooleanProperty seperatorProperty;
  private BooleanProperty dragProperty;

  private Label lblTitle;

  private Button exitButton;
  private Button minimizeButton;
  private Button fullscreenButton;

  public MetroTitleBar()
  {
    enableExitButton = new SimpleBooleanProperty(true);
    enableMinimizeButton = new SimpleBooleanProperty(true);
    enableFullscreenButton = new SimpleBooleanProperty(true);

    seperatorProperty = new SimpleBooleanProperty(false);
    dragProperty = new SimpleBooleanProperty(true);

    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/components/MetroTitlebar.fxml"));

    loader.setRoot(this);
    loader.setController(this);

    try
    {
      root = loader.load();
    }
    catch(IOException exception)
    {
      throw new RuntimeException(exception);
    }
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    exitButton = new Button();
    exitButton.getStyleClass().addAll("metroGraphicButton", "exitButton");
    exitButton.setOnAction(DEFAULT_EXIT_BEHAVIOR);

    minimizeButton = new Button();
    minimizeButton.getStyleClass().addAll("metroGraphicButton", "minimizeButton");
    minimizeButton.setOnAction((evt) ->
    {
      ((Stage) root.getScene().getWindow()).setIconified(true);
    });

    fullscreenButton = new Button();
    fullscreenButton.getStyleClass().addAll("metroGraphicButton", "buttonMaximize");
    fullscreenButton.setOnAction((evt) ->
    {
      Stage cStage = (Stage) root.getScene().getWindow();;

      ObservableList<String> styles = fullscreenButton.getStyleClass();

      if(cStage.isMaximized())
      {
        if(styles.remove("buttonRestore") && styles.add("buttonMaximize"))
        {
          cStage.setMaximized(false);
        }
      }
      else
      {
        if(styles.remove("buttonMaximize") && styles.add("buttonRestore"))
        {
          cStage.setMaximized(true);
        }
      }
    });

    this.setOnMousePressed((evt) ->
    {
      if(getDraggable())
      {
        xOffset = evt.getSceneX();
        yOffset = evt.getSceneY();
      }
    });

    this.setOnMouseDragged((evt) ->
    {
      if(getDraggable())
      {
        Stage stage = (Stage) root.getScene().getWindow();

        stage.setX(evt.getScreenX() - xOffset);
        stage.setY(evt.getScreenY() - yOffset);
      }
    });

    lblTitle = new Label();
    lblTitle.getStyleClass().add("titleText");
    this.add(lblTitle, "cell 0 0, center");

    if(getExit())
      this.add(exitButton, "cell 4 0");
    if(getMinimize())
      this.add(minimizeButton, "cell 2 0");
    if(getMaximize())
      this.add(fullscreenButton, "cell 3 0");

    if(getSeperator())
      this.getStyleClass().add("contentBorder");

    seperatorProperty.addListener((bp, oldValue, newValue) ->
    {
      if(!newValue)
        this.getStyleClass().remove("contentBorder");
      else
        this.getStyleClass().add("contentBorder");
    });

    exitProperty().addListener((bp, oldValue, newValue) ->
    {
      if(!newValue)
        this.remove(exitButton);
      else
        this.add(exitButton, "cell 4 0");
    });

    minimizeProperty().addListener((bp, oldValue, newValue) ->
    {
      if (!newValue)
        this.remove(minimizeButton);
      else
        this.add(minimizeButton, "cell 2 0");
    });

    maximizeProperty().addListener((bp, oldValue, newValue) ->
    {
      if (!newValue)
        this.remove(fullscreenButton);
      else
        this.add(fullscreenButton, "cell 3 0");
    });
  }

  public void setExitButtonBehavior(EventHandler<ActionEvent> exitHandler)
  {
    exitButton.setOnAction(exitHandler);
  }

  public Label getTitleLabel() { return lblTitle; }

  public void setText(String ee) { lblTitle.textProperty().set(ee); }
  public String getText() { return lblTitle.textProperty().get(); }
  public StringProperty textProperty() { return lblTitle.textProperty(); }

  public void setExit(boolean ee) { enableExitButton.set(ee); }
  public boolean getExit() { return enableExitButton.get(); }
  public BooleanProperty exitProperty() { return enableExitButton; }

  public void setMinimize(boolean ee) { enableMinimizeButton.set(ee); }
  public boolean getMinimize() { return enableMinimizeButton.get(); }
  public BooleanProperty minimizeProperty() { return enableMinimizeButton; }

  public void setMaximize(boolean ee) { enableFullscreenButton.set(ee); }
  public boolean getMaximize() { return enableFullscreenButton.get(); }
  public BooleanProperty maximizeProperty() { return enableFullscreenButton; }

  public void setSeperator(boolean ee) { seperatorProperty.set(ee); }
  public boolean getSeperator() { return seperatorProperty.get(); }
  public BooleanProperty seperatorProperty() { return seperatorProperty; }

  public void setDraggable(boolean ee) { dragProperty.set(ee); }
  public boolean getDraggable() { return dragProperty.get(); }
  public BooleanProperty draggableProperty() { return dragProperty; }

}
