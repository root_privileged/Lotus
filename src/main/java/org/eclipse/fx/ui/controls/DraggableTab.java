package org.eclipse.fx.ui.controls;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Tab;

public class DraggableTab extends Tab
{
  private BooleanProperty canDrag;
  private BooleanProperty isDragging;
  private BooleanProperty pinned;

  public DraggableTab()
  {
    canDrag = new SimpleBooleanProperty(this, "canDrag", true);
    isDragging = new SimpleBooleanProperty(this, "isDragging", false);
    pinned = new SimpleBooleanProperty(this, "pinned", false);
  }

  public DraggableTab(String text)
  {
    super(text);

    canDrag = new SimpleBooleanProperty(this, "canDrag", true);
    isDragging = new SimpleBooleanProperty(this, "isDragging", false);
    pinned = new SimpleBooleanProperty(this, "pinned", false);
  }

  public final BooleanProperty canDragProperty()
  {
    return canDrag;
  }

  public final BooleanProperty isDraggingProperty()
  {
    return isDragging;
  }

  public final BooleanProperty pinnedProperty() { return pinned; }

  public final boolean canDrag()
  {
    return canDragProperty().get();
  }
  public final void setCanDrag(boolean canDrag)
  {
    canDragProperty().set(canDrag);
  }

  public final boolean isDragging()
  {
    return isDraggingProperty().get();
  }
  public final void setIsDragging(boolean isDragging)
  {
    isDraggingProperty().set(isDragging);
  }

  public final boolean isPinned() { return pinnedProperty().get(); }
  public final void setIsPinned(boolean isPinned) { pinnedProperty().set(isPinned);}
}
