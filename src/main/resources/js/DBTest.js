var objPrint = function(obj) {
    var propValue;

    for(var propName in obj)
    {
        propValue = obj[propName];

        print(propName + " | " + propValue);
    }
};

print('Saving Data: ' + Database.save('testKey', { test : 'test' }));


var ld = Database.load('testKey');
print('Reading Data: ');

objPrint(ld);
print('Re-Saving Data: ' + Database.save('testKey', ld));
print('Saving Data 2: ' + Database.save('testKey2', { test : 'testValue2' }));

var ld2 = Database.load('testKey2');
print('Reading Data 2: ');

objPrint(ld2);

print('Re-Saving Data 2: ' + Database.save('testKey2', ld2));

print('Reading Data: ');


var testObject = {
    test: "lol",
    tarr: [" lol2", "lol4"]
}

print(JSON.stringify(testObject));

LotusEnvironment.printObject(testObject);


LotusEnvironment.printObject(ld);
