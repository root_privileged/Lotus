var messageAuthenticator = function(characterReference, command, arguments) {
    var rv = isOwner(characterReference);

    if(!rv)
    {
        characterReference.sendMessage("[url=https://www.youtube.com/watch?v=dqM8mjpMWL8]I'm sorry are you addressing me? Because your authority is not recognized in...'Fort Kick-Ass'.[/url]");
    }

    return rv;
};

var messageCommands = {
    'exit': function(characterReference, command, arguments) {
        Chat.disconnect();
        FChat.exit();
    },
    'join': function(characterReference, command, arguments) {
        Chat.joinChannel(arguments.join(' '));
    },
    'evaluate': function(characterReference, command, arguments) {
        Script.evaluate(arguments.join(' '))
    }
};

var defaultPrivateMessageHandlers = [
    CreateCommandListener('!', messageAuthenticator, messageCommands)
];


var channelAuthenticator = function(channelContext, characterReference, command, arguments) {
    return isOwner(characterReference) || characterReference.isChannelModerator(channelContext.getChannelContext());
};

var channelCommands = {
    'KRIEGER!': function(channelContext, characterReference, command, arguments) {
        channelContext.sendMessage("Smokebomb!");
    },
    '!evaluate': function(channelContext, characterReference, command, arguments) {
        Script.evaluate(arguments.join(' '));
    }
};

var defaultChannelMessageHandlers = [
    CreateChannelCommandListener('', channelAuthenticator, channelCommands),
    function(channelContext, characterReference, message) {
        if(S(message.toLowerCase()).contains("possible genetic clone of adolf hitler"))
        {
            channelContext.sendMessage("Oh, and by the way, if I was a clone of Adolf Goddamn Hitler, wouldn't I look like Adolf Goddamn Hitler?!");
        }
    }
];

Chat.onConnectionStatusChange = function(oldValue, newValue) {
    print("Connection Status: " + newValue);
};

Chat.onPrivateMessage = defaultPrivateMessageHandlers;

Chat.onChannelJoin = function(name, cr) {
    cr.onMessage = defaultChannelMessageHandlers;
};

Chat.tryConnect();