Object.defineProperty(this, 'internalInclude', {
    value: function(path) {
        print("IInclude: " + path);
    },
    writable: false,
    enumerable: false,
    configurable: false
});

var Lotus = {
    createBlankObject : function() { return {}; },
    printObject : function(obj) {
        print(JSON.stringify(obj));
    }
};

function isBoolean(val) { return typeof(val) === 'boolean'; }

function isFunction (v) {
    return typeof v === 'function';
}

function isNumber (val) {
    return typeof val === 'number' || val instanceof Number;
}

function isString (val) {
    return typeof val === 'string' || val instanceof String;
}

function isObject (val) {
    return typeof val === 'object' &&
        exists(val) &&
        !Array.isArray(val) &&
        !(val instanceof RegExp) &&
        !(val instanceof String) &&
        !(val instanceof Number);
}

function isOwner(characterReference)
{
    var crName = characterReference.getName();

    if(Owner.Name)
    {
        return _.includes(Owner.Name, crName);
    }
    else if(_.isString(Owner.Name))
    {
        return crName === Owner.Name;
    }

    return false;
}

function CreateChannelCommandListener(commandPrefix, characterAuthenticationCallback, commandCallbacks) {
    if(typeof(commandPrefix) === 'undefined' || commandPrefix === null) commandPrefix = '!';

    return function(channelContext, characterReference, message) {

        if(commandPrefix === '' || message.indexOf(commandPrefix) === 0)
        {
            var parts = message.chompLeft(commandPrefix).split(' ');

            if(parts.length >= 1)
            {
                var command = parts[0];
                var arguments = parts.slice(1);

                var authResponse = characterAuthenticationCallback(channelContext, characterReference, command, arguments);

                if(isBoolean(authResponse) && authResponse && commandCallbacks.hasOwnProperty(command))
                {
                    var cc = commandCallbacks[command];

                    if(isFunction(cc))
                        cc(channelContext, characterReference, command, arguments);
                }
                else if(isString(authResponse) && commandCallbacks.hasOwnProperty(authResponse))
                {
                    var ccGroup = commandCallbacks[authResponse];

                    if(isObject(ccGroup) && ccGroup.hasOwnProperty(command))
                    {
                        var cc = ccGroup[command];

                        if(isFunction(cc))
                            cc(channelContext, characterReference, command, arguments);
                    }
                }
            }
        }
    };
};

function CreateCommandListener(commandPrefix, characterAuthenticationCallback, commandCallbacks) {
    if(typeof(commandPrefix) === 'undefined' || commandPrefix === null) commandPrefix = '!';

    return function(characterReference, message) {
        if(commandPrefix === '' || message.indexOf(commandPrefix) === 0)
        {
            var parts = message.chompLeft(commandPrefix).split(' ');

            if(parts.length >= 1)
            {
                var command = parts[0];
                var arguments = parts.slice(1);

                var authResponse = characterAuthenticationCallback(characterReference, command, arguments);

                if(isBoolean(authResponse) && authResponse && commandCallbacks.hasOwnProperty(command))
                {
                    var cc = commandCallbacks[command];

                    if(isFunction(cc))
                        cc(characterReference, command, arguments);
                }
                else if(isString(authResponse) && commandCallbacks.hasOwnProperty(authResponse))
                {
                    var ccGroup = commandCallbacks[authResponse];

                    if(isObject(ccGroup) && ccGroup.hasOwnProperty(command))
                    {
                        var cc = ccGroup[command];

                        if(isFunction(cc))
                            cc(characterReference, command, arguments);
                    }
                }
            }
        }
    };
};

if(Script !== 'undefined')
{

}

internalInclude("test");

// This is a script environment, may as well latch string.js onto the prototype.
S.extendPrototype();